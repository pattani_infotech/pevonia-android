package com.pevonia.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.activity.Navigation;
import com.pevonia.activity.PevoniaApp;
import com.pevonia.bean.CartProductBean;
import com.pevonia.fragment.Cart;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.CartProductListener;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.pevonia.fragment.Cart.ordertotal1;
import static com.pevonia.fragment.Cart.otnew1;
import static com.pevonia.util.WebService.paypalAmount;

/**
 * Created by user on 7/10/2017.
 */

public class CartProAdapter extends BaseAdapter {
    private ArrayList<CartProductBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    String img_url;
    private ProgressDialog pDialog;
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String, String> lcmap;
CartProductListener cartProductListener;
    public CartProAdapter(ArrayList<CartProductBean> cartProBeen, FragmentActivity activity) {
        stringList = cartProBeen;
        this.context = activity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        lcManager = new LCManager(context);
        lcmap = lcManager.getLC();
    }


    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_add_cart, null);
            holder.pname = (TextView) convertView.findViewById(R.id.pname);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.totalprice = (TextView) convertView.findViewById(R.id.totalprice);

            holder.pimg = (ImageView) convertView.findViewById(R.id.pimg);
            holder.minus = (ImageView) convertView.findViewById(R.id.minus);
            holder.plus = (ImageView) convertView.findViewById(R.id.plus);
            holder.remove = (Button) convertView.findViewById(R.id.remove);

            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();

        }
        //double subtotal = 0;
        //Log.e("size cart",""+stringList.size());
        if (stringList.get(position) != null) {
            /*try {
                JSONArray namearray = new JSONArray(stringList.get(position).getName());
                JSONObject o = namearray.getJSONObject(0);
                holder.pname.setText(o.getString("value"));
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            holder.pname.setText(stringList.get(position).getName());


            holder.price.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(Double.parseDouble(stringList.get(position).getPrice()))));
            holder.qty.setText(stringList.get(position).getQty());

            if (stringList.get(position).getId_default_image().equals("") || stringList.get(position).getId_default_image().equals("na") || stringList.get(position).getId_default_image().equals("null")) {
                Glide.with(context).load(R.drawable.productimagenot)
                        .thumbnail(0.5f)
                        .into(holder.pimg);
            } else {
                img_url = WebService.image_url + stringList.get(position).getId() + "/" + stringList.get(position).getId_default_image() + "&ws_key=" + WebService.api_key;
                Glide.with(context).load(img_url)
                        .thumbnail(0.5f)
                        .into(holder.pimg);
            }


            /*img_url = WebService.image_url + stringList.get(position).getId() + "/" + stringList.get(position).getId_default_image() + "&ws_key=" + WebService.api_key;
            Glide.with(context).load(stringList.get(position).getId_default_image())
                    .thumbnail(0.5f)
                    .into(holder.pimg);*/
        }
        //ByDefaultcosting(stringList.get(position).getPrice(),stringList.get(position).getQty());
        holder.totalprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(ByDefaultcosting(stringList.get(position).getPrice(), stringList.get(position).getQty()))));
        //subtotal=subtotal+ (ByDefaultcosting(stringList.get(position).getPrice(), stringList.get(position).getQty()));
        NewTotalAmount(Integer.parseInt(holder.qty.getText().toString()));
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.qty.getText().toString());
                int tqty = qty + 1;

                UpdateProductQty(tqty, stringList.get(position).getId(),position);

                //stringList.get(position).setQty(String.valueOf(tqty));
                holder.qty.setText("" + tqty);

                holder.totalprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(IncrementCosting(stringList.get(position).getPrice(), holder.qty.getText().toString()))));
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.qty.getText().toString());
                if (qty < 2) {

                } else {
                    int tqty = qty - 1;
                    UpdateProductQty(tqty, stringList.get(position).getId(),position);
                    //notifyDataSetChanged();
                    //stringList.get(position).setQty(String.valueOf(tqty));
                    holder.qty.setText("" + tqty);

                    holder.totalprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(DecrementCosting(stringList.get(position).getPrice(), holder.qty.getText().toString()))));
                }
            }
        });
        /*holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Remove Item");
                alertDialog.setMessage("Are you sure you want to Remove it?");
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        stringList.remove(getItem(position));
                        notifyDataSetChanged();
                        if (stringList.size()==0){
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans));
                            }else {
                                Navigation.counttext.setBackgroundColor(ContextCompat.getColor(context,R.color.trans));
                            }
                            //Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans,null));
                            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID),context)+"0.00");
                            Navigation.cancel.setText("Your cart is empty...");
                            Navigation.confirm.setVisibility(View.GONE);
                            Cart.nodata.setVisibility(View.VISIBLE);
                            Cart.withshipping.setVisibility(View.GONE);
                            Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID),context)+"0.00");
                            paypalAmount="0.00";
                        }else {
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Navigation.counttext.setBackground(ContextCompat.getDrawable(context,R.drawable.round));
                            }else {
                                Navigation.counttext.setBackground(ContextCompat.getDrawable(context,R.drawable.round));
                            }
                            //Navigation.counttext.setBackground(context.getResources().getDrawable(R.drawable.round,null));
                            Navigation.counttext.setText(""+stringList.size());
                            Navigation.cancel.setText("cancel Order");
                            Navigation.confirm.setVisibility(View.VISIBLE);
                        }
                        //NewTotalAmount();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });*/
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Remove Item");
                alertDialog.setMessage("Are you sure you want to Remove it?");
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (detector.isConnectingToInternet()) {
                            RemoveFromCartProduct(stringList.get(position).getId(), stringList.get(position).getQty(), position);
                        } else {
                            WebService.MakeToast(context, context.getString(R.string.check_internet));
                        }
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
        return convertView;
    }

    private void UpdateProductQty(final int tqty, final String id, final int position) {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Edit_Cart + WebService.CartID;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        Log.e("Edit_Cart", "" + fic_response);
                        if (fic_response != null) {
                            //startActivity(new Intent(ProductDetail.this,ProductActivity.class));
                            JSONObject obj = new JSONObject(fic_response);
                            JSONObject obj1 = new JSONObject(obj.getString("cart"));
                            JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                            Log.e("cart_rows", "" + associationsObj.getString("cart_rows"));
                            JSONObject cartobj = new JSONObject(associationsObj.getString("cart_rows"));
                            if (cartobj.getString("cart_row").trim().charAt(0) == '{') {
                                stringList.get(position).setQty(String.valueOf(tqty));
                                notifyDataSetChanged();
                                WebService.MakeToast(context, "Update successfull...");
                            }else {
                                JSONArray ary = cartobj.getJSONArray("cart_row");
                                Log.e("size", ary.length() + "");
                                stringList.get(position).setQty(String.valueOf(tqty));
                                notifyDataSetChanged();
                                WebService.MakeToast(context, "Update successfull...");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_product", "" +id);
                    params.put("quantity", ""+tqty);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(context,context.getString(R.string.check_internet));
        }
    }

    private void RemoveFromCartProduct(final String id, final String qty, final int position) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        if (!WebService.CartID.equals("")) {
            pDialog.show();
            String url = WebService.Product_remove_to_cart + WebService.CartID + "&delete=deleting";
            Log.e("url", url + "");
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        Log.e("Product_remove_to_cart", "" + fic_response);
                        if (fic_response != null) {
                            JSONObject obj = new JSONObject(fic_response);
                            JSONObject obj1 = new JSONObject(obj.getString("cart"));                //                            if (obj1.has("associations")) {
//                                stringList.remove(getItem(position));
//                                notifyDataSetChanged();
//                                JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
//                                JSONObject cartobj = new JSONObject(associationsObj.getString("cart_rows"));
//                                if (cartobj.getString("cart_row").trim().charAt(0) == '[') {
//                                    JSONArray ary = cartobj.getJSONArray("cart_row");
//                                    if (ary.length() == 0) {
//                                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                            Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans));
//                                        } else {
//                                            Navigation.counttext.setBackgroundColor(ContextCompat.getColor(context, R.color.trans));
//                                        }
//                                        Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + "0.00");
//                                        Navigation.cancel.setText("Your cart is empty...");
//                                        Navigation.confirm.setVisibility(View.GONE);
//                                    } else {
//                                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                            Navigation.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.round));
//                                        } else {
//                                            Navigation.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.round));
//                                        }
//                                        Navigation.counttext.setText("" + ary.length());
//                                        Navigation.cancel.setText("cancel Order");
//                                        Navigation.confirm.setVisibility(View.VISIBLE);
//                                    }
//                                    WebService.MakeToast(context, "Product remove from cart successfull...");
//                                } else {
//                                    if (!cartobj.isNull("cart_row")) {
//                                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                            Navigation.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.round));
//                                        } else {
//                                            Navigation.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.round));
//                                        }
//                                        Navigation.counttext.setText("1");
//                                        Navigation.cancel.setText("cancel Order");
//                                        Navigation.confirm.setVisibility(View.VISIBLE);
//                                    }
//                                }
//                            } else {
//                                //cartProductListener.loadProduct(stringList);
//
//                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                    Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans));
//                                } else {
//                                    Navigation.counttext.setBackgroundColor(ContextCompat.getColor(context, R.color.trans));
//                                }
//                                Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + "0.00");
//                                Navigation.cancel.setText("Your cart is empty...");
//                                Navigation.confirm.setVisibility(View.GONE);
//                            }
                            ((Navigation)context).refresh();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_product", id);
                    params.put("quantity", qty);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }
    }

    private void NewTotalAmount(int qty) {
      /*  List<CartProductBean> ffff = new ArrayList<CartProductBean>();
        for (int i = 0; i < stringList.size(); i++) {
            CartProductBean bean = new CartProductBean();
            bean.setId(stringList.get(i).getId());
            bean.setPrice(stringList.get(i).getPrice());
            bean.setName(stringList.get(i).getName());
            bean.setQty(""+qty);
            bean.setId_default_image(stringList.get(i).getId_default_image());
            ffff.add(bean);
            notifyDataSetChanged();
        }*/

        double ttt = 0;
        WebService.ttt=0;
        double sip = 15;
        for (int i = 0; i < stringList.size(); i++) {
            //Log.e("price", stringList.get(i).getPrice());
            //Log.e("qty", stringList.get(i).getQty());
            WebService.ttt = WebService.ttt + (ByDefaultcosting(stringList.get(i).getPrice(), stringList.get(i).getQty()));
        }
        //Log.e("ttt", ttt + "");

        if (!WebService.shipingid.equals("")){
            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip))));
            ordertotal1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(WebService.ttt)));
            otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip))));
            paypalAmount = (WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip)));
            Cart.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(WebService.sip))));
        }else {
            //Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(ttt + sip))));
            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(WebService.ttt))));
            ordertotal1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + WebService.GetDecimalFormate(String.valueOf(WebService.ttt)));
            //otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(ttt + sip))));
            otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + (WebService.GetDecimalFormate(String.valueOf(WebService.ttt))));
            //paypalAmount = (WebService.GetDecimalFormate(String.valueOf(ttt + sip)));
            paypalAmount = (WebService.GetDecimalFormate(String.valueOf(ttt)));
        }
    }

    private double DecrementCosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }

    private double IncrementCosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }

    public double ByDefaultcosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }

    public void Remove() {
        stringList.removeAll(stringList);
        notifyDataSetChanged();
        //WebService.CountAdapter(stringList.size(), context);
        if (stringList.size() == 0) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans));
            } else {
                Navigation.counttext.setBackgroundColor(ContextCompat.getColor(context, R.color.trans));
            }
            //Navigation.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans,null));
            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) + "0.00");
            Navigation.cancel.setText("Your cart is empty...");
            Navigation.confirm.setVisibility(View.GONE);
            //Cart.nodata.setVisibility(View.VISIBLE);
            //Cart.withshipping.setVisibility(View.GONE);
            //Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID),context)+"0.00");
            paypalAmount = "0.00";
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Navigation.counttext.setBackground(context.getResources().getDrawable(R.drawable.round));
            } else {
                Navigation.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.round));
            }
            //Navigation.counttext.setBackground(context.getResources().getDrawable(R.drawable.round,null));
            Navigation.counttext.setText("" + stringList.size());
            Navigation.cancel.setText("cancel Order");
            Navigation.confirm.setVisibility(View.VISIBLE);
        }
    }

    private static class ViewHolder {
        RelativeLayout container;
        TextView pname, qty, price, totalprice;
        ImageView pimg, minus, plus;
        Button remove;
    }


}
