package com.pevonia.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.TreatmentBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 7/10/2017.
 */

public class TreatmentAdapter extends BaseAdapter implements Filterable {
    private List<TreatmentBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    private List<TreatmentBean> plist;
  ValueFilter valueFilter;


    public TreatmentAdapter(List<TreatmentBean> treatmentBeen, FragmentActivity activity) {
        stringList = treatmentBeen;
        this.plist = treatmentBeen;
        this.context = activity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        /*if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.right_side_layout, null);
            holder.tv1 = (TextView) convertView.findViewById(R.id.tv1);
            holder.disc = (TextView) convertView.findViewById(R.id.disc);
            holder.timg = (ImageView) convertView.findViewById(R.id.timg);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
            holder.tv1.setText(stringList.get(position).getTname());
            holder.disc.setText(stringList.get(position).getTdisc());
            holder.timg.setImageResource(stringList.get(position).getTimage());*/
        if (convertView == null){

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        if ((position%2)==0){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.right_side_layout, null);
            holder.tv1 = (TextView) convertView.findViewById(R.id.tv1);
            holder.disc = (TextView) convertView.findViewById(R.id.disc);
            holder.timg = (ImageView) convertView.findViewById(R.id.timg);
            convertView.setTag(holder);
        }else {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.left_side_layout, null);
            holder.tv1 = (TextView) convertView.findViewById(R.id.tv1);
            holder.disc = (TextView) convertView.findViewById(R.id.disc);
            holder.timg = (ImageView) convertView.findViewById(R.id.timg);
            convertView.setTag(holder);
        }
        holder.tv1.setText(stringList.get(position).getTname());
        holder.disc.setText(stringList.get(position).getTdisc());
        holder.timg.setImageResource(stringList.get(position).getTimage());

        return convertView;
    }

    private static class ViewHolder {
        TextView tv1,disc;
        ImageView timg;

    }
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                //Log.e("SearchFilter", " if");
                List<TreatmentBean> filterList = new ArrayList<TreatmentBean>();
                for (int i = 0; i < plist.size(); i++) {
                    //Log.e("size",""+plist.size());
                    if (plist.get(i).getTname().toString().toUpperCase().contains(constraint.toString().toUpperCase())||plist.get(i).getTdisc().toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        TreatmentBean bean = new TreatmentBean();
                        bean.setTname(plist.get(i).getTname());
                        bean.setTdisc(plist.get(i).getTdisc());
                        bean.setTimage(plist.get(i).getTimage());
                        filterList.add(bean);
                    }

                }

                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<TreatmentBean>) results.values;
            //Log.e("SearchFilter",stringList.size()+" size ");
            notifyDataSetChanged();
        }
    }

}
