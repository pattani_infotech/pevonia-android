package com.pevonia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.SkinAdvisoryBean;

import java.util.List;

/**
 * Created by user on 7/10/2017.
 */

public class SkinAdvisoryAdapter extends BaseAdapter {
    private List<SkinAdvisoryBean> stringList;
    private Context context;
    private LayoutInflater inflater;


    public SkinAdvisoryAdapter(List<SkinAdvisoryBean> skinAdvisoryBeen, Context context) {
        stringList = skinAdvisoryBeen;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.skin_advisory_layout, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv1);
            holder.skinimg = (ImageView) convertView.findViewById(R.id.photoView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (stringList.get(position) != null) {
            holder.title.setText(stringList.get(position).getTitle());
            holder.skinimg.setImageResource(stringList.get(position).getSkinimage());
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView title;
        ImageView skinimg;

    }


}
