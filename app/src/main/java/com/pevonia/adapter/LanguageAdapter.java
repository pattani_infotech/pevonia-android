package com.pevonia.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.LanguageBean;

import java.util.List;

/**
 * Created by user on 8/17/2017.
 */

public class LanguageAdapter extends BaseAdapter {
    List<LanguageBean> rowItems;
    Context context;
    public LanguageAdapter(Context context, List<LanguageBean> rowItems){
        this.context=context;
        this.rowItems=rowItems;
    }
    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        LayoutInflater mInflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        holder=new ViewHolder();
        if(convertView ==null){
            convertView=mInflater.inflate(R.layout.lg_layout,null);
            holder.name=(TextView)convertView.findViewById(R.id.name);


            convertView.setTag(holder);

        }else{
            holder=(ViewHolder)convertView.getTag();
        }
        LanguageBean lgBean=rowItems.get(position);

        holder.name.setText(lgBean.getName());
        return convertView;
    }



    public class ViewHolder{
        TextView name;
    }
}
