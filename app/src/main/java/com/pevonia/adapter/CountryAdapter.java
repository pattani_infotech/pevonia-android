package com.pevonia.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.CountryBean;

import java.util.List;

/**
 * Created by user on 8/17/2017.
 */

public class CountryAdapter extends BaseAdapter {
    List<CountryBean> rowItems;
    Context context;
    public CountryAdapter(Context context, List<CountryBean> rowItems){
        this.context=context;
        this.rowItems=rowItems;
    }
    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        LayoutInflater mInflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        holder=new ViewHolder();
        if(convertView ==null){
            convertView=mInflater.inflate(R.layout.country_layout,null);
            holder.countryid=(TextView)convertView.findViewById(R.id.countryid);


            convertView.setTag(holder);

        }else{
            holder=(ViewHolder)convertView.getTag();
        }
        CountryBean countryBean=rowItems.get(position);
        /*if (position==0){
            holder.countryid.setText("Select Countries");
        }*/
        /*try {
            JSONArray namearray = new JSONArray(rowItems.get(position).getName());
            JSONObject o = namearray.getJSONObject(0);
            //Log.e("aryname", "" + o.getString("value"));
            holder.countryid.setText(o.getString("value"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        holder.countryid.setText(countryBean.getName());
        return convertView;
    }



    public class ViewHolder{
        TextView countryid;
    }
}
