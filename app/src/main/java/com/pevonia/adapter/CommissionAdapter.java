package com.pevonia.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.CommissionBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 16-Dec-17.
 */

public class CommissionAdapter extends RecyclerView.Adapter<CommissionAdapter.ViewHolder> {

    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private List<CommissionBean> stringList;
    private List<CommissionBean> plist;
    private Context context;
    double totalPaid,rate,total;


    public CommissionAdapter(List<CommissionBean> orderBeansList, Context context) {
        this.stringList = orderBeansList;
        this.plist = orderBeansList;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();

    }
    @Override
    public CommissionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.commission_layout, parent, false);
        return new CommissionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
       // Log.e("stringList",stringList.size()+" "+position);
      String Currency_code= WebService.GEtCurrecyById(stringList.get(position).getCurrecy_id(),context);
    holder.order_date.setText(stringList.get(position).getInvoice_date());
         String order_ref="<b><font size=15 color=black>Order Reference</font></b> :-<font color='#ad013d'>"+stringList.get(position).getOrder_reference()+"</font>";
        String total_price;
        if(Currency_code!=null)
        {
             total_price="<b><font size=15 color=black>Payment</font></b> :-<b>"+Currency_code+WebService.GetDecimalFormate(stringList.get(position).getTotal_paid())+"</b>";
        }else{
            total_price="<b><font size=15 color=black>Payment</font></b> :-<b>"+WebService.GetDecimalFormate(stringList.get(position).getTotal_paid())+"</b>";
        }
          String Order_date="<b><font size=15 color=black>Order Date</font></b> :-<b>"+stringList.get(position).getInvoice_date()+"</b>";

        String Order_status="<b><font size=15 color=black>Status</font></b> :-<b>"+stringList.get(position).getStatus()+"</b>";;
        String customerName="<b><font size=15 color=black>Customer Name</font></b> :-<b>"+stringList.get(position).getCustomerName()+"</b>";;
      //  Log.e("stringList",stringList.size()+" "+position+" "+stringList.get(position).getPayment());;
;       rate= Double.parseDouble(WebService.dealer_Commission_rate);
        totalPaid= Double.parseDouble(stringList.get(position).getTotal_paid());
        total=((totalPaid*rate)/100);
        DecimalFormat decimalFormat = new DecimalFormat("#.00");

        if (Build.VERSION.SDK_INT >= 24) {
            holder.order_reference.setText(Html.fromHtml(order_ref, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_total_price.setText(Html.fromHtml(total_price, Html.FROM_HTML_MODE_LEGACY));
            holder.order_status.setText(Html.fromHtml(Order_status, Html.FROM_HTML_MODE_LEGACY));
            holder.customer_name.setText(Html.fromHtml(customerName, Html.FROM_HTML_MODE_LEGACY));
             // for 24 api and more

        } else {
            holder.order_reference.setText(Html.fromHtml(order_ref));
            holder.order_date.setText(Html.fromHtml(Order_date));
            holder.order_total_price.setText(Html.fromHtml(total_price));
            holder.order_status.setText(Html.fromHtml(Order_status));
            holder.customer_name.setText(Html.fromHtml(customerName));
             // or for older api
        }
        holder.commission.setText("Commission\n"+Currency_code+decimalFormat.format(total));
        /*holder.order_layoout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                Intent intent=new Intent(context,OrderDetail.class);
                intent.putExtra("id",stringList.get(position).getId());
                intent.putExtra("currencycode",stringList.get(position).getCurrecy_id());
                context.startActivity(intent);
                } else {
                    WebService.MakeToast(context, context.getString(R.string.check_internet));
                }
            }
        });*/




    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

         private final TextView order_reference,order_date,order_total_price,order_status,customer_name,commission;
LinearLayout order_layoout;
        public ViewHolder(View itemView) {
            super(itemView);

            order_reference = (TextView) itemView.findViewById(R.id.order_reference);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
            order_total_price = (TextView) itemView.findViewById(R.id.order_total_price);
            order_status = (TextView) itemView.findViewById(R.id.order_status);
            customer_name = (TextView) itemView.findViewById(R.id.customer_name);
            commission = (TextView) itemView.findViewById(R.id.commission);
            order_layoout = (LinearLayout) itemView.findViewById(R.id.order_layoout);
        }
    }
}
