package com.pevonia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.activity.ProductDetail;
import com.pevonia.bean.PerentBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by user on 7/10/2017.
 */

public class ProGridAdapter extends BaseAdapter {
    private List<PerentBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    String img_url;
    ConnectionDetector detector;


    public ProGridAdapter(List<PerentBean> proGridBean, FragmentActivity activity) {
        stringList = proGridBean;
        this.context = activity;
        detector = new ConnectionDetector(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (stringList.get(position) != null) {
            //holder.pName.setText(stringList.get(position).getName());
            //holder.pimage.setImageResource(stringList.get(position).getPtimage());
            holder.price.setText("RM"+stringList.get(position).getPrice());
             img_url= WebService.image_url+stringList.get(position).getId()+"/"+stringList.get(position).getId_default_image()+"&ws_key="+WebService.api_key;


            Glide.with(context).load(img_url)
                    .thumbnail(0.5f)
                    .into(holder.pimage);

            try {
                JSONArray namearray = new JSONArray(stringList.get(position).getName());
                JSONObject o=namearray.getJSONObject(0);
                holder.pName.setText(o.getString("value"));
               // holder.pName.setText("العلاج");
               // Log.e("aryname", "" + namearray.length());
              /*  for (int j = 0; j < namearray.length(); j++) {
                    JSONObject o=namearray.getJSONObject(1);
                    Log.e("id : ",o.getString("id"));
                    Log.e("value : ",o.getString("value"));
                    holder.pName.setText(o.getString("value"));
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        /*if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (listpress==true){
            Log.e("gride view",""+listpress);
            convertView = inflater.inflate(R.layout.list_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);


        }else if (listpress==false){
            Log.e("listview",""+ listpress);
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);
        }
        holder.pName.setText(stringList.get(position).getPtname());
        holder.pimage.setImageResource(stringList.get(position).getPtimage());*/
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                String newimag=WebService.image_url+stringList.get(position).getId()+"/"+stringList.get(position).getId_default_image()+"&ws_key="+WebService.api_key;
                Intent intent=new Intent(context, ProductDetail.class);
                intent.putExtra("pname",stringList.get(position).getName());
                intent.putExtra("id",stringList.get(position).getId());
                intent.putExtra("imgurl",newimag);
                intent.putExtra("price",stringList.get(position).getPrice());
                context.startActivity(intent);
            } else {
                WebService.MakeToast(context, context.getString(R.string.check_internet));
            }
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        RelativeLayout container;
        TextView pName,qty,price;
        ImageView pimage,wishlist;

    }


}
