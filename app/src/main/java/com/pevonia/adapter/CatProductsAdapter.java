package com.pevonia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.activity.CategoryProduct;
import com.pevonia.activity.ProductDetail;
import com.pevonia.bean.CatProductBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.pevonia.activity.CategoryProduct.listpressCP;

/**
 * Created by user on 7/10/2017.
 */

public class CatProductsAdapter extends BaseAdapter implements Filterable {
    private List<CatProductBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    String img_url;
    private List<CatProductBean> plist;
    ValueFilter valueFilter;
    ConnectionDetector detector;
    private String[] bgColors;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    public CatProductsAdapter(List<CatProductBean> proGridBean, FragmentActivity activity) {
        this.stringList = proGridBean;
        this.plist = proGridBean;
        this.context = activity;
        detector = new ConnectionDetector(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        bgColors = activity.getApplicationContext().getResources().getStringArray(R.array.movie_serial_bg);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        lcManager=new LCManager(context);
        lcmap=lcManager.getLC();
    }


    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        /*if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (stringList.get(position) != null) {
            holder.pName.setText(stringList.get(position).getPtname());
            holder.pimage.setImageResource(stringList.get(position).getPtimage());
        }
        if (ProductActivity.listpress){
            Log.e("gride view",""+ProductActivity.listpress);
        }else {
            Log.e("listview",""+ProductActivity.listpress);
        }*/
        if (convertView == null) {
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (listpressCP) {
            //Log.e("listpressCP = fa adp ",listpressCP+"");
            //Log.e("gride view", "" + listpress);
            convertView = inflater.inflate(R.layout.list_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            convertView.setTag(holder);
        } /*else if (listpressCP == true) {
            Log.e("listpressCP = tr adp ",listpressCP+"");
            //Log.e("listview", "" + listpress);
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);
        }*/ else {
            //Log.e("listpressCP = def adp ",listpressCP+"");
            //Log.e("listview", "" + listpress);
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            convertView.setTag(holder);
        }
        try {
            JSONArray namearray = new JSONArray(stringList.get(position).getName());
            if (lcmap.get(LCManager.languageID)!=null) {
                for (int i = 0; i < namearray.length(); i++) {
                    JSONObject o = namearray.getJSONObject(i);
                    if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                        holder.pName.setText(o.getString("value"));
                    }
                }
            }else {
                    JSONObject on = namearray.getJSONObject(0);
                    holder.pName.setText(on.getString("value"));
            }
            //Log.e("aryname", "" + o.getString("value"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (stringList.get(position).getAvailable().toString().equals("1")) {
            holder.price.setVisibility(View.VISIBLE);
        } else {
            holder.price.setVisibility(View.GONE);
        }
        holder.price.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID),context) + WebService.GetDecimalFormate(stringList.get(position).getPrice()));
        //holder.pName.setText(stringList.get(position).getName());
        if (stringList.get(position).getId_default_image().equals("") || stringList.get(position).getId_default_image().equals("na") || stringList.get(position).getId_default_image().equals("null")) {
            Glide.with(context).load(R.drawable.not_available)
                    .thumbnail(0.5f)
                    .into(holder.pimage);
        } else {
            img_url = WebService.image_url + stringList.get(position).getId() + "/" + stringList.get(position).getId_default_image() + "&ws_key=" + WebService.api_key;
            //Log.e("send img",img_url);
            Glide.with(context).load(img_url)
                    .thumbnail(0.5f)
                    .into(holder.pimage);
        }
        //holder.pimage.setImageResource(stringList.get(position).getPtimage());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                    String newimag = "";
                    if (stringList.get(position).getId_default_image().equals("") || stringList.get(position).getId_default_image().equals("na") || stringList.get(position).getId_default_image().equals("null")) {
                        newimag = "";
                    } else {
                        newimag = WebService.image_url + stringList.get(position).getId() + "/" + stringList.get(position).getId_default_image() + "&ws_key=" + WebService.api_key;
                    }
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("pname", stringList.get(position).getName());
                    intent.putExtra("id", stringList.get(position).getId());
                    intent.putExtra("imgurl", newimag);
                    intent.putExtra("available", stringList.get(position).getAvailable());
                    intent.putExtra("price", stringList.get(position).getPrice());
                    context.startActivity(intent);
                } else {
                    WebService.MakeToast(context, context.getString(R.string.check_internet));
                }
            }
        });
        /*String color = bgColors[position % bgColors.length];
        holder.pName.setBackgroundColor(Color.parseColor(color));*/
        return convertView;
    }

    private static class ViewHolder {
        TextView pName, qty, price;
        ImageView pimage, wishlist;
        RelativeLayout container;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                //Log.e("SearchFilter", " if");
                List<CatProductBean> filterList = new ArrayList<CatProductBean>();
                for (int i = 0; i < plist.size(); i++) {
                    //Log.e("size",""+plist.size());
                    if (plist.get(i).getName().toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        CatProductBean bean = new CatProductBean();
                        bean.setId(plist.get(i).getId());
                        bean.setPrice(plist.get(i).getPrice());
                        bean.setName(plist.get(i).getName());
                        bean.setAvailable(plist.get(i).getAvailable());
                        bean.setId_default_image(plist.get(i).getId_default_image());
                        filterList.add(bean);
                    }

                }

                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<CatProductBean>) results.values;
            Log.e("SearchFilter",stringList.size()+" size ");
            if (stringList.size() == 0) {
                CategoryProduct.nodata.setVisibility(View.VISIBLE);
                //CategoryProduct.gridview.setVisibility(View.GONE);
                //CategoryProduct.listview.setVisibility(View.GONE);
            } else {
                CategoryProduct.nodata.setVisibility(View.GONE);
                //CategoryProduct.gridview.setVisibility(View.VISIBLE);
                //CategoryProduct.listview.setVisibility(View.VISIBLE);
            }
            notifyDataSetChanged();
        }
    }

}
