package com.pevonia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.activity.StoreListDetail;
import com.pevonia.bean.StoreListBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 16-Dec-17.
 */

public class StoreListAdapter extends RecyclerView.Adapter<StoreListAdapter.ViewHolder>implements Filterable {

    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    private List<StoreListBean> stringList;
    private List<StoreListBean> plist;
    private Context context;
ValueFilter valueFilter;
    String img_url;

    public StoreListAdapter(List<StoreListBean> listBeen, Context context) {
        this.stringList = listBeen;
        this.plist = listBeen;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        lcManager=new LCManager(context);
        lcmap=lcManager.getLC();

    }

    @Override
    public StoreListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.store_listing_layout, parent, false);
        return new StoreListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
       /* // Log.e("stringList",stringList.size()+" "+position);
        String Currency_code = WebService.GEtCurrecyById(stringList.get(position).getCurrecy_id(), context);
        holder.order_date.setText(stringList.get(position).getInvoice_date());
        String order_ref = "<b><font size=15 color=black>Order Reference</font></b> :-<font color='#ad013d'>" + stringList.get(position).getOrder_reference() + "</font>";
        String total_price;
        if (Currency_code != null) {
            total_price = "<b><font size=15 color=black>Payment</font></b> :-<b>" + Currency_code + stringList.get(position).getTotal_paid() + "</b>";
        } else {
            total_price = "<b><font size=15 color=black>Payment</font></b> :-<b>" + stringList.get(position).getTotal_paid() + "</b>";
        }
        String Order_date = "<b><font size=15 color=black>Order Date</font></b> :-<b>" + stringList.get(position).getInvoice_date() + "</b>";

        String Order_status = "<b><font size=15 color=black>Status</font></b> :-<b>" + stringList.get(position).getPayment() + "</b>";
        ;
        //  Log.e("stringList",stringList.size()+" "+position+" "+stringList.get(position).getPayment());;
        ;
        if (Build.VERSION.SDK_INT >= 24) {
            holder.order_reference.setText(Html.fromHtml(order_ref, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_total_price.setText(Html.fromHtml(total_price, Html.FROM_HTML_MODE_LEGACY));
            holder.order_status.setText(Html.fromHtml(Order_status, Html.FROM_HTML_MODE_LEGACY));
             // for 24 api and more
        } else {
            holder.order_reference.setText(Html.fromHtml(order_ref));
            holder.order_date.setText(Html.fromHtml(Order_date));
            holder.order_total_price.setText(Html.fromHtml(total_price));
            holder.order_status.setText(Html.fromHtml(Order_status));
             // or for older api
        }*/
        try {
            JSONArray namearray = new JSONArray(stringList.get(position).getStore_name());
            for (int i=0;i<namearray.length();i++) {
                JSONObject o = namearray.getJSONObject(i);
                //Log.e("aryname", "" + o.getString("value"));
                if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                 if (o.getString("value").equals("")||o.getString("value").equals("null")){
                     holder.strname.setText("No Store Name");
                 }else {
                     holder.strname.setText(o.getString("value"));
                 }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.e("getStore_image",stringList.get(position).getStore_image());
        if(!stringList.get(position).getStore_image().equalsIgnoreCase("na")||!stringList.get(position).getStore_image().equalsIgnoreCase("")) {
            try {
                JSONObject obj1 = new JSONObject(stringList.get(position).getStore_image());
                if (!obj1.isNull("store_image")) {
                    JSONArray arr = obj1.getJSONArray("store_image");
                    //Log.e("arr", arr.toString());
                    JSONObject obj2 = arr.getJSONObject(0);
                    //Log.e("image",obj2.getString("image"));
                    img_url = WebService.store_image_url + obj2.getString("image");
                    Glide.with(context).load(img_url)
                            .thumbnail(0.5f)
                            .into(holder.strimg);
                }else {
                    Glide.with(context).load(R.drawable.pevoniastore)
                            .thumbnail(0.5f)
                            .into(holder.strimg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Glide.with(context).load(R.drawable.pevoniastore)
                    .thumbnail(0.5f)
                    .into(holder.strimg);
        }
        /*try {
            if (stringList.get(position).getStore_image() == null) {

            } else {
                JSONArray namearray = new JSONArray(stringList.get(position).getStore_image());
                JSONObject o = namearray.getJSONObject(0);
                //Log.e("o id", "" + o.getString("id"));
                //Log.e("o value", "" + o.getString("value"));
                if (o.getString("value").equals("null")) {
                    Glide.with(context).load(R.drawable.not_available)
                            .thumbnail(0.5f)
                            .into(holder.strimg);
                } else {
                     img_url = WebService.store_image_url + o.getString("value");
                    Glide.with(context).load(img_url)
                            .thumbnail(0.5f)
                            .into(holder.strimg);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        holder.city.setText(stringList.get(position).getCity());
        //Log.e("rating", stringList.get(position).getStore_rating());
        if (stringList.get(position).getStore_rating().equals("null") || stringList.get(position).getStore_rating() == null || stringList.get(position).getStore_rating().equals("")) {
            //Log.e("if", "no");
        } else {
           // Log.e("else", "no");
            holder.rating.setRating(Float.parseFloat(stringList.get(position).getStore_rating()));
        }


        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                Intent intent=new Intent(context,StoreListDetail.class);
                intent.putExtra("strname", stringList.get(position).getStore_name());
                intent.putExtra("id", stringList.get(position).getId());
                intent.putExtra("imgurl",img_url);
                context.startActivity(intent);
                } else {
                    WebService.MakeToast(context, context.getString(R.string.check_internet));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView strname,city;
        ImageView strimg;
        RelativeLayout container;
        RatingBar rating;

        public ViewHolder(View itemView) {
            super(itemView);
            rating = (RatingBar) itemView.findViewById(R.id.rating);
            strname = (TextView) itemView.findViewById(R.id.strname);
            city = (TextView) itemView.findViewById(R.id.city);
            strimg = (ImageView) itemView.findViewById(R.id.strimg);
            container = (RelativeLayout) itemView.findViewById(R.id.container);
        }
    }
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                //Log.e("SearchFilter", " if");
                List<StoreListBean> filterList = new ArrayList<StoreListBean>();
                for (int i = 0; i < plist.size(); i++) {
                    //Log.e("size",""+plist.size());
                    if (plist.get(i).getStore_name().toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        StoreListBean bean = new StoreListBean();
                        bean.setId(plist.get(i).getId());
                        bean.setCity(plist.get(i).getCity());
                        bean.setStore_image(plist.get(i).getStore_image());
                        bean.setStore_rating(plist.get(i).getStore_rating());
                        bean.setStore_name(plist.get(i).getStore_name());
                        filterList.add(bean);
                    }

                }

                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<StoreListBean>) results.values;
            //Log.e("SearchFilter",stringList.size()+" size ");
            notifyDataSetChanged();
        }
    }
}
