package com.pevonia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.activity.Category;
import com.pevonia.activity.CategoryProduct;
import com.pevonia.bean.CategoryBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by user on 3/6/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> implements Filterable {
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private List<CategoryBean> stringList;
    private List<CategoryBean> plist;
    private Context context;
    ValueFilter valueFilter;
    LCManager lcManager;
    HashMap<String,String>lcmap;

    public CategoryAdapter(List<CategoryBean> orderBeansList, Context context) {
        this.stringList = orderBeansList;
        this.plist = orderBeansList;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        lcManager=new LCManager(context);
        lcmap=lcManager.getLC();
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category, parent, false);
        return new CategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {

        //holder.categoryname.setText(stringList.get(position).getFname() + "  " + stringList.get(position).getLname());
        //holder.distance.setText(stringList.get(position).getDistance());
        //holder.mobileno.setText("Mobile No. :" + stringList.get(position).getMobile());
        /*Log.e("position",position+"");
        Log.e("size adapter",stringList.size()+"");*/
/*for (int i=0;i<stringList.size();i++){
    if (position==Integer.parseInt(stringList.get(i).getPosition())){
        try {
            JSONArray namearray = new JSONArray(stringList.get(i).getName());
            if (lcmap.get(LCManager.languageID) != null) {
                for (int k = 0;k < namearray.length(); k++) {
                    JSONObject o = namearray.getJSONObject(k);
                    if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                        Log.e("position",position+" "+stringList.get(i).getPosition()+" "+o.getString("value"));
                        holder.categoryname.setText(o.getString("value"));
                    }
                }
            } else {
                JSONObject on = namearray.getJSONObject(0);
                holder.categoryname.setText(on.getString("value"));
            }
            //Log.e("aryname", "" + o.getString("value"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}*/
                try {
                    JSONArray namearray = new JSONArray(stringList.get(position).getName());
                    if (lcmap.get(LCManager.languageID) != null) {
                        for (int i = 0; i < namearray.length(); i++) {
                            JSONObject o = namearray.getJSONObject(i);
                            if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                                //holder.categoryname.setText(getUTF8Encoded(o.getString("value")));
                                //holder.categoryname.setText(URLDecoder.decode(o.getString("value"), "UTF-8"));
                                //holder.categoryname.setText(new String(o.getString("value").getBytes("ISO-8859-1"), "UTF-8"));
                                holder.categoryname.setText(getUTF8Encoded(o.getString("value")));
                            }
                        }
                    } else {
                        JSONObject on = namearray.getJSONObject(0);
                        holder.categoryname.setText(getUTF8Encoded(on.getString("value")));//.getBytes("ISO-8859-1"), "UTF-8"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        holder.categoryname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryProduct.class);
                intent.putExtra("id", stringList.get(position).getId());
                /*intent.putExtra("lname", stringList.get(position).getLname());
                intent.putExtra("email", stringList.get(position).getEmail());
                intent.putExtra("company", stringList.get(position).getCompany());
                intent.putExtra("mobile", stringList.get(position).getMobile());
                intent.putExtra("r_message", stringList.get(position).getReview());
                intent.putExtra("dp", stringList.get(position).getAgent_propic().toString());
                intent.putExtra("agent_id", stringList.get(position).getId().toString());
                //Log.e("image123", stringList.get(position).getAgent_propic());*/
                context.startActivity(intent);
            }
        });
    }

    public static String getUTF8Encoded(String targetString) {
        String resultant = "";
        try {
            return new String(targetString.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return resultant;
        }
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }
    @Override
    public Filter getFilter() {

        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView categoryname;

        public ViewHolder(View itemView) {
            super(itemView);

            categoryname = (TextView) itemView.findViewById(R.id.categoryname);


        }
    }
    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                List<CategoryBean> filterList = new ArrayList<CategoryBean>();

                for (int i = 0; i < plist.size(); i++) {
                    if (plist.get(i).getName().toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        CategoryBean bean = new CategoryBean();
                        bean.setId(plist.get(i).getId());
                        bean.setNb_products_recursive(plist.get(i).getNb_products_recursive());
                        bean.setPosition(plist.get(i).getPosition());
                        bean.setName(plist.get(i).getName());
                        bean.setLink_rewrite(plist.get(i).getLink_rewrite());
                        filterList.add(bean);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<CategoryBean>) results.values;
            if (stringList.size() == 0) {
                Category.nodata.setVisibility(View.VISIBLE);
                Category.categorylist.setVisibility(View.GONE);
            } else {
                Category.nodata.setVisibility(View.GONE);
                Category.categorylist.setVisibility(View.VISIBLE);

            }
            notifyDataSetChanged();

        }
    }


}
