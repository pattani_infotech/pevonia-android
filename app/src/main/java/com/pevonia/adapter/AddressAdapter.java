package com.pevonia.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.activity.EditAddress;
import com.pevonia.activity.MyAddress;
import com.pevonia.activity.PevoniaApp;
import com.pevonia.bean.AddressBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.ItemClickListener;
import com.pevonia.util.WebService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 16-Dec-17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private List<AddressBean> stringList;
    private List<AddressBean> plist;
    private Context context;
    private ProgressDialog pDialog;
    double totalPaid, rate, total;
    private ItemClickListener clickListener;

    public AddressAdapter(List<AddressBean> orderBeansList, Context context) {
        this.stringList = orderBeansList;
        this.plist = orderBeansList;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();

    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.address_layout, parent, false);
        return new AddressAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // Log.e("stringList",stringList.size()+" "+position);

        holder.fname.setText(stringList.get(position).getFirstname() + " " + stringList.get(position).getLastname());
        //holder.lname.setText(stringList.get(position).getLastname());
        if (stringList.get(position).getCompany().equals("") || stringList.get(position).getCompany().equals("null")) {
            holder.company.setVisibility(View.GONE);
        } else {
            holder.company.setText(stringList.get(position).getCompany());
        }
        holder.alias.setText(stringList.get(position).getAlias());
        if (stringList.get(position).getAddress1().equals("") || stringList.get(position).getAddress1().equals("null")) {
            holder.address1.setVisibility(View.GONE);
        } else {
            holder.address1.setText(stringList.get(position).getAddress1());
        }
        if (stringList.get(position).getAddress2().equals("") || stringList.get(position).getAddress2().equals("null")) {
            holder.address2.setVisibility(View.GONE);
        } else {
            holder.address2.setText(stringList.get(position).getAddress2());
        }
        if (stringList.get(position).getPostcode().equals("") || stringList.get(position).getPostcode().equals("null")) {
            holder.postcode.setVisibility(View.GONE);
        } else {
            holder.postcode.setText(stringList.get(position).getPostcode() + " " + stringList.get(position).getCity());
        }
        if (stringList.get(position).getCity().equals("") || stringList.get(position).getCity().equals("null")) {
            holder.country.setVisibility(View.GONE);
        } else {
            holder.country.setText(stringList.get(position).getCity());
        }
        if (stringList.get(position).getPhone().equals("") || stringList.get(position).getPhone().equals("null")) {
            holder.phone.setVisibility(View.GONE);
        } else {
            holder.phone.setText(stringList.get(position).getPhone());
        }
        if (stringList.get(position).getPhone_mobile().equals("") || stringList.get(position).getPhone_mobile().equals("null")) {
            holder.phone_mobile.setVisibility(View.GONE);
        } else {
            holder.phone_mobile.setText(stringList.get(position).getPhone_mobile());
        }
        if (stringList.get(position).getPhone() != null && stringList.get(position).getPhone_mobile() != null) {
            holder.phone_mobile.setVisibility(View.GONE);
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,EditAddress.class);
                intent.putExtra("edit","edit");

                intent.putExtra("id",stringList.get(position).getId());
                intent.putExtra("id_country",stringList.get(position).getCountry_id());
                intent.putExtra("id_state",stringList.get(position).getState_id());
                intent.putExtra("alias",stringList.get(position).getAlias());
                intent.putExtra("company",stringList.get(position).getCompany());
                intent.putExtra("lastname",stringList.get(position).getLastname());
                intent.putExtra("firstname",stringList.get(position).getFirstname());
                intent.putExtra("address1",stringList.get(position).getAddress1());
                intent.putExtra("address2",stringList.get(position).getAddress2());
                intent.putExtra("postcode",stringList.get(position).getPostcode());
                intent.putExtra("city",stringList.get(position).getCity());
                intent.putExtra("other",stringList.get(position).getOther());
                intent.putExtra("phone",stringList.get(position).getPhone());
                intent.putExtra("phone_mobile",stringList.get(position).getPhone_mobile());
                context.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Delete address");
                alertDialog.setMessage("Are you sure you want to remove address?");
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GetDeleteAddress(stringList.get(position).getId());
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();

            }
        });
    }

    private void GetDeleteAddress(final String id) {
        if (detector.isConnectingToInternet()){
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Delete_Address;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        //Log.e("Delete_Address", response);
                        JSONObject object = new JSONObject(response);
                        if (!object.isNull("code")) {
                            if (object.getString("code").equals("200")){
                                WebService.MakeToast(context, "Adreess deleted successfully...");
                                context.startActivity(new Intent(context,MyAddress.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            }else {
                                WebService.MakeToast(context, "Please try again...");
                            }
                        }else {
                            WebService.MakeToast(context, "Please try again...");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_address",id);

                    return params;
                }
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(context, "No commission to display");
        }
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView fname, company, alias, address1, address2, postcode, country, phone, phone_mobile;
        LinearLayout order_layoout;
        ImageButton edit, delete;

        public ViewHolder(View itemView) {
            super(itemView);

            fname = (TextView) itemView.findViewById(R.id.fname);
            /*lname = (TextView) itemView.findViewById(R.id.lname);*/
            company = (TextView) itemView.findViewById(R.id.company);
            alias = (TextView) itemView.findViewById(R.id.alias);
            address1 = (TextView) itemView.findViewById(R.id.address1);
            address2 = (TextView) itemView.findViewById(R.id.address2);
            postcode = (TextView) itemView.findViewById(R.id.postcode);
            country = (TextView) itemView.findViewById(R.id.country);
            phone = (TextView) itemView.findViewById(R.id.phone);
            phone_mobile = (TextView) itemView.findViewById(R.id.phone_mobile);
            edit = (ImageButton) itemView.findViewById(R.id.edit);
            delete = (ImageButton) itemView.findViewById(R.id.delete);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }
}
