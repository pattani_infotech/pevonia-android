package com.pevonia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.bean.ShipingBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.ItemClickListener;
import com.pevonia.util.WebService;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by user on 3/6/2017.
 */

public class ShipingAdapter extends RecyclerView.Adapter<ShipingAdapter.ViewHolder> {
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private ArrayList<ShipingBean> stringList;
    private Context context;
    private ItemClickListener clickListener;
    LCManager lcManager;
    HashMap<String, String> lcmap;
    public ShipingAdapter(ArrayList<ShipingBean> shipingBean, Context context) {
        this.stringList = shipingBean;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        lcManager = new LCManager(context);
        lcmap = lcManager.getLC();
    }

    @Override
    public ShipingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_shiping_layout, parent, false);
        return new ShipingAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ShipingAdapter.ViewHolder holder, final int position) {

final ShipingBean bean=stringList.get(position);
        if (bean.getLogo().equals("")||bean.getLogo().equals("na")||bean.getLogo().equals("null")) {
            Glide.with(context).load(R.drawable.productimagenot).thumbnail(0.5f).into(holder.photoView);
        }else {
            Log.e("path",WebService.carrier_image_path + bean.getLogo()+"");
            Glide.with(context).load(WebService.carrier_image_path + bean.getLogo()).thumbnail(0.5f).into(holder.photoView);
        }
        holder.tv1.setText(bean.getCarrier_name());
        if (bean.getIs_free().equals("false")) {
            holder.taxprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), context) +
                    WebService.GetDecimalFormate(bean.getTotal_price_with_tax()) + " (tax excl.)");
        }else {
            holder.taxprice.setText("Free!");
        }



    }


    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final TextView tv1, taxprice;
        ImageView photoView;

        public ViewHolder(View itemView) {
            super(itemView);

            tv1 = (TextView) itemView.findViewById(R.id.tv1);
            taxprice = (TextView) itemView.findViewById(R.id.taxprice);
            photoView = (ImageView) itemView.findViewById(R.id.photoView);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
        }
    }


}
