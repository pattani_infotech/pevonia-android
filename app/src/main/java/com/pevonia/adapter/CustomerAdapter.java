package com.pevonia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.bean.CustomerBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



/**
 * Created by user on 3/6/2017.
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.ViewHolder> implements Filterable {
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private List<CustomerBean> stringList;
    private List<CustomerBean> plist;
    private Context context;
    ValueFilter valueFilter;


    public CustomerAdapter(List<CustomerBean> orderBeansList, Context context) {
        this.stringList = orderBeansList;
        this.plist = orderBeansList;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();

    }

    @Override
    public CustomerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_customer, parent, false);
        return new CustomerAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomerAdapter.ViewHolder holder, final int position) {

        holder.customername.setText("Name :"+stringList.get(position).getFname() + "  " + stringList.get(position).getLname());
        //holder.distance.setText(stringList.get(position).getDistance());
        //holder.mobileno.setText("Mobile No. :" + stringList.get(position).getMobile());

        holder.customername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent intent = new Intent(context, AgentDetails.class);
                intent.putExtra("fname", stringList.get(position).getFname());
                intent.putExtra("lname", stringList.get(position).getLname());
                intent.putExtra("email", stringList.get(position).getEmail());
                intent.putExtra("company", stringList.get(position).getCompany());
                intent.putExtra("mobile", stringList.get(position).getMobile());
                intent.putExtra("r_message", stringList.get(position).getReview());
                intent.putExtra("dp", stringList.get(position).getAgent_propic().toString());
                intent.putExtra("agent_id", stringList.get(position).getId().toString());
                //Log.e("image123", stringList.get(position).getAgent_propic());
                context.startActivity(intent);*/
            }
        });
    }


    @Override
    public int getItemCount() {
        return stringList.size();
    }

    @Override
    public Filter getFilter() {

        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView customername, mobileno;

        public ViewHolder(View itemView) {
            super(itemView);

            customername = (TextView) itemView.findViewById(R.id.customername);
            mobileno = (TextView) itemView.findViewById(R.id.mobileno);

        }
    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                List<CustomerBean> filterList = new ArrayList<CustomerBean>();
                for (int i = 0; i < plist.size(); i++) {

                    if (plist.get(i).getFname().toString().toUpperCase().contains(constraint.toString().toUpperCase())||plist.get(i).getLname().toString().toUpperCase().contains(constraint.toString().toUpperCase())
                            /*||plist.get(i).getMobile().toString().toUpperCase().contains(constraint.toString().toUpperCase())*/) {
                        CustomerBean bean = new CustomerBean();

                        bean.setId(plist.get(i).getId());
                        bean.setFname(plist.get(i).getFname());
                        bean.setLname(plist.get(i).getLname());
                        //bean.setMobile(plist.get(i).getMobile());

                        filterList.add(bean);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<CustomerBean>) results.values;
            notifyDataSetChanged();

        }
    }


}
