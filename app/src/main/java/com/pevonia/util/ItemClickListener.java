package com.pevonia.util;

import android.view.View;

/**
 * Created by ANIX on 10-02-2018.
 */

public interface ItemClickListener {

    void onClick(View view, int position);

}
