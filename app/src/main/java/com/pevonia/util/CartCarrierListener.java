package com.pevonia.util;

import com.pevonia.bean.ShipingBean;

import java.util.ArrayList;

/**
 * Created by ANIX on 10-02-2018.
 */

public interface CartCarrierListener {

    void loadCarrier(ArrayList<ShipingBean> shipingBean);

}
