package com.pevonia.util;

import com.pevonia.bean.CartProductBean;

import java.util.ArrayList;

/**
 * Created by ANIX on 10-02-2018.
 */

public interface CartProductListener {

    void loadProduct(ArrayList<CartProductBean> leaderBoards);

}
