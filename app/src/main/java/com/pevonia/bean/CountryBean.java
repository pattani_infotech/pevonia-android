package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class CountryBean {
    String id,name;

    public CountryBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }




    public CountryBean(String id, String name) {
        this.id = id;
        this.name = name;

    }
}
