package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class StateBean {
    String id,state_name;

    public StateBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getState_name() {
        return state_name;
    }
    public void setState_name(String state_name) {
        this.state_name = state_name;
    }




    public StateBean(String id, String state_name) {
        this.id = id;
        this.state_name = state_name;

    }
}
