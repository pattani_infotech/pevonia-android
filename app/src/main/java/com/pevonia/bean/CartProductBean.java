package com.pevonia.bean;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 7/10/2017.
 */

public class CartProductBean {
    private String id;
    private String price;
    private String name;
    private String qty;
    private String id_default_image;
    private String id_product_attribute;
    private String id_address_delivery;

    public String getId_default_image() {

        return id_default_image;
    }

    public void setId_default_image(String id_default_image) {
        this.id_default_image = id_default_image;
    }

    public CartProductBean() {

    }

    public String getQty() {
        return qty;
    }
    public void setQty(String qty) {
        this.qty = qty;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getId_address_delivery() {
        return id_address_delivery;
    }
    public void setId_address_delivery(String id_address_delivery) {
        this.id_address_delivery = id_address_delivery;
    }

    public String getId_product_attribute() {
        return id_product_attribute;
    }
    public void setId_product_attribute(String id_product_attribute) {
        this.id_product_attribute = id_product_attribute;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public CartProductBean(String id, String price, String name,String qty,String id_default_image,String id_product_attribute,String id_address_delivery) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.qty = qty;
        this.id_default_image = id_default_image;
        this.id_product_attribute = id_product_attribute;
        this.id_address_delivery = id_address_delivery;

    }
    @Override
    public String toString() {
      /*  Log.e("datacpb=","product_id="+id+",quantity=" + qty + ",id_product_attribute=" + id_product_attribute);
        return "product_id="+id+",quantity=" + qty + ",id_product_attribute=" + id_product_attribute;*/
        JSONObject obj = new JSONObject();
        try {
            obj.put("product_id",id);
            obj.put("quantity",qty);
            obj.put("id_product_attribute",id_product_attribute);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();
    }
}
