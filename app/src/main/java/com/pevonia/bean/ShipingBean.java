package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class ShipingBean {
    private String id,logo,carrier_name,delay,total_price_with_tax,total_price_without_tax,is_free;

    public ShipingBean() {}

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCarrier_name() {
        return carrier_name;
    }
    public void setCarrier_name(String carrier_name) {
        this.carrier_name = carrier_name;
    }

    public String getDelay() {
        return delay;
    }
    public void setDelay(String delay) {
        this.delay = delay;
    }

    public String getTotal_price_with_tax() {
        return total_price_with_tax;
    }
    public void setTotal_price_with_tax(String total_price_with_tax) {
        this.total_price_with_tax = total_price_with_tax;
    }

    public String getTotal_price_without_tax() {
        return total_price_without_tax;
    }
    public void setTotal_price_without_tax(String total_price_without_tax) {
        this.total_price_without_tax = total_price_without_tax;
    }

    public String getIs_free() {
        return is_free;
    }
    public void setIs_free(String is_free) {
        this.is_free = is_free;
    }


    public ShipingBean(String id, String logo, String carrier_name, String delay, String total_price_with_tax, String total_price_without_tax, String is_free) {
        this.id = id;
        this.logo = logo;
        this.carrier_name = carrier_name;
        this.delay = delay;
        this.total_price_with_tax = total_price_with_tax;
        this.total_price_without_tax = total_price_without_tax;
        this.is_free = is_free;

    }
}
