package com.pevonia.bean;

/**
 * Created by user on 16-Dec-17.
 */

public class AddressBean {

    private String id,country_id,state_id,alias,company,lastname,firstname,address1,address2,postcode,city,phone,phone_mobile,other;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getCountry_id() {
        return country_id;
    }
    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }
    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getAlias() {
        return alias;
    }
    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCompany() {
        return company;
    }
    public void setCompany(String company) {
        this.company = company;
    }



    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstnamee(String firstname) {
        this.firstname = firstname;
    }

    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }
    public String getPostcode() {
        return postcode;
    }
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone_mobile() {
        return phone_mobile;
    }
    public void setPhone_mobile(String phone_mobile) {
        this.phone_mobile = phone_mobile;
    }
    public String getOther() {
        return other;
    }
    public void setOther(String other) {
        this.other = other;
    }

}
