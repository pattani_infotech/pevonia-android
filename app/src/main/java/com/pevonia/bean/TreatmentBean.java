package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class TreatmentBean {
    String tname,tdisc;
    int timage;

    public TreatmentBean() {

    }


    public int getTimage() {
        return timage;
    }
    public void setTimage(int timage) {
        this.timage = timage;
    }

    public String getTname() {
        return tname;
    }
    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTdisc() {
        return tdisc;
    }
    public void setTdisc(String tdisc) {
        this.tdisc = tdisc;
    }



    public TreatmentBean(String tname,String tdisc, int timage) {
        this.timage = timage;
        this.tname = tname;
        this.tdisc = tdisc;

    }
}
