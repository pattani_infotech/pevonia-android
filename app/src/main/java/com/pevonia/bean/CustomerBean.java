package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class CustomerBean {
    String id,fname,lname,mobile;

    public CustomerBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }
    public void setLname(String lname) {
        this.lname = lname;
    }



    public CustomerBean(String id, String fname, String lname,String mobile) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.mobile = mobile;
    }
}
