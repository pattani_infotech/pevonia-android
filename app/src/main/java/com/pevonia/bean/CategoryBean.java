package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class CategoryBean {
    String id,nb_products_recursive,position,name,link_rewrite;

    public CategoryBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getNb_products_recursive() {
        return nb_products_recursive;
    }
    public void setNb_products_recursive(String nb_products_recursive) {
        this.nb_products_recursive = nb_products_recursive;
    }

    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLink_rewrite() {
        return link_rewrite;
    }
    public void setLink_rewrite(String link_rewrite) {
        this.link_rewrite = link_rewrite;
    }



    public CategoryBean(String id, String nb_products_recursive, String position, String name, String link_rewrite) {
        this.id = id;
        this.nb_products_recursive = nb_products_recursive;
        this.position = position;
        this.name = name;
        this.link_rewrite = link_rewrite;
    }
}
