package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class ChBean {
    String idc,name;

    public ChBean() {

    }


    public String getIdc() {
        return idc;
    }
    public void setIdc(String idc) {
        this.idc = idc;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }



    public ChBean(String idc,String name) {
        this.idc = idc;
        this.name = name;
    }
}
