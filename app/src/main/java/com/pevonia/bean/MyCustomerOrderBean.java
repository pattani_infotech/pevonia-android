package com.pevonia.bean;

/**
 * Created by user on 16-Dec-17.
 */

public class MyCustomerOrderBean {

    private String order_id;

    public String getCurrecy_id() {
        return currecy_id;
    }

    public void setCurrecy_id(String currecy_id) {
        this.currecy_id = currecy_id;
    }

    private String currecy_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getTotal_paid() {
        return total_paid;
    }

    public void setTotal_paid(String total_paid) {
        this.total_paid = total_paid;
    }

    public String getOrder_reference() {
        return order_reference;
    }

    public void setOrder_reference(String order_reference) {
        this.order_reference = order_reference;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    private String invoice_date;
    private String payment;
    private String total_paid;
    private String order_reference;
    private String customerName;
}
