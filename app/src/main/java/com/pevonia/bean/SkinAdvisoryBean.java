package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class SkinAdvisoryBean {
    String title;
    int skinimage;



    public int getSkinimage() {
        return skinimage;
    }
    public void setSkinimage(int skinimage) {
        this.skinimage = skinimage;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }



    public SkinAdvisoryBean(String title, int skinimage) {
        this.skinimage = skinimage;
        this.title = title;

    }
}
