package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class PerentBean{
    private String id;
    private String price;
    private String name;
    private String id_default_image,available;

    public String getId_default_image() {

        return id_default_image;
    }

    public void setId_default_image(String id_default_image) {
        this.id_default_image = id_default_image;
    }




    public PerentBean() {}


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getAvailable() {
        return available;
    }
    public void setAvailable(String available) {
        this.available = available;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public PerentBean(String id, String price,String name,String available,String id_default_image) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.available = available;
        this.id_default_image = id_default_image;

    }


}
