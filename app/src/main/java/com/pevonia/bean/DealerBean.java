package com.pevonia.bean;

/**
 * Created by user on 7/10/2017.
 */

public class DealerBean {
    String id,fname,lname;

    public DealerBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }
    public void setLname(String lname) {
        this.lname = lname;
    }



    public DealerBean(String id, String fname, String lname) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
    }
}
