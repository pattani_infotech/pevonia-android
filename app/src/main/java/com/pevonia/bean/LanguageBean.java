package com.pevonia.bean;

/**
 * Created by user on 8/15/2017.
 */

public class LanguageBean {

    private String id,name,language_code;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage_code() {
        return language_code;
    }
    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }



}
