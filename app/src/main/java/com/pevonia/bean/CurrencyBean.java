package com.pevonia.bean;

/**
 * Created by user on 8/15/2017.
 */

public class CurrencyBean {

    private String id,name,iso_code;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getIso_code() {
        return iso_code;
    }
    public void setIso_code(String iso_code) {
        this.iso_code = iso_code;
    }



}
