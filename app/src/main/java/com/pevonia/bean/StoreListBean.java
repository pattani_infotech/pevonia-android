package com.pevonia.bean;

/**
 * Created by user on 16-Dec-17.
 */

public class StoreListBean {

    private String id,store_name,city,store_image,store_rating;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getStore_name() {
        return store_name;
    }
    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getStore_image() {
        return store_image;
    }
    public void setStore_image(String store_image) {
        this.store_image = store_image;
    }

    public String getStore_rating() {
        return store_rating;
    }
    public void setStore_rating(String store_rating) {
        this.store_rating = store_rating;
    }



    public void setOrder_reference(String id,String store_name,String city,String store_image,String store_rating) {
        this.id = id;
        this.store_name = store_name;
        this.city = city;
        this.store_image = store_image;
        this.store_rating = store_rating;
    }


}
