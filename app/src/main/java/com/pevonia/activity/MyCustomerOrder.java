package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.MyCustomerOrderAdapter;
import com.pevonia.bean.MyCustomerOrderBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyCustomerOrder extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;

    ImageButton back;
    ImageView nodata;
    Button shopcontinue;
    FrameLayout framelayout;
    private RecyclerView customer_order_history; ;
    private ProgressDialog pDialog;
    List<MyCustomerOrderBean> myCustomerOrderBeen=new ArrayList<>();
    private MyCustomerOrderAdapter myCustomerorderadapte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_customer_order);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        SetUpViews();
        detector = new ConnectionDetector(MyCustomerOrder.this);
        manager = new UserSessionManager(MyCustomerOrder.this);
        map = manager.getUserDetail();
        if (detector.isConnectingToInternet()) {
            GetMyCustomerOrder();
        } else {
            WebService.MakeToast(MyCustomerOrder.this, getString(R.string.check_internet));
        }
    }

    private void GetMyCustomerOrder() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(MyCustomerOrder.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Dealer_Customer_Orders+"["+map.get(UserSessionManager.KEY_USERID).toString()+"]";
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        JSONObject object=new JSONObject(response);
                        if(!object.isNull("order")) {
                            //Log.e("Dealer_Customer_Orders",response);
                            JSONArray orderarray=object.getJSONArray("order");
                            for(int i=0;i<orderarray.length();i++) {
                                MyCustomerOrderBean myCustomerOrderBean=new MyCustomerOrderBean();
                                JSONObject object1=orderarray.getJSONObject(i);
                                myCustomerOrderBean.setOrder_id(object1.getString("id"));
                                myCustomerOrderBean.setCustomerName(object1.getString("customer_name"));
                                myCustomerOrderBean.setInvoice_date(object1.getString("invoice_date"));
                                myCustomerOrderBean.setOrder_reference(object1.getString("reference"));
                                myCustomerOrderBean.setPayment(object1.getString("payment"));
                                myCustomerOrderBean.setTotal_paid(object1.getString("total_paid"));
                                myCustomerOrderBean.setCurrecy_id(object1.getString("id_currency"));
                                myCustomerOrderBeen.add(myCustomerOrderBean);
                                //Log.e("myOrderBeen",myOrderBeen.size()+" ");
                            }
                            if (myCustomerOrderBeen.size()==0){
                                framelayout.setVisibility(View.VISIBLE);
                            }else {
                                myCustomerorderadapte = new MyCustomerOrderAdapter(myCustomerOrderBeen, MyCustomerOrder.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyCustomerOrder.this);
                                customer_order_history.setLayoutManager(mLayoutManager);
                                customer_order_history.setItemAnimator(new DefaultItemAnimator());
                                customer_order_history.setAdapter(myCustomerorderadapte);
                            }
                        }else{
                            WebService.MakeToast(MyCustomerOrder.this, "No orders to display");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyCustomerOrder.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        nodata = (ImageView)findViewById(R.id.nodata);
        customer_order_history=(RecyclerView)findViewById(R.id.customer_order_history);
        shopcontinue= (Button) findViewById(R.id.shopcontinue);
        framelayout= (FrameLayout) findViewById(R.id.framelayout);
        shopcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyCustomerOrder.this,ProductActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
    }

}
