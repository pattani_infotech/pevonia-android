package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CustomerAdapter;
import com.pevonia.bean.CustomerBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyCustomer extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
    TextView tprofile,nodata;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    EditText search;
    RecyclerView customerlist;
    List<CustomerBean> customerBean = new ArrayList<>();
    CustomerAdapter cAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_customer);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        detector = new ConnectionDetector(MyCustomer.this);
        manager = new UserSessionManager(MyCustomer.this);
        map = manager.getUserDetail();
        SetUpViews();
        //PrepareData();

        if (detector.isConnectingToInternet()) {
            GetMyCustomer();
        } else {
            WebService.MakeToast(MyCustomer.this, getString(R.string.check_internet));
        }
    }

    private void GetMyCustomer() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(MyCustomer.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.MyCustomerList+"["+map.get(UserSessionManager.KEY_USERID)+"]";
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        //Log.e("MyCustomerList",response);
                        JSONObject object=new JSONObject(response);
                        if(!object.isNull("customers")) {
                            JSONArray custarry=object.getJSONArray("customers");
                            for(int i=0;i<custarry.length();i++) {
                                JSONObject object1=custarry.getJSONObject(i);
                                //Log.e("lastname",object1.getString("lastname"));
                                //Log.e("firstname",object1.getString("firstname"));
                                CustomerBean bean=new CustomerBean();
                                bean.setFname(object1.getString("firstname"));
                                bean.setLname(object1.getString("lastname"));
                                customerBean.add(bean);

                                //Log.e("myOrderBeen",myOrderBeen.size()+" ");
                            }
                            if (customerBean.size()==0){
                                nodata.setVisibility(View.VISIBLE);
                            }else {
                                cAdapter = new CustomerAdapter(customerBean, MyCustomer.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyCustomer.this);
                                customerlist.setLayoutManager(mLayoutManager);
                                customerlist.setAdapter(cAdapter);
                            }
                        }else{
                            WebService.MakeToast(MyCustomer.this, "No orders to display");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyCustomer.this, getString(R.string.check_internet));
        }
    }

    private void PrepareData() {
        /*CustomerBean bean=new CustomerBean("1","abc","bbc","9935667788");
        customerBean.add(bean);
        bean=new CustomerBean("1","abc","bbc","9923667788");
        customerBean.add(bean);
        bean=new CustomerBean("2","def","eef","9955998999");
        customerBean.add(bean);
        bean=new CustomerBean("3","ghi","hhi","8866654422");
        customerBean.add(bean);
        bean=new CustomerBean("4","jkl","kkl","9998887776");
        customerBean.add(bean);
        bean=new CustomerBean("5","mno","nno","6677889900");
        customerBean.add(bean);
        bean=new CustomerBean("6","pqrs","qqrs","1122334455");
        customerBean.add(bean);
        bean=new CustomerBean("7","tuv","uuv","1234567890");
        customerBean.add(bean);
        bean=new CustomerBean("8","wxyz","xxyz","9925667788");
        customerBean.add(bean);
        bean=new CustomerBean("9","def","eef","9955995599");
        customerBean.add(bean);
        bean=new CustomerBean("10","ghi","hhi","8866554422");
        customerBean.add(bean);
        bean=new CustomerBean("11","jkl","kkl","9998887776");
        customerBean.add(bean);*/

    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        nodata = (TextView) findViewById(R.id.nodata);
        back = (ImageButton) toolbar.findViewById(R.id.back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        search= (EditText) findViewById(R.id.search);
        customerlist= (RecyclerView) findViewById(R.id.customerlist);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.equals("")) {
                    cAdapter.getFilter().filter(s.toString());
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }
}
