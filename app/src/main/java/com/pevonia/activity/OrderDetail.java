package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderDetail extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    private ProgressDialog pDialog;
    TextView date1, date2, status1, status2, billname, baddress, dname, daddress, reference, product, qty, unitprice, totalprice, totalproduct, totalshipping, total, date, carrier, weight, shippingcost, tracking_number,
            bphone, dphone;

    double tprice = 0;
    double scharge = 0;

    LinearLayout addStatusLayout;
    LinearLayout addOrderLayout;
    LinearLayout layout2, layout1;
    LinearLayout rrr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector = new ConnectionDetector(OrderDetail.this);
        manager = new UserSessionManager(OrderDetail.this);
        map = manager.getUserDetail();
        lcManager=new LCManager(OrderDetail.this);
        lcmap=lcManager.getLC();
        SetUpViews();
        if (getIntent().hasExtra("id")) {
            if (detector.isConnectingToInternet()) {
                GetOrderDetails();
            } else {
                WebService.MakeToast(OrderDetail.this, getString(R.string.check_internet));
            }
        }

    }

    private void GetOrderDetails() {
        pDialog = new ProgressDialog(OrderDetail.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        //final String Currency_code = WebService.GEtCurrecyById(getIntent().getStringExtra("currencycode"), OrderDetail.this);
        final String Currency_code = WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this);
        //Log.e("customerId", map.get(UserSessionManager.KEY_USERID));
        //http://www.pevonia.com.my/demo/api/order_details?filter[id_order]=[23]&display=full&output_format=JSON
        String url = WebService.OrderDetail + "[" + getIntent().getStringExtra("id") + "]&display=full&" + WebService.output_format;
        //Log.e("url", url);
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                   // Log.e("OrderDetail", fic_response + "");
                    JSONObject object = new JSONObject(fic_response);
                    if (!object.isNull("order_details")) {
                        JSONArray array = object.getJSONArray("order_details");
                        //Log.e("main array length",array.length()+"");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            RelativeLayout layout = (RelativeLayout) View.inflate(OrderDetail.this, R.layout.new_order_layout, null);
                            reference = ((TextView) layout.findViewById(R.id.reference));
                            product = ((TextView) layout.findViewById(R.id.product));
                            qty = ((TextView) layout.findViewById(R.id.qty));
                            unitprice = ((TextView) layout.findViewById(R.id.unitprice));
                            totalprice = ((TextView) layout.findViewById(R.id.totalprice));

                            reference.setText(obj.getString("product_reference"));
                            product.setText(obj.getString("product_name"));
                            qty.setText(obj.getString("product_quantity"));
                            unitprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) + WebService.GetDecimalFormate(obj.getString("unit_price_tax_incl")));
                            totalprice.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) + WebService.GetDecimalFormate(obj.getString("total_price_tax_incl")));                                   // Log.e("id",obj.getString("id"));
                            /*reference.setText(obj.getString("product_reference"));
                            product.setText(obj.getString("product_name"));
                            qty.setText(obj.getString("product_quantity"));
                            unitprice.setText(Currency_code+obj.getString("unit_price_tax_incl"));
                            totalprice.setText(Currency_code+obj.getString("total_price_tax_incl"));*/

                            //Log.e("associations",obj.getString("associations"));
                            JSONObject obj1 = new JSONObject(obj.getString("associations"));
                            JSONArray arr = obj1.getJSONArray("order_billing_address");
                            JSONObject obj2 = arr.getJSONObject(0);
                            // Log.e("order_billing_address",obj2.getString("alias"));
                            billname.setText(obj2.getString("firstname") + " " + obj2.getString("lastname"));
                            bphone.setText(obj2.getString("phone"));
                            String address = obj2.getString("address1") + "" + obj2.getString("address2") + "\n" + obj2.getString("postcode") + "\n" + obj2.getString("country") + "," + obj2.getString("state");
                            baddress.setText(address);
                            //Log.e("associations",obj.getString("associations"));
                            //JSONObject obj3 = new JSONObject(obj.getString("associations"));
                            //JSONArray arr1=obj3.getJSONArray("order_delivery_address");
                            JSONArray arr1 = obj1.getJSONArray("order_delivery_address");
                            JSONObject obj4 = arr1.getJSONObject(0);
                            //Log.e("order_delivery_address",obj4.getString("firstname"));
                            dname.setText(obj4.getString("firstname") + " " + obj4.getString("lastname"));
                            dphone.setText(obj4.getString("phone"));
                            String address2 = obj4.getString("address1") + "" + obj4.getString("address2") + "\n" + obj4.getString("postcode") + "\n" + obj4.getString("country") + "," + obj4.getString("state");
                            daddress.setText(address2);                                                                                       //String dummu=CreateJsonPAcket();
                            //Log.e("dummu",dummu+" ");
                            //JSONArray arr2 = obj1.getJSONArray("order_states");
                            //JSONArray arr2 = new JSONArray(dummu);
                            //Log.e("arr2",arr2.toString());
                            //JSONObject obj5=arr2.getJSONObject(0);
                           /* for (int j = 0; j < arr2.length(); j++) {
                                JSONObject obj5 = arr2.getJSONObject(j);
                                //JSONObject obj5 = arr2.getJSONObject(0);
                                LinearLayout layout2 = (LinearLayout) View.inflate(OrderDetail.this, R.layout.status_layout, null);
                                date1 = ((TextView) layout2.findViewById(R.id.date1));
                                status1 = ((TextView) layout2.findViewById(R.id.status1));
                                date1.setText(obj5.getString("date_add"));
                                status1.setText(obj5.getString("ostate_name"));

                                Log.e("date_add  "+j,obj5.getString("date_add"));
                                Log.e("ostate_name  "+j,obj5.getString("ostate_name"));
                                addStatusLayout.addView(layout2, j);
                            }*/
                            /*JSONObject objarray = array.getJSONObject(0);
                            JSONObject objarray1 = new JSONObject(objarray.getString("associations"));
                            JSONArray arr2 = objarray1.getJSONArray("order_states");*/

                           /* Log.e("arr2", arr2.length() + "");
                            Log.e("arr2", arr2.toString() + "");
                            for (int j = 0; j < arr2.length(); j++) {
                                JSONObject obj5 = arr2.getJSONObject(j);
                                //JSONObject obj5 = arr2.getJSONObject(0);
                                //layout1 = (LinearLayout) View.inflate(OrderDetail.this, R.layout.single_layout, null);
                                //rrr = (LinearLayout) layout1.findViewById(R.id.rrr);
                                layout2 = (LinearLayout) View.inflate(OrderDetail.this, R.layout.status_layout, null);
                                date1 = ((TextView) layout2.findViewById(R.id.date1));
                                status1 = ((TextView) layout2.findViewById(R.id.status1));
                                date1.setText(obj5.getString("date_add"));
                                status1.setText(obj5.getString("ostate_name"));
                                Log.e("date_add  " + j, obj5.getString("date_add"));
                                Log.e("ostate_name  " + j, obj5.getString("ostate_name"));
                                //layout1.addView(layout2,j);
                                addStatusLayout.addView(layout2);
                            }*/

                            //JSONObject obj6=arr2.getJSONObject(1);
                            //date2.setText(obj6.getString("date_add"));
                            //status2.setText(obj6.getString("ostate_name"));
                            JSONArray arr3 = obj1.getJSONArray("order_shipping");
                            JSONObject obj7 = arr3.getJSONObject(0);
                            date.setText(obj7.getString("date_add"));
                            carrier.setText(obj7.getString("state_name"));
                            weight.setText(obj7.getString("weight"));
                            scharge = Double.parseDouble(obj7.getString("shipping_cost"));
                            shippingcost.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) + "" + WebService.GetDecimalFormate(obj7.getString("shipping_cost")));
                            if (obj7.getString("tracking_number").equals("") || obj7.isNull("tracking_number") || obj7.getString("tracking_number").equals("null")) {
                                tracking_number.setText("-");
                            } else {
                                tracking_number.setText(obj7.getString("tracking_number"));
                            }
                            totalshipping.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) + WebService.GetDecimalFormate(String.valueOf(scharge)));
                            //tprice = (Double.parseDouble(obj.getString("total_price_tax_incl")) + scharge);
                            tprice = tprice + (Double.parseDouble(obj.getString("total_price_tax_incl")));

                            totalproduct.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) +WebService.GetDecimalFormate(String.valueOf(tprice)));
                            total.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), OrderDetail.this) +WebService.GetDecimalFormate(String.valueOf((tprice + scharge))));

                            addOrderLayout.addView(layout, i);
                        }
                        JSONObject obj = array.getJSONObject(0);
                        JSONObject obj1 = new JSONObject(obj.getString("associations"));
                        JSONArray arr2 = obj1.getJSONArray("order_states");
                        for (int j = 0; j < arr2.length(); j++){
                            JSONObject obj5 = arr2.getJSONObject(j);
                            //JSONObject obj5 = arr2.getJSONObject(0);
                            /*layout1 = (LinearLayout) View.inflate(OrderDetail.this, R.layout.single_layout, null);
                              rrr = (LinearLayout) layout1.findViewById(R.id.rrr);*/
                            layout2 = (LinearLayout) View.inflate(OrderDetail.this, R.layout.status_layout, null);
                            date1 = ((TextView) layout2.findViewById(R.id.date1));
                            status1 = ((TextView) layout2.findViewById(R.id.status1));
                            date1.setText(obj5.getString("date_add"));
                            status1.setText(obj5.getString("ostate_name"));
                            //Log.e("date_add  " + j, obj5.getString("date_add"));
                            //Log.e("ostate_name  " + j, obj5.getString("ostate_name"));
                            //layout1.addView(layout2,j);
                            addStatusLayout.addView(layout2);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private String CreateJsonPAcket() {
        String json = null;

        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            JSONObject object = new JSONObject();
            try {
                object.put("date_add", "2017-12-20 20:55:09");
                object.put("ostate_name", "Preparation in progress");
                jsonArray.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        json = jsonArray.toString();
        return json;
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        tprofile.setText("Order Detail");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //date1 = (TextView) findViewById(R.id.date1);
        //status1 = (TextView) findViewById(R.id.status1);
        //date2 = (TextView) findViewById(R.id.date2);
        //status2 = (TextView) findViewById(R.id.status2);

        billname = (TextView) findViewById(R.id.billname);
        baddress = (TextView) findViewById(R.id.baddress);
        bphone = (TextView) findViewById(R.id.bphone);

        dname = (TextView) findViewById(R.id.dname);
        daddress = (TextView) findViewById(R.id.daddress);
        dphone = (TextView) findViewById(R.id.dphone);

        date = (TextView) findViewById(R.id.date);
        carrier = (TextView) findViewById(R.id.carrier);
        weight = (TextView) findViewById(R.id.weight);
        shippingcost = (TextView) findViewById(R.id.shippingcost);
        tracking_number = (TextView) findViewById(R.id.tracking_number);

        //reference = (TextView) findViewById(R.id.reference);
        //product = (TextView) findViewById(R.id.product);
        //qty = (TextView) findViewById(R.id.qty);
        //unitprice = (TextView) findViewById(R.id.unitprice);
        //totalprice = (TextView) findViewById(R.id.totalprice);

        totalproduct = (TextView) findViewById(R.id.totalproduct);
        totalshipping = (TextView) findViewById(R.id.totalshipping);
        total = (TextView) findViewById(R.id.total);

        addStatusLayout = (LinearLayout) findViewById(R.id.addStatusLayout);
        addOrderLayout = (LinearLayout) findViewById(R.id.addOrderLayout);

    }
}
