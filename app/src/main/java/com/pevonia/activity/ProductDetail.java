package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.pevonia.R;
import com.pevonia.adapter.CartProAdapter;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProductDetail extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile, proname, qty, price, description;
    ImageButton back;
    ImageView proimg;
    Button share, wishlist, addtocart, buynow;
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String, String> lcmap;
    private ProgressDialog pDialog;
    private LinearLayout shop_layout;
    public static CartProAdapter cpAdapter;
    public static String comanid = "";
Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext=ProductDetail.this;
        cd = new ConnectionDetector(ProductDetail.this);
        manager = new UserSessionManager(ProductDetail.this);
        map = manager.getUserDetail();
        lcManager = new LCManager(ProductDetail.this);
        lcmap = lcManager.getLC();

        SetUpViews();
        Intent intent = getIntent();
        if (intent.hasExtra("pname")) {
            //tprofile.setText(intent.getStringExtra("pname"));
            if (intent.getStringExtra("available").toString().equals("1")) {
                price.setVisibility(View.VISIBLE);
            } else {
                price.setVisibility(View.GONE);
            }
            price.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), ProductDetail.this) + WebService.GetDecimalFormate(intent.getStringExtra("price")));
            //Log.e("img",intent.getStringExtra("imgurl").toString());
            if (intent.getStringExtra("imgurl").toString().equals("") || intent.getStringExtra("imgurl").toString().equals("na") || intent.getStringExtra("imgurl").toString().equals("null")) {
                Glide.with(ProductDetail.this).load(R.drawable.not_available)
                        .thumbnail(0.5f)
                        .into(proimg);
            } else {
                Glide.with(ProductDetail.this).load(intent.getStringExtra("imgurl").toString())
                        .thumbnail(0.5f)
                        .into(proimg);
            }
            //Log.e("image detail",intent.getStringExtra("imgurl").toString());
            try {
                JSONArray namearray = new JSONArray(intent.getStringExtra("pname"));
                if (lcmap.get(LCManager.languageID) != null) {
                    for (int i = 0; i < namearray.length(); i++) {
                        JSONObject o = namearray.getJSONObject(i);
                        //Log.e("aryname", "" + namearray.toString());
                        if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                            tprofile.setText(o.getString("value"));
                            proname.setText(o.getString("value"));
                        }
                    }
                } else {
                    JSONObject o = namearray.getJSONObject(0);
                    tprofile.setText(o.getString("value"));
                    proname.setText(o.getString("value"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //Log.e("123id",getIntent().getStringExtra("id"));
        if (cd.isConnectingToInternet()) {
            GetProductDetails();
        } else {
            WebService.MakeToast(ProductDetail.this, getString(R.string.check_internet));
        }

    }

    private void GetProductDetails() {
        pDialog = new ProgressDialog(ProductDetail.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = WebService.productDetail + getIntent().getStringExtra("id") + "/&" + WebService.output_format;
        //Log.e("Productdetail url:",url);
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    //Log.e("pro detail", fic_response + "");
                    JSONObject obj = new JSONObject(fic_response);
                    //Log.e("detaillength", obj.length() + "");
                    JSONObject obj1 = obj.getJSONObject("product");
                    JSONArray ary = obj1.getJSONArray("description");

                    if (obj1.has("available_for_order")) {
                        if (obj1.getString("available_for_order").equalsIgnoreCase("1")) {
                            shop_layout.setVisibility(View.VISIBLE);
                        } else {
                            shop_layout.setVisibility(View.GONE);
                        }
                    }
                    if (lcmap.get(LCManager.languageID) != null) {
                        for (int i = 0; i < ary.length(); i++) {
                            JSONObject obj2 = ary.getJSONObject(i);
                            if (obj2.getString("id").equals(lcmap.get(LCManager.languageID))) {
                                if (Build.VERSION.SDK_INT >= 24) {
                                    description.setText(Html.fromHtml(obj2.getString("value"), Html.FROM_HTML_MODE_LEGACY));
                                } else {
                                    description.setText(Html.fromHtml(obj2.getString("value")));
                                }
                            }
                        }
                    } else {
                        JSONObject obj2 = ary.getJSONObject(0);
                        if (obj2.getString("id").equals(lcmap.get(LCManager.languageID))) {
                            if (Build.VERSION.SDK_INT >= 24) {
                                description.setText(Html.fromHtml(obj2.getString("value"), Html.FROM_HTML_MODE_LEGACY));
                            } else {
                                description.setText(Html.fromHtml(obj2.getString("value")));
                            }
                        }
                    }

                    /*for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj2 = ary.getJSONObject(1);
                        Log.e("dddt ",obj2.getString("id"));
                        description.setText(Html.fromHtml(obj2.getString("value")));
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }


    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        proname = (TextView) findViewById(R.id.proname);
        description = (TextView) findViewById(R.id.description);
        proimg = (ImageView) findViewById(R.id.proimg);
        qty = (TextView) findViewById(R.id.qty);
        price = (TextView) findViewById(R.id.price);
        share = (Button) findViewById(R.id.share);
        wishlist = (Button) findViewById(R.id.wishlist);
        addtocart = (Button) findViewById(R.id.addtocart);
        buynow = (Button) findViewById(R.id.buynow);
        shop_layout = (LinearLayout) findViewById(R.id.shop);

    }

    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.share:
                Intent imageIntent = new Intent(Intent.ACTION_SEND);
                String url = WebService.shareProduct + getIntent().getStringExtra("id");
                imageIntent.setType("text/plain");
                imageIntent.putExtra(Intent.EXTRA_TEXT, url);
                startActivity(Intent.createChooser(imageIntent, "send"));
                break;
            case R.id.wishlist:
                break;
            case R.id.addtocart:
                //UserLoginCheck();
                if (map.get(UserSessionManager.KEY_USERID) != null) {
                    AddToCartProduct();
                } else {
                    WebService.MakeToast(ProductDetail.this, "Please user login first...");
                    Intent intent = new Intent(ProductDetail.this, Login.class);
                    startActivity(intent);
                    finish();
                }
                break;
            case R.id.buynow:
                UserLoginCheck();
                break;
        }
    }

    private void UserLoginCheck() {
       /* Log.e("id_currency", "" + lcmap.get(LCManager.currencyID));
        Log.e("id_customer", "" + map.get(UserSessionManager.KEY_USERID));
        Log.e("id_lang", "" + lcmap.get(LCManager.languageID));
        Log.e("id_product ", "" + getIntent().getStringExtra("id").toString());
        Log.e("id_product_attribute ", "0");
        Log.e("id_address_delivery ", "0");
        Log.e("quantity ", "1");
        if (map.get(UserSessionManager.KEY_USERID) != null) {
//WebService.MakeToast(ProductDetail.this,WebService.firstname+"\n"+WebService.lastname);
            CartProductBean bean = new CartProductBean();
            bean.setId(getIntent().getStringExtra("id").toString());
            bean.setPrice(getIntent().getStringExtra("price"));
            bean.setName(getIntent().getStringExtra("pname"));
            bean.setQty("1");
            bean.setId_default_image(getIntent().getStringExtra("imgurl"));
            *//*cpBean.add(bean);
            WebService.MakeToast(ProductDetail.this, "Add to Cart Sucessfully");*//*
            if (cpBean.size() == 0) {
                cpBean.add(bean);
                WebService.MakeToast(ProductDetail.this, "Add to Cart Sucessfully");
            } else {
                for (int i = 0; i < cpBean.size(); i++) {
                    Log.e("c id : ", cpBean.get(i).getId());
                    Log.e("123", getIntent().getStringExtra("id"));
                    if (getIntent().getStringExtra("id").equals(cpBean.get(i).getId())) {
                        Log.e("if " + getIntent().getStringExtra("id"), cpBean.get(i).getId());
                        comanid = cpBean.get(i).getId();
                        Log.e("456", comanid);
                        //WebService.MakeToast(ProductDetail.this,"Products already exist...");
                    } *//*else {
                        Log.e("else " + getIntent().getStringExtra("id"), cpBean.get(i).getId());
                    }*//*
                }
                Log.e("comanid", comanid);
                if (comanid.toString().equals(getIntent().getStringExtra("id"))) {
                    WebService.MakeToast(ProductDetail.this, "Products already exist...");
                } else {
                    cpBean.add(bean);
                    WebService.MakeToast(ProductDetail.this, "Add to Cart Sucessfully");
                }
            }
            cpAdapter = new CartProAdapter(cpBean, ProductDetail.this);
            cpAdapter.notifyDataSetChanged();
            //WebService.CountAdapter(cpAdapter.getCount(),ProductDetail.this);
            if (cpAdapter.getCount() == 0) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Navigation.counttext.setBackgroundColor(ContextCompat.getColor(getApplication(),R.color.trans));
                }else {
                    Navigation.counttext.setBackgroundColor(ContextCompat.getColor(getApplication(),R.color.trans));
                }
                //Navigation.counttext.setBackgroundColor(getApplication().getResources().getColor(R.color.trans, null));
                Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getApplication()) + "0.00");
                Navigation.cancel.setText("Your cart is empty...");
                Navigation.confirm.setVisibility(View.GONE);
                Cart.nodata.setVisibility(View.VISIBLE);
                Cart.withshipping.setVisibility(View.GONE);
                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getApplication()) + "0.00");
                paypalAmount = "0.00";
            } else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Navigation.counttext.setBackground(getApplication().getResources().getDrawable(R.drawable.round, null));
                }else {
                    Navigation.counttext.setBackground(ContextCompat.getDrawable(getApplication(),R.drawable.round));
                }
                //Navigation.counttext.setBackground(getApplication().getResources().getDrawable(R.drawable.round, null));
                Navigation.counttext.setText("" + cpAdapter.getCount());
                Navigation.cancel.setText("cancel Order");
                Navigation.confirm.setVisibility(View.VISIBLE);
            }
            Log.e("count", cpAdapter.getCount() + "");
            //WebService.MakeToast(ProductDetail.this, "Add to Cart Sucessfully");
            onBackPressed();

        } else {
            WebService.MakeToast(ProductDetail.this, "Please user login first...");
            Intent intent = new Intent(ProductDetail.this, Login.class);
            startActivity(intent);
            finish();
        }*/
    }

    private void AddToCartProduct() {
        if (cd.isConnectingToInternet()) {
            pDialog = new ProgressDialog(ProductDetail.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            if (WebService.CartID.equals("")) {
                pDialog.show();
                String url = WebService.Add_to_Cart;
                StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            String fic_response = WebService.fixEncoding(response);
                            Log.e("Add_to_Cart", "" + fic_response);
                            if (fic_response != null) {
                                JSONObject obj = new JSONObject(fic_response);
                                JSONObject obj1 = new JSONObject(obj.getString("cart"));
                                Log.e("obj1", "" + obj1.toString());
                                Log.e("id", "" + obj1.getString("id"));
                                WebService.CartID = obj1.getString("id");
                                JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                                Log.e("cart_rows",""+associationsObj.getString("cart_rows"));
                                JSONObject cartobj=new JSONObject(associationsObj.getString("cart_rows"));
                                JSONObject cartobj1=new JSONObject(cartobj.getString("cart_row"));
                                Log.e("id_product",cartobj1.getString("id_product")+"");

                                WebService.MakeToast(ProductDetail.this, "Add to cart successfull...");
                                ProductDetail.this.finish();
                                ((Navigation) mContext).refresh();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                //Log.e("ERROR", "error => " + error.toString());
                                pDialog.dismiss();
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("id_currency", lcmap.get(LCManager.currencyID));
                        params.put("id_customer", "" + map.get(UserSessionManager.KEY_USERID));
                        params.put("id_lang", "" + lcmap.get(LCManager.languageID));
                        params.put("id_product", "" + getIntent().getStringExtra("id").toString());
                        params.put("id_product_attribute", "0");
                        params.put("id_address_delivery", "0");
                        params.put("quantity", "1");
                        params.put("id_shop_group", map.get(UserSessionManager.KEY_shope_group));
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = WebService.SetAuth();
                        //Log.e("params", params + "");
                        return params;
                    }
                };
                loginreq.setRetryPolicy(new DefaultRetryPolicy(
                        10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                // Adding request to request queue
                PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
            }else {
                pDialog.show();
                String url = WebService.Edit_Cart+WebService.CartID;
                StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        try {
                            String fic_response = WebService.fixEncoding(response);
                            Log.e("Edit_Cart", "" + fic_response);
                            if (fic_response != null) {
                                //startActivity(new Intent(ProductDetail.this,ProductActivity.class));
                                JSONObject obj = new JSONObject(fic_response);
                                JSONObject obj1 = new JSONObject(obj.getString("cart"));
                                JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                                Log.e("cart_rows",""+associationsObj.getString("cart_rows"));
                                JSONObject cartobj=new JSONObject(associationsObj.getString("cart_rows"));
                                if (cartobj.getString("cart_row").trim().charAt(0) == '{') {

                                    WebService.MakeToast(ProductDetail.this, "Add to cart successfull...");
                                    ProductDetail.this.finish();
                                    ((Navigation) mContext).refresh();
                                }else {
                                    JSONArray ary = cartobj.getJSONArray("cart_row");
                                    Log.e("size", ary.length() + "");
                               /* if (ary.length()==0){
                                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        Navigation.counttext.setBackgroundColor(ProductDetail.this.getResources().getColor(R.color.trans));
                                    }else {
                                        Navigation.counttext.setBackgroundColor(ContextCompat.getColor(ProductDetail.this,R.color.trans));
                                    }
                                    Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID),ProductDetail.this)+"0.00");
                                    Navigation.cancel.setText("Your cart is empty...");
                                    Navigation.confirm.setVisibility(View.GONE);
                                }else {
                                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        Navigation.counttext.setBackground(ContextCompat.getDrawable(ProductDetail.this,R.drawable.round));
                                    }else {
                                        Navigation.counttext.setBackground(ContextCompat.getDrawable(ProductDetail.this,R.drawable.round));
                                    }
                                    Navigation.counttext.setText(""+ary.length());
                                    Navigation.cancel.setText("cancel Order");
                                    Navigation.confirm.setVisibility(View.VISIBLE);
                                }*/
                                    WebService.MakeToast(ProductDetail.this, "Add to cart successfull...");
                                    ProductDetail.this.finish();
                                    ((Navigation) mContext).refresh();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                //Log.e("ERROR", "error => " + error.toString());
                                pDialog.dismiss();
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("id_product", "" + getIntent().getStringExtra("id").toString());
                        params.put("id_product_attribute", "0");
                        params.put("quantity", "1");
                        return params;
                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = WebService.SetAuth();
                        return params;
                    }
                };
                loginreq.setRetryPolicy(new DefaultRetryPolicy(
                        10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
            }
        } else {
            WebService.MakeToast(ProductDetail.this, getString(R.string.check_internet));
        }
    }


}
