package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.AddressAdapter;
import com.pevonia.bean.AddressBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.ItemClickListener;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyAddress extends AppCompatActivity implements ItemClickListener {
    Toolbar toolbar;
    ImageButton back,add;
    TextView tprofile, nodata;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog, pcDialog;
    RecyclerView address_list;
    List<AddressBean> addressBean = new ArrayList<>();
    private AddressAdapter addressAdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SetUpViews();
        detector = new ConnectionDetector(MyAddress.this);
        manager = new UserSessionManager(MyAddress.this);
        map = manager.getUserDetail();
        if (detector.isConnectingToInternet()) {
            GetAddress();
        } else {
            WebService.MakeToast(MyAddress.this, getString(R.string.check_internet));
        }

    }

    private void GetAddress() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(MyAddress.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Customer_Address + "[" + map.get(UserSessionManager.KEY_USERID) + "]&filter[deleted]=0&"+WebService.output_format;
            Log.e("123",url+"");
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    Log.e("Customer_Address_array", response);
                    if (response.trim().charAt(0) == '[') {
                        //Log.e("Response is : " , "JSONArray");
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            if (jsonArray.length() <= 0) {
                                nodata.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else if (response.trim().charAt(0) == '{') {
                        try {
                            Log.e("Customer_Address_object", response);
                            JSONObject object = new JSONObject(response);
                            if (!object.isNull("addresses")) {
                                JSONArray addressesarray = object.getJSONArray("addresses");
                                for (int i = 0; i < addressesarray.length(); i++) {
                                    JSONObject object1 = addressesarray.getJSONObject(i);
                                    AddressBean bean = new AddressBean();
                                    bean.setId(object1.getString("id"));
                                    bean.setCountry_id(object1.getString("id_country"));
                                    bean.setState_id(object1.getString("id_state"));
                                    bean.setAlias(object1.getString("alias"));
                                    bean.setCompany(object1.getString("company"));
                                    bean.setLastname(object1.getString("lastname"));
                                    bean.setFirstnamee(object1.getString("firstname"));
                                    bean.setAddress1(object1.getString("address1"));
                                    bean.setAddress2(object1.getString("address2"));
                                    bean.setPostcode(object1.getString("postcode"));
                                    bean.setCity(object1.getString("city"));
                                    bean.setOther(object1.getString("other"));
                                    bean.setPhone(object1.getString("phone"));
                                    bean.setPhone_mobile(object1.getString("phone_mobile"));
                                    addressBean.add(bean);
                                }
                                if (addressBean.size() == 0) {
                                    nodata.setVisibility(View.VISIBLE);
                                } else {
                                    addressAdp = new AddressAdapter(addressBean, MyAddress.this);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyAddress.this);
                                    address_list.setLayoutManager(mLayoutManager);
                                    address_list.setItemAnimator(new DefaultItemAnimator());
                                    address_list.setAdapter(addressAdp);
                                    addressAdp.setClickListener(MyAddress.this);
                                }
                            } else {
                                WebService.MakeToast(MyAddress.this, "No address to display");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyAddress.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        add = (ImageButton) toolbar.findViewById(R.id.add);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        nodata = (TextView) findViewById(R.id.nodata);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        address_list = (RecyclerView) findViewById(R.id.address_list);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyAddress.this,EditAddress.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view, int position) {
Log.e("position adapter",position+"");
        final AddressBean address = addressBean.get(position);
        Log.e("name",address.getId()+" "+address.getFirstname());
        Intent mIntent = new Intent();
        mIntent.putExtra("addressId", address.getId());
        setResult(RESULT_OK, mIntent);
        finish();
    }
}
