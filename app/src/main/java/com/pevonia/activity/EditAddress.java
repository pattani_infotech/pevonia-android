package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CountryAdapter;
import com.pevonia.adapter.StateAdapter;
import com.pevonia.bean.CountryBean;
import com.pevonia.bean.StateBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditAddress extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
    TextView tprofile;
    EditText fname, lname, company, alias, address1, address2, postcode, city, phone, phone_mobile, other;
    Spinner country, state;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog, p1Dialog, p2Dialog, p3Dialog;
    Button addaddress, update;
    private int index, indexstate;
    List<CountryBean> countryBean = new ArrayList<>();
    List<StateBean> stateBean = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        SetUpViews();
        detector = new ConnectionDetector(EditAddress.this);
        manager = new UserSessionManager(EditAddress.this);
        map = manager.getUserDetail();
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            fname.setText(map.get(UserSessionManager.KEY_fname));
            lname.setText(map.get(UserSessionManager.KEY_lname));
        }
        if (detector.isConnectingToInternet()) {
            GetSpinnerCountry();
        } else {
            WebService.MakeToast(EditAddress.this, getString(R.string.check_internet));
        }
        if (getIntent().hasExtra("edit")) {
            fname.setText(getIntent().getStringExtra("firstname"));
            lname.setText(getIntent().getStringExtra("lastname"));
            company.setText(getIntent().getStringExtra("company"));
            alias.setText(getIntent().getStringExtra("alias"));
            address1.setText(getIntent().getStringExtra("address1"));
            address2.setText(getIntent().getStringExtra("address2"));
            postcode.setText(getIntent().getStringExtra("postcode"));
            city.setText(getIntent().getStringExtra("city"));
            phone.setText(getIntent().getStringExtra("phone"));
            phone_mobile.setText(getIntent().getStringExtra("phone_mobile"));
            other.setText(getIntent().getStringExtra("other"));
            WebService.country_id = getIntent().getStringExtra("id_country");
            WebService.state_id = getIntent().getStringExtra("id_state");
            addaddress.setVisibility(View.GONE);
            update.setVisibility(View.VISIBLE);
        }
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                WebService.country_id = countryBean.get(i).getId();
                if (!WebService.country_id.equals("0")) {
                    GetSpinnerState(WebService.country_id);
                } else {
                    stateBean.clear();
                    StateBean cbean = new StateBean();
                    cbean.setId("0");
                    cbean.setState_name("Select state");
                    stateBean.add(cbean);
                    StateAdapter sAdapter = new StateAdapter(EditAddress.this, stateBean);
                    state.setAdapter(sAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                WebService.state_id = stateBean.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void GetSpinnerState(String country_id) {
        if (detector.isConnectingToInternet()) {

            p1Dialog = new ProgressDialog(EditAddress.this);
            p1Dialog.setMessage("Loading...");
            p1Dialog.setCancelable(false);
            p1Dialog.show();
            String url = WebService.State_list + "[" + country_id + "]&filter[active]=1&display=[id,name]&" + WebService.output_format;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    p1Dialog.dismiss();

                    try {
                        stateBean.clear();
                        //Log.e("State_list", response);
                        JSONObject object = new JSONObject(response);
                        StateBean cbean = new StateBean();
                        cbean.setId("0");
                        cbean.setState_name("Select State");
                        stateBean.add(cbean);
                        if (!object.isNull("states")) {
                            JSONArray array = object.getJSONArray("states");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                StateBean bean = new StateBean();
                                //Log.e("id", object1.getString("id"));
                                //Log.e("name", object1.getString("name"));
                                if (getIntent().hasExtra("edit")) {
                                    if (getIntent().getStringExtra("id_state").toString().equals(object1.getString("id"))) {
                                        indexstate = i;
                                    }
                                }
                                bean.setId(object1.getString("id"));
                                bean.setState_name(object1.getString("name"));
                                /*URLEncoder.encode(object1.getString("name"))
                                bean.setState_name(URLDecoder.decode( object1.getString("name"), "UTF-8" ));*/
                                stateBean.add(bean);
                            }
                            StateAdapter sAdapter = new StateAdapter(EditAddress.this, stateBean);
                            state.setAdapter(sAdapter);
                            if (getIntent().hasExtra("edit")) {
                                //Log.e("getIntent if", index + " ");
                                state.setSelection(indexstate + 1);
                            }
                        } else {
                            WebService.MakeToast(EditAddress.this, "No Country to display");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            p1Dialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(EditAddress.this, getString(R.string.check_internet));
        }
    }

    private void GetSpinnerCountry() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(EditAddress.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Country_list;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();

                    try {
                        //Log.e("Country_list", response);
                        JSONObject object = new JSONObject(response);
                        CountryBean cbean = new CountryBean();
                        cbean.setId("0");
                        cbean.setName("Select countries");
                        countryBean.add(cbean);

                        if (!object.isNull("countries")) {
                            JSONArray array = object.getJSONArray("countries");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                CountryBean bean = new CountryBean();
                                //Log.e("id", object1.getString("id"));
                                //Log.e("zip_code_format", object1.getString("zip_code_format"));
                                JSONArray namearray = new JSONArray(object1.getString("name"));
                                JSONObject o = namearray.getJSONObject(0);
                                if (getIntent().hasExtra("edit")) {
                                    if (getIntent().getStringExtra("id_country").toString().equals(object1.getString("id"))) {
                                        index = i;
                                    }
                                }
                                //Log.e("name", o.getString("value"));
                                bean.setId(object1.getString("id"));
                                bean.setName(o.getString("value"));
                                countryBean.add(bean);
                            }

                        } else {
                            WebService.MakeToast(EditAddress.this, "No country to display");
                        }
                        CountryAdapter cAdapter = new CountryAdapter(EditAddress.this, countryBean);
                        country.setAdapter(cAdapter);
                        if (getIntent().hasExtra("edit")) {
                            //Log.e("getIntent if", index + " ");
                            country.setSelection(index + 1);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(EditAddress.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        country = (Spinner) findViewById(R.id.country);
        state = (Spinner) findViewById(R.id.state);
        addaddress = (Button) findViewById(R.id.addaddress);
        update = (Button) findViewById(R.id.update);
        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        company = (EditText) findViewById(R.id.company);
        alias = (EditText) findViewById(R.id.alias);
        address1 = (EditText) findViewById(R.id.address1);
        address2 = (EditText) findViewById(R.id.address2);
        postcode = (EditText) findViewById(R.id.postcode);
        city = (EditText) findViewById(R.id.city);
        phone = (EditText) findViewById(R.id.phone);
        phone_mobile = (EditText) findViewById(R.id.phone_mobile);
        other = (EditText) findViewById(R.id.other);
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddressAddProgress();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddressAddProgress();
            }
        });
    }

    private void AddressAddProgress() {
        if (fname.getText().toString().equals("")) {
            fname.setError("Type your first name...");
        } else if (lname.getText().toString().equals("")) {
            lname.setError("Type your last name...");
        } else if (alias.getText().toString().equals("")) {
            alias.setError("Type your alias text...");
        } else if (address1.getText().toString().equals("") && address2.getText().toString().equals("")) {
            address1.setError("Please type any one address");
        } else if (postcode.getText().toString().equals("")) {
            postcode.setError("Type postal code...");
        } else if (city.getText().toString().equals("")) {
            city.setError("Type your city...");
        } else if (phone.getText().toString().length()<10 && phone_mobile.getText().toString().length()<10) {
            WebService.MakeToast(EditAddress.this, "Please type any contact number...");
           /* if (phone.getText().toString().equals("")&&!phone_mobile.getText().toString().equals("")){
                if (phone_mobile.getText().toString().startsWith("0") || phone_mobile.getText().toString().startsWith("+") ||
                        phone_mobile.getText().toString().startsWith("+91") || phone_mobile.getText().toString().length() != 10){
                    phone_mobile.setError("Please type valid Contact number...");
                }
            }else if (!phone.getText().toString().equals("")&&phone_mobile.getText().toString().equals("")){
                if (phone.getText().toString().startsWith("0") || phone.getText().toString().startsWith("+") ||
                        phone.getText().toString().startsWith("+91") || phone.getText().toString().length() != 10){
                    phone.setError("Please type valid Contact number...");
                }*/

        }else if (country.getSelectedItemPosition() == 0) {
            WebService.MakeToast(EditAddress.this, "Please select countries");
        } else if (state.getSelectedItemPosition() == 0) {
            WebService.MakeToast(EditAddress.this, "Please select state");
        } else {

            /*Log.e("Id_customer ", map.get(UserSessionManager.KEY_USERID));
            Log.e("Id_country  ", WebService.country_id);
            Log.e("Id_state   ", WebService.state_id);
            Log.e("Alias ", alias.getText().toString());
            Log.e("Company ", company.getText().toString());
            Log.e("Lastname ", fname.getText().toString());
            Log.e("Firstname ", lname.getText().toString());
            Log.e("Address1  ", address1.getText().toString());
            Log.e("Address2 ", address2.getText().toString());
            Log.e("Postcode ", postcode.getText().toString());
            Log.e("City ", city.getText().toString());
            Log.e("Other  ", other.getText().toString());
            Log.e("Phone  ", phone.getText().toString());
            Log.e("Phone_mobile  ", phone_mobile.getText().toString());*/
            if (getIntent().hasExtra("edit")) {
                GetUpdateAddress();
            } else {
                GetAddAddress();
            }

        }
    }

    private void GetUpdateAddress() {
        if (detector.isConnectingToInternet()) {
            p3Dialog = new ProgressDialog(EditAddress.this);
            p3Dialog.setMessage("Loading...");
            p3Dialog.setCancelable(false);
            p3Dialog.show();
            String url = WebService.Edit_Address;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    p3Dialog.dismiss();
                    try {
                        //Log.e("Edit_Address", response);
                        JSONObject object = new JSONObject(response);
                        if (!object.isNull("code")) {
                            if (object.getString("code").equals("200")) {
                                WebService.MakeToast(EditAddress.this, "Adreess updated successfully...");
                                startActivity(new Intent(EditAddress.this, MyAddress.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            } else {
                                WebService.MakeToast(EditAddress.this, "Please try again...");
                            }
                        } else {
                            WebService.MakeToast(EditAddress.this, "Please try again...");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            p3Dialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id", getIntent().getStringExtra("id"));
                    params.put("id_customer", map.get(UserSessionManager.KEY_USERID));
                    params.put("id_country", WebService.country_id);
                    params.put("id_state", WebService.state_id);
                    params.put("alias", alias.getText().toString());
                    params.put("company", company.getText().toString());
                    params.put("lastname", lname.getText().toString());
                    params.put("firstname", fname.getText().toString());
                    params.put("address1", address1.getText().toString());
                    params.put("address2", address2.getText().toString());
                    params.put("postcode", postcode.getText().toString());
                    params.put("city", city.getText().toString());
                    params.put("other", other.getText().toString());

                    params.put("phone", WebService.FormatNumber(phone.getText().toString()));
                    params.put("phone_mobile", WebService.FormatNumber(phone_mobile.getText().toString()));

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(EditAddress.this, getString(R.string.check_internet));
        }
    }

    private void GetAddAddress() {
        if (detector.isConnectingToInternet()) {
            p2Dialog = new ProgressDialog(EditAddress.this);
            p2Dialog.setMessage("Loading...");
            p2Dialog.setCancelable(false);
            p2Dialog.show();
            String url = WebService.Add_Address;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    p2Dialog.dismiss();
                    try {
                        //Log.e("Add_Address", response);
                        JSONObject object = new JSONObject(response);
                        if (!object.isNull("code")) {
                            if (object.getString("code").equals("200")) {
                                WebService.MakeToast(EditAddress.this, "Adreess added successfully...");
                                startActivity(new Intent(EditAddress.this, MyAddress.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            } else {
                                WebService.MakeToast(EditAddress.this, "Please try again...");
                            }
                        } else {
                            WebService.MakeToast(EditAddress.this, "Please try again...");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            p2Dialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_customer", map.get(UserSessionManager.KEY_USERID));
                    params.put("id_country", WebService.country_id);
                    params.put("id_state", WebService.state_id);
                    params.put("alias", alias.getText().toString());
                    params.put("company", company.getText().toString());
                    params.put("lastname", lname.getText().toString());
                    params.put("firstname", fname.getText().toString());
                    params.put("address1", address1.getText().toString());
                    params.put("address2", address2.getText().toString());
                    params.put("postcode", postcode.getText().toString());
                    params.put("city", city.getText().toString());
                    params.put("other", other.getText().toString());
                    params.put("phone", phone.getText().toString());
                    params.put("phone_mobile", phone_mobile.getText().toString());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(EditAddress.this, getString(R.string.check_internet));
        }
    }
}
