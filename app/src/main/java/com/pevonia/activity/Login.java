package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    EditText username, password;
    RelativeLayout create;
    TextView forgot;
    Button login,skip;
    private RadioGroup rg;
    private RadioButton acctype,customer,dealer;
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    String loginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        connectionDetector = new ConnectionDetector(Login.this);
        manager = new UserSessionManager(Login.this);
        map = manager.getUserDetail();
        SetupView();
        WebService.GetCurrentLanguage();


        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginprocess();
                }
                return false;
            }
        });
        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;
            }
        });
    }

    private void loginprocess() {
         //startActivity(new Intent(Login.this,Navigation.class));
        int selectedId = rg.getCheckedRadioButtonId();
        acctype = (RadioButton) findViewById(selectedId);
        if (username.getText().toString().length() <= 0) {
            username.setError(getString(R.string.enter_unam));
        } else if (password.getText().toString().length() <= 0) {
            username.setError(getString(R.string.enter_pwd));
        }/*else if(rg.getCheckedRadioButtonId() == -1) {
            WebService.MakeToast(Login.this,"Please select Account type");
        }*/ else {
            if (connectionDetector.isConnectingToInternet()) {
                GetLogin();
            } else {
                WebService.MakeToast(Login.this, getString(R.string.check_internet));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void GetLogin() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
     // String url = WebService.product;
       String url = WebService.customer_login + "filter[email]=" + username.getText().toString().trim() + "&filter[passwd]=" + WebService.ConverToMD5(WebService.cookie_key + password.getText().toString());
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
               Log.e("customer_login", ""+ response);
                if(response.trim().charAt(0) == '[') {
                    //Log.e("Response is : " , "JSONArray");
                    try {
                        JSONArray jsonArray =new JSONArray(response);
                        if(jsonArray.length()<=0) {
                            WebService.MakeToast(Login.this,"Login error please try again...");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if(response.trim().charAt(0) == '{') {
                    //Log.e("Response is : ", "JSONObject");
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("customers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            //Log.e("customer_login", jsonArray.toString());
                            //Toast.makeText(Login.this, "id : "+obj.getString("id"), Toast.LENGTH_SHORT).show();
                            if (!obj.getString("id").isEmpty()){
                                WebService.MakeToast(Login.this,"Successfully logged in");
                                //WebService.account_type=acctype.getText().toString();
                                manager.createUserLoginSession(obj.getString("id"),obj.getString("firstname"),obj.getString("lastname"),obj.getString("id_default_group"),obj.getString("id_shop"),obj.getString("id_shop_group"));
                                startActivity(new Intent(Login.this,Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                finish();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }


    private void SetupView() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        forgot = (TextView) findViewById(R.id.forgot);
        login = (Button) findViewById(R.id.login);
        skip = (Button) findViewById(R.id.skip);
        create = (RelativeLayout) findViewById(R.id.create);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (connectionDetector.isConnectingToInternet()) {
                    Log.e("connection",connectionDetector.isConnectingToInternet()+"");
                } else {
                    Log.e("connection",connectionDetector.isConnectingToInternet()+"");
                    WebService.MakeToast(Login.this, getString(R.string.check_internet));
                }*/
                loginprocess();
            }
        });
        rg = (RadioGroup)findViewById(R.id.rg);
        customer = (RadioButton)findViewById(R.id.customer);
        dealer = (RadioButton)findViewById(R.id.dealer);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,ForgotPassword.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                //finish();
            }
        });
    }

    public void SignUp(View V) {
        startActivity(new Intent(Login.this, Registration.class));
    }
}
