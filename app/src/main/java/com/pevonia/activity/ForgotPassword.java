package com.pevonia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;

import java.util.HashMap;

public class ForgotPassword extends AppCompatActivity {
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
    EditText email,password,cpassword;
    Button submit,confirm;
    LinearLayout emailLayout,passwordLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        connectionDetector = new ConnectionDetector(ForgotPassword.this);
        manager = new UserSessionManager(ForgotPassword.this);
        map = manager.getUserDetail();
        SetupView();
    }
    private void SetupView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        tprofile.setText("Forgot Password");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        email= (EditText) findViewById(R.id.email);
        password= (EditText) findViewById(R.id.password);
        cpassword= (EditText) findViewById(R.id.cpassword);
        submit= (Button) findViewById(R.id.submit);
        confirm= (Button) findViewById(R.id.confirm);
        emailLayout= (LinearLayout) findViewById(R.id.emailLayout);
        passwordLayout= (LinearLayout) findViewById(R.id.passwordLayout);

        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;}
        });
        cpassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (cpassword.getRight() - cpassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        cpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        cpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    cpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    cpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;}
        });
    }
    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                submitProcess();
                break;
            case R.id.confirm:
                confirmProcess();
                break;
        }
    }
    private void submitProcess() {
        if (email.getText().toString().equals("")){
            email.setError("Please type email...");
        }else {
            emailLayout.setVisibility(View.GONE);
            passwordLayout.setVisibility(View.VISIBLE);
        }
    }
    private void confirmProcess() {
        if (password.getText().toString().length() <= 0){
            password.setError("Please enter your password");
        }else if (!password.getText().toString().equals(cpassword.getText().toString())) {
            cpassword.setError("Password don't match please enter your currect password...");
        }else {
            //WebService.MakeToast(ForgotPassword.this,"Password Change successfull...");
            startActivity(new Intent(ForgotPassword.this,Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }
}
