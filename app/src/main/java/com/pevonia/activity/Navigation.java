package com.pevonia.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pevonia.R;
import com.pevonia.adapter.CartProAdapter;
import com.pevonia.adapter.ShipingAdapter;
import com.pevonia.bean.CartProductBean;
import com.pevonia.bean.ShipingBean;
import com.pevonia.fragment.Cart;
import com.pevonia.fragment.Home;
import com.pevonia.fragment.Store;
import com.pevonia.fragment.UserAccount;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.CartCarrierListener;
import com.pevonia.util.CartProductListener;
import com.pevonia.util.VerticalSpaceItemDecoration;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.pevonia.util.WebService.paypalAmount;


public class Navigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CartProductListener, CartCarrierListener {
    Context mContext;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String, String> lcmap;
    Toolbar toolbar;
    static TextView title;
    public static TextView counttext, ordertotal;
    Typeface custom_font;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    View header;

    EditText searchView;
    ImageView notification;
    public static Button products, treatment, cancel, confirm;
    Button home, productn, category, treatmentn, logout, skinadvisory, ouroutlet, prozone, video, news, aboutus, faq, contact, mycustomer, mycommission, mycustomerorder, mystore, storelist, myaddress, settings;
    private TabLayout tabLayout;
    RelativeLayout mycustomerlayout, mycommissionlayout, mycustomerorderlayout, mystorelayout, logoutlayout, myaddresslayout, pricelayout;
    ViewPager viewPager;
    LinearLayout layouttwo, layoutone;
    private RelativeLayout myorderlayout;
    private Button myorderbtn;
    private TextView profile_name;
    FrameLayout frame;
    Fragment fragment = null;
    Class fragmentClass = null;
    FragmentManager fragmentManager;
    public static CartProAdapter cpAdapter;

    private ProgressDialog pDialog;
    boolean categoryDisplay = true;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    //private static final String CONFIG_CLIENT_ID = "AZOXVhKXVQ6mDtmAO_npRcKuNFJulIWJ4Xi_L642s1yNZWIXdip-dZ79TirBDHlQ-AP-6SkzPdFqDu5t";
    private static final String CONFIG_CLIENT_ID = "AalJ2pnhyXAW2XzkSSc1eHM2hlv0tcFN2WL2tZ9FIvMyWgRut2xN9UKcl3NWRY1MPg8GjY25tcCPj2JZ";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Pevonia")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    static PayPalPayment thingToBuy;
    //public static List<CartProductBean> cartProduct = new ArrayList<>();
    CartProductListener cartProductListener;
    CartCarrierListener cartCarrierListener;
    private ArrayList<CartProductBean> mList;
    //private ArrayList<ShipingBean> shipingBean;
    private ShipingAdapter shipAdapter;
    private static final int REQUEST_CODE = 101;
    private static final int REQUEST_CODE2 = 102;
    Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = Navigation.this;
        gson = new Gson();
        detector = new ConnectionDetector(Navigation.this);
        manager = new UserSessionManager(Navigation.this);
        map = manager.getUserDetail();
        lcManager = new LCManager(Navigation.this);
        lcmap = lcManager.getLC();
        mList = new ArrayList<>();
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        setUpViews();
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        //Log.e("KEY_GroupID",map.get(UserSessionManager.KEY_GroupID).toString());
        if (map.get(UserSessionManager.KEY_GroupID) != null) {
            //myorderlayout.setVisibility(View.VISIBLE);
            //logoutlayout.setVisibility(View.VISIBLE);
            if (map.get(UserSessionManager.KEY_GroupID).toString().equals("5")) {
                mycustomerlayout.setVisibility(View.VISIBLE);
                myorderlayout.setVisibility(View.VISIBLE);
                mycommissionlayout.setVisibility(View.VISIBLE);
                logoutlayout.setVisibility(View.VISIBLE);
                mycustomerorderlayout.setVisibility(View.VISIBLE);
                myaddresslayout.setVisibility(View.VISIBLE);
                mystorelayout.setVisibility(View.GONE);
            } else {
                //myorderlayout.setVisibility(View.GONE);
                logoutlayout.setVisibility(View.VISIBLE);
                myorderlayout.setVisibility(View.VISIBLE);
                myaddresslayout.setVisibility(View.VISIBLE);
                mycustomerlayout.setVisibility(View.GONE);
                mycommissionlayout.setVisibility(View.GONE);
                mycustomerorderlayout.setVisibility(View.GONE);
                mystorelayout.setVisibility(View.GONE);
                //logoutlayout.setVisibility(View.GONE);
            }
        } else {
            myorderlayout.setVisibility(View.GONE);
            logoutlayout.setVisibility(View.GONE);
            myaddresslayout.setVisibility(View.GONE);
        }

        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.home)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.store)));
        //tabLayout.addTab(tabLayout.newTab().setText("Wishlist"));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.cart)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.profile)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        setupTabIcons();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //tab.getIcon().setColorFilter(getResources().getColor(R.color.dark,null), PorterDuff.Mode.SRC_IN);
                    tab.getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
                } else {
                    tab.getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
                }
                //tab.getIcon().setColorFilter(getResources().getColor(R.color.dark,null), PorterDuff.Mode.SRC_IN);
                //viewPager.setCurrentItem(tab.getPosition());
                //Log.e("currenttab", tab.getPosition() + "");
                /*if (tab.getPosition() == 1) {
                    searchView.setText("");
                } else if (tab.getPosition() == 2) {
                    searchView.setText("");
                } else if (tab.getPosition() == 3) {
                    searchView.setText("");
                } else if (tab.getPosition() == 4) {
                    searchView.setText("");
                } else {
                    searchView.setText("");
                }*/
                switch (tab.getPosition()) {
                    case 0:
                        fragmentClass = Home.class;
                        searchView.setHint("Search for Products");
                        //counttext.setVisibility(View.GONE);
                        //cancel.setVisibility(View.GONE);
                        //confirm.setVisibility(View.GONE);
                        layouttwo.setVisibility(View.GONE);
                        layoutone.setVisibility(View.VISIBLE);
                        pricelayout.setVisibility(View.GONE);
                        treatment.setVisibility(View.VISIBLE);
                        products.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        fragmentClass = Store.class;
                        searchView.setHint("Search for Stores");
                        //counttext.setVisibility(View.GONE);
                        //cancel.setVisibility(View.GONE);
                        //confirm.setVisibility(View.GONE);
                        layouttwo.setVisibility(View.GONE);
                        layoutone.setVisibility(View.VISIBLE);
                        pricelayout.setVisibility(View.GONE);
                        treatment.setVisibility(View.VISIBLE);
                        products.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.VISIBLE);
                        break;
                    /*case 2:
                        fragmentClass = Wishlist.class;
                        searchView.setHint("Search for Wishlist");
                        //counttext.setVisibility(View.GONE);
                        //cancel.setVisibility(View.GONE);
                        //confirm.setVisibility(View.GONE);
                        pricelayout.setVisibility(View.GONE);
                        layouttwo.setVisibility(View.GONE);
                        layoutone.setVisibility(View.VISIBLE);
                        treatment.setVisibility(View.VISIBLE);
                        products.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.VISIBLE);
                        break;*/
                    case 2:
                        fragmentClass = Cart.class;
                        searchView.setHint("Search for Products");
                        //counttext.setVisibility(View.VISIBLE);
                        //cancel.setVisibility(View.VISIBLE);
                        //confirm.setVisibility(View.VISIBLE);
                        layouttwo.setVisibility(View.VISIBLE);
                        pricelayout.setVisibility(View.VISIBLE);
                        layoutone.setVisibility(View.GONE);
                        treatment.setVisibility(View.GONE);
                        products.setVisibility(View.GONE);
                        searchView.setVisibility(View.GONE);
                        break;
                    case 3:
                        fragmentClass = UserAccount.class;
                        searchView.setHint("Search for Products");
                        //counttext.setVisibility(View.GONE);
                        //cancel.setVisibility(View.GONE);
                        //confirm.setVisibility(View.GONE);
                        pricelayout.setVisibility(View.GONE);
                        layouttwo.setVisibility(View.GONE);
                        layoutone.setVisibility(View.VISIBLE);
                        treatment.setVisibility(View.VISIBLE);
                        products.setVisibility(View.VISIBLE);
                        searchView.setVisibility(View.VISIBLE);
                        break;
                }
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                    searchView.setText("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabLayout.getTabAt(0).getIcon().clearColorFilter();
                tab.getIcon().clearColorFilter();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        /*Pager pageradapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageradapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));*/
    }


    private void setUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        counttext = (TextView) toolbar.findViewById(R.id.counttext);
        notification = (ImageView) toolbar.findViewById(R.id.notification);
        ordertotal = (TextView) findViewById(R.id.ordertotal);
        searchView = (EditText) findViewById(R.id.search);
        products = (Button) findViewById(R.id.products);
        treatment = (Button) findViewById(R.id.treatment);
        cancel = (Button) findViewById(R.id.cancel);
        confirm = (Button) findViewById(R.id.confirm);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);


        home = (Button) header.findViewById(R.id.home);
        profile_name = (TextView) header.findViewById(R.id.profile_name);
        if (map.get(UserSessionManager.KEY_GroupID) != null) {
            profile_name.setVisibility(View.VISIBLE);
            //  profile_name.setText("Welcome, "+ map.get(UserSessionManager.KEY_fname).toString());
            profile_name.setText("Welcome, " + WebService.capitalize(map.get(UserSessionManager.KEY_fname).toString()));
        } else {
            profile_name.setVisibility(View.GONE);
        }
        productn = (Button) header.findViewById(R.id.productn);
        category = (Button) header.findViewById(R.id.category);
        treatmentn = (Button) header.findViewById(R.id.treatmentn);

        skinadvisory = (Button) header.findViewById(R.id.skinadvisory);
        ouroutlet = (Button) header.findViewById(R.id.ouroutlet);
        prozone = (Button) header.findViewById(R.id.prozone);
        video = (Button) header.findViewById(R.id.video);
        news = (Button) header.findViewById(R.id.news);
        aboutus = (Button) header.findViewById(R.id.aboutus);
        faq = (Button) header.findViewById(R.id.faq);
        contact = (Button) header.findViewById(R.id.contact);
        settings = (Button) header.findViewById(R.id.settings);
        logout = (Button) header.findViewById(R.id.logout);

        myaddress = (Button) header.findViewById(R.id.myaddress);
        mycustomer = (Button) header.findViewById(R.id.mycustomer);
        mycommission = (Button) header.findViewById(R.id.mycommission);
        mycustomerorder = (Button) header.findViewById(R.id.mycustomerorder);
        mystore = (Button) header.findViewById(R.id.mystore);
        storelist = (Button) header.findViewById(R.id.storelist);

        mycustomerlayout = (RelativeLayout) header.findViewById(R.id.mycustomerlayout);
        mycommissionlayout = (RelativeLayout) header.findViewById(R.id.mycommissionlayout);
        mycustomerorderlayout = (RelativeLayout) header.findViewById(R.id.mycustomerorderlayout);
        mystorelayout = (RelativeLayout) header.findViewById(R.id.mystorelayout);
        logoutlayout = (RelativeLayout) header.findViewById(R.id.logoutlayout);
        myorderlayout = (RelativeLayout) header.findViewById(R.id.myorderlayout);
        myaddresslayout = (RelativeLayout) header.findViewById(R.id.myaddresslayout);
        pricelayout = (RelativeLayout) findViewById(R.id.pricelayout);
        layoutone = (LinearLayout) findViewById(R.id.layoutone);
        layouttwo = (LinearLayout) findViewById(R.id.layouttwo);
        myorderbtn = (Button) header.findViewById(R.id.myorder);

        myorderbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyOrder.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        /*myorderlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyOrder.class));
            }
        });*/
        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, ProductActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cancel.getText().toString().equals("cancel Order")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(Navigation.this);
                    alertDialog.setTitle("Cancel Order");
                    alertDialog.setMessage("Are you sure you want to cancel order?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //cpBean.clear();
                        /*cpBean.removeAll(cpBean);
                        cpAdapter.notifyDataSetChanged();*/
                            Cart.RemoveData("cancel");
                            WebService.MakeToast(Navigation.this, "Order cancel sucessfully");
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    WebService.MakeToast(Navigation.this, "Your cart is empty...");
                }
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//Confirm Order
                if (confirm.getText().toString().equals("Select Address")) {
                    Intent mIntent = new Intent(Navigation.this, MyAddress.class);
                    startActivityForResult(mIntent, REQUEST_CODE);
                } else {
                    if (WebService.shipingid.equals("")){
                        Intent mIntent = new Intent(Navigation.this, PopupActivity.class);
                        startActivityForResult(mIntent, REQUEST_CODE2);
                    }else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Navigation.this);
                        alertDialog.setTitle("Confirm Order");
                        alertDialog.setMessage("Are you sure you want to confirm order?");
                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                        /*cpBean.clear();
                        cpAdapter.notifyDataSetChanged();*/
                                //Cart.RemoveData("confirm");
                                //GetPayment(WebService.paypalAmount);
                                //WebService.MakeToast(Navigation.this,"Order confirm sucessfully");
                                initiateNewPaymentOptionpopup();
                            }
                        });
                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }
                }
            }
        });
        treatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, TreatmentActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(Navigation.this, Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                startActivity(new Intent(Navigation.this, Navigation.class));
                drawer.closeDrawer(GravityCompat.START);
                finish();
            }
        });
        productn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, ProductActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, Category.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        treatmentn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, TreatmentActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        skinadvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, SkinAdvisory.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ouroutlet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, OutLetActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        prozone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, ProzoneActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, Videos.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, News.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, AboutUsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, FaqActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, ContactActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Navigation.this, Language.class);
                intent.putExtra("setting", "setting");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        myaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyAddress.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                //startActivity(new Intent(Navigation.this, EditAddress.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        mycustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyCustomer.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        mycommission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyCommission.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        mycustomerorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyCustomerOrder.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        mystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, MyStore.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        storelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Navigation.this, StoreList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Navigation.this);
                alertDialog.setTitle("LogOut");
                alertDialog.setMessage("Are you sure you want to logout?");
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (map.get(UserSessionManager.KEY_USERID) != null) {

                            manager.logoutUser();
                            finish();
                            //Cart.RemoveData("remove");
                            cpAdapter.Remove();
                        } else {
                            //manager.logoutUser();
                            finish();
                        }
                        finish();
                        //startActivity(new Intent(Navigation.this,Login.class));
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        drawer.closeDrawer(GravityCompat.START);
                    }
                });
                alertDialog.show();
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                //Log.e("tabcount", tabLayout.getSelectedTabPosition() + "");
                if (tabLayout.getSelectedTabPosition() == 0) {
                    if (!s.equals("")) {
                        Home.SearchFilter(s.toString());
                    }
                } else if (tabLayout.getSelectedTabPosition() == 1) {
                    if (!s.equals("")) {
                        Store.SearchFilter(s.toString());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        fragmentClass = Home.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {   /*   tabLayout.getTabAt(0).getIcon().clearColorFilter();
             *//* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //tab.getIcon().setColorFilter(getResources().getColor(R.color.dark,null), PorterDuff.Mode.SRC_IN);
                    tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
                }else {
                    tabLayout.getTabAt(3).getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
                }*//*
                tabLayout.getTabAt(2).select();
                fragmentClass = Cart.class;
                layouttwo.setVisibility(View.VISIBLE);
                pricelayout.setVisibility(View.VISIBLE);
                layoutone.setVisibility(View.GONE);
                treatment.setVisibility(View.GONE);
                products.setVisibility(View.GONE);
                searchView.setVisibility(View.GONE);
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();*/
            }
        });

    }

    private void GetPayment(String payment) {
        Log.e("payment123", payment);
        //Cart.RemoveData("confirm");

        String currency;
        if (WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext).equals("RM")) {
            currency = "MYR";
        }else {currency=WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext);}
        Log.e("currency", currency);
        thingToBuy = new PayPalPayment(new BigDecimal(payment), "USD",
                "Pevonia", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(Navigation.this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private void setupTabIcons() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //tabLayout.getTabAt(0).setIcon(R.drawable.ic_home).getIcon().setColorFilter(getResources().getColor(R.color.dark,null), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_home).getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_list_black);
            //tabLayout.getTabAt(2).setIcon(R.drawable.ic_wishlist);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_cart);
            tabLayout.getTabAt(3).setIcon(R.drawable.ic_account);
            tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
        } else {
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_home).getIcon().setColorFilter(ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_list_black);
            //tabLayout.getTabAt(2).setIcon(R.drawable.ic_wishlist);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_cart);
            tabLayout.getTabAt(3).setIcon(R.drawable.ic_account);
            tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
            //ContextCompat.getColor(Navigation.this, R.color.dark), PorterDuff.Mode.SRC_IN
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_camera) {
        } else if (id == R.id.nav_gallery) {
        } else if (id == R.id.nav_slideshow) {
        } else if (id == R.id.nav_manage) {
        } else if (id == R.id.nav_share) {
        } else if (id == R.id.nav_send) {
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void loadProduct(ArrayList<CartProductBean> cartProductBeans) {
        if (cartProductListener != null) {
            pDialog.dismiss();
            cartProductListener.loadProduct(cartProductBeans);
        }
    }
    @Override
    public void loadCarrier(ArrayList<ShipingBean> shipingBean) {
        if (cartCarrierListener != null) {
            pDialog.dismiss();
            cartCarrierListener.loadCarrier(shipingBean);

        }
    }
    public void refresh() {
        if (detector.isConnectingToInternet()) {
            if (map.get(UserSessionManager.KEY_USERID) != null) {
                getCartProductList();
            }
        } else {
            WebService.MakeToast(mContext, getString(R.string.check_internet));
        }
    }
    private class Pager extends FragmentStatePagerAdapter {
        int tabcount;

        public Pager(FragmentManager fm, int tabcount) {
            super(fm);
            this.tabcount = tabcount;
        }

        @Override
        public Fragment getItem(int position) {

            /*switch (position) {
                case 0:
                    Home home = new Home();
                    return home;
                case 1:
                    Store store = new Store();
                    return store;
                case 2:
                    Wishlist wishlist = new Wishlist();
                    return wishlist;
                case 3:
                    Cart cart = new Cart();
                    return cart;
                case 4:
                    UserAccount uaccount = new UserAccount();
                    return uaccount;
            }*/

            return null;

        }

        @Override
        public int getCount() {
            return tabcount;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult",requestCode+" ");
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                 Log.e("payment log :",""+confirm.toString() );
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject().toString(4));
                        Log.e("123456 : ", "" + confirm.toJSONObject().toString(4));
                        Log.e("666666 : ", "" + confirm.getPayment().toJSONObject().toString(4));

                        Toast.makeText(Navigation.this, "Order placed",
                                Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);
                        //sendAuthorizationToServer(auth);
                        Toast.makeText(getApplicationContext(),
                                "Future Payment code received from PayPal",
                                Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE && data != null) {
                String addid = data.getStringExtra("addressId");
                Log.e("Result: message >>", "" + addid);
                if (!addid.equals("")) {

                    confirm.setText("Select Carriers");
                    WebService.addressid = addid;
                    Intent mIntent = new Intent(Navigation.this, PopupActivity.class);
                    startActivityForResult(mIntent, REQUEST_CODE2);
                    //initiatePopupWindow(mContext);
                } else {
                    confirm.setText("Select Address");
                }
            } else if (requestCode == REQUEST_CODE2 && data != null) {
                WebService.shipingid = data.getStringExtra("shipId");
                Log.e("Result: shipingId >>", "" + WebService.shipingid);
                Log.e("Result: tax >>", "" + data.getStringExtra("tax"));
                if (!WebService.shipingid.equals("")) {
                    confirm.setText("Confirm Order");
                    Cart.ordertotal.setVisibility(View.VISIBLE);
                    Cart.tv1.setVisibility(View.VISIBLE);
                    WebService.sip=Double.parseDouble(data.getStringExtra("tax"));
                }else {
                    confirm.setText("Select Carriers");
                }
            }

        }
    }

    private void initiatePopupWindow(Context mContext) {
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            View popupView = inflater.inflate(R.layout.carrier_layout, null);
            final PopupWindow mPopupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            mPopupWindow.update();
            RecyclerView popuplist = (RecyclerView) popupView.findViewById(R.id.recyclerViewpopup);
            popuplist.setLayoutManager(new LinearLayoutManager(mContext));
            popuplist.setItemAnimator(new DefaultItemAnimator());
            popuplist.addItemDecoration(new VerticalSpaceItemDecoration(18));

            shipAdapter = new ShipingAdapter(WebService.shipingBean, mContext);
            shipAdapter.notifyDataSetChanged();
            popuplist.setAdapter(shipAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initiateNewPaymentOptionpopup() {
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = inflater.inflate(R.layout.payment_option_layout, null);
            final PopupWindow mPopupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            mPopupWindow.update();
            ImageView id_dismiss = (ImageView) popupView.findViewById(R.id.id_dismiss);
            ImageView paypal = (ImageView) popupView.findViewById(R.id.paypalb);
            Button submit = (Button) popupView.findViewById(R.id.submit);
            id_dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPopupWindow.dismiss();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("pamentAmount",WebService.paypalAmount+"");
                    JSONObject object = new JSONObject();
                    Log.e("string",mList.toString());
                    try {
                        //object.put("data", new JSONArray(jsonCPB));
                        object.put("id_customer",map.get(UserSessionManager.KEY_USERID));
                        object.put("id_address_delivery",WebService.addressid);
                        object.put("id_address_invoice",WebService.addressid);
                        object.put("id_cart",WebService.CartID);
                        object.put("id_currency",lcmap.get(LCManager.currencyID));
                        object.put("id_lang",lcmap.get(LCManager.languageID));
                        object.put("id_carrier",WebService.shipingid);
                        object.put("current_state","3");
                        object.put("valid","0");
                        object.put("payment","Cash on delivery (COD)");
                        object.put("module","cashondelivery");
                        object.put("total_paid",WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip)));
                        object.put("total_paid_tax_incl",WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip)));
                        object.put("total_paid_tax_excl",WebService.GetDecimalFormate(String.valueOf(WebService.ttt+WebService.sip)));
                        object.put("total_paid_real","0");
                        object.put("total_products",WebService.ttt);
                        object.put("total_products_wt",WebService.ttt);
                        object.put("conversion_rate",WebService.GetDecimalFormate(WebService.conversion_rate));
                        object.put("total_shipping",WebService.sip);
                        object.put("total_shipping_tax_incl",WebService.sip);
                        object.put("total_shipping_tax_excl",WebService.sip);
                        object.put("total_discounts","0.00");
                        object.put("products", new JSONArray(mList.toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("object", object.toString());
                    if (detector.isConnectingToInternet()) {
                        String data=object.toString();
                        checkoutProcess(data);
                    } else {
                        WebService.MakeToast(mContext, mContext.getString(R.string.check_internet));
                    }

                    mPopupWindow.dismiss();
                }
            });
            paypal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("pamentAmount",WebService.paypalAmount+"");
                    GetPayment(WebService.paypalAmount);
                    mPopupWindow.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkoutProcess(final String data) {
        Log.e("checkoutProcess",data);
        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = WebService.CheckOutOrder;
        StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    Log.e("CheckOutOrder123", "" + fic_response);
                    //{"code":"200","message":"Order placed successfully","data":{"id_order":"72","total_paid":"435.00"}}
                    if (fic_response != null) {
                        JSONObject obj = new JSONObject(fic_response);
                        if (obj.getString("code").equals("200")) {
                            //JSONObject obj1 = new JSONObject(obj.getString("code"));
                            WebService.MakeToast(mContext,obj.getString("message"));
                           cleardata();
                            startActivity(new Intent(Navigation.this, Navigation.class));
                            finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.e("checkoutProcess ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", data, "utf-8");
                    return null;
                }
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void cleardata() {
        WebService.CartID="";
        WebService.addressid="";
        WebService.shipingid="";
        WebService.sip=0;
        WebService.shipingBean.clear();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment != null) {
            if (fragment instanceof Cart) {
                cartProductListener = (CartProductListener) fragment;
                if (map.get(UserSessionManager.KEY_USERID) != null) {
                    getCartProductList();
                }
            }
            if (fragment instanceof Home) {
            }
            if (fragment instanceof Store) {
            }
            if (fragment instanceof UserAccount) {
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (detector.isConnectingToInternet()) {
            if (map.get(UserSessionManager.KEY_USERID) != null) {
                getCartProductList();
            }
        } else {
            WebService.MakeToast(mContext, getString(R.string.check_internet));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    public void getCartProductList() {
        if (!WebService.CartID.equals("")) {
            if (detector.isConnectingToInternet()) {
                pDialog = new ProgressDialog(mContext);
                pDialog.setMessage("Loading...");
                pDialog.setCancelable(false);
                pDialog.show();

                String url = WebService.Cart_List + WebService.CartID + "?" + WebService.output_format;
                StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pDialog.dismiss();
                        mList.clear();
                        WebService.shipingBean.clear();
                        try {
                            String fic_response = WebService.fixEncoding(response);
                            Log.e("Cart_List navi", fic_response + " ");
                            JSONObject obj = new JSONObject(fic_response);
                            JSONObject obj1 = new JSONObject(obj.getString("cart"));
                            if (obj1.has("associations")) {
                                JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                                Log.e("cart_rows", "" + associationsObj.getString("cart_rows"));
                                JSONArray ary = associationsObj.getJSONArray("cart_rows");
                                for (int i = 0; i < ary.length(); i++) {
                                    JSONObject cproduct = ary.getJSONObject(i);
                                    CartProductBean bean = new CartProductBean();
                                    Log.e("id_product", "" + cproduct.getString("id_product"));
                                    Log.e("product_name", "" + cproduct.getString("product_name"));
                                    Log.e("quantity", "" + cproduct.getString("quantity"));
                                    bean.setId(cproduct.getString("id_product"));
                                    bean.setPrice(cproduct.getString("product_price"));
                                    bean.setName(cproduct.getString("product_name"));
                                    bean.setQty(cproduct.getString("quantity"));
                                    bean.setId_product_attribute(cproduct.getString("id_product_attribute"));
                                    bean.setId_address_delivery(cproduct.getString("id_address_delivery"));
                                    bean.setId_default_image(cproduct.getString("product_image"));
                                    mList.add(bean);
                                }
                                if (mList.size() == 0) {
                                    Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                    Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                    Navigation.cancel.setText("Your cart is empty...");
                                    Navigation.confirm.setVisibility(View.GONE);
                                    Cart.nodata.setVisibility(View.VISIBLE);
                                    Cart.withshipping.setVisibility(View.GONE);
                                    Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                    paypalAmount = "0.00";
                                    WebService.addressid="";
                                    WebService.shipingid="";
                                    WebService.sip=0;
                                    WebService.shipingBean.clear();
                                } else {
                                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        Navigation.counttext.setBackground(ContextCompat.getDrawable(mContext, R.drawable.round));
                                    } else {
                                        Navigation.counttext.setBackground(ContextCompat.getDrawable(mContext, R.drawable.round));
                                    }
                                    //Navigation.counttext.setBackground(context.getResources().getDrawable(R.drawable.round,null));
                                    Navigation.counttext.setText("" + mList.size());
                                    Navigation.cancel.setText("cancel Order");
                                    Navigation.confirm.setVisibility(View.VISIBLE);
                                }
                                if (associationsObj.has("cart_carrirers")) {
                                    JSONArray cart_carrirersary = associationsObj.getJSONArray("cart_carrirers");
                                    for (int i = 0; i < cart_carrirersary.length(); i++) {
                                        JSONObject carrirerobj = cart_carrirersary.getJSONObject(i);
                                        ShipingBean bean = new ShipingBean();
                                        Log.e("id_carrier", "" + carrirerobj.getString("id_carrier"));
                                        Log.e("carrier_name", "" + carrirerobj.getString("carrier_name"));
                                        bean.setId(carrirerobj.getString("id_carrier"));
                                        if (carrirerobj.getString("logo").trim().charAt(0) == '{') {
                                            bean.setLogo("");
                                        } else {
                                            bean.setLogo(carrirerobj.getString("logo"));
                                        }
                                        if (carrirerobj.getString("carrier_name").trim().charAt(0) == '{') {
                                            bean.setCarrier_name("");
                                        } else {
                                            bean.setCarrier_name(carrirerobj.getString("carrier_name"));
                                        }
                                        if (carrirerobj.getString("delay").trim().charAt(0) == '{') {
                                            bean.setDelay("");
                                        } else {
                                            bean.setDelay(carrirerobj.getString("delay"));
                                        }
                                        bean.setTotal_price_with_tax(carrirerobj.getString("total_price_with_tax"));
                                        bean.setTotal_price_without_tax(carrirerobj.getString("total_price_without_tax"));
                                        bean.setIs_free(carrirerobj.getString("is_free"));

                                        WebService.shipingBean.add(bean);
                                    }


                                }
                                cartProductListener.loadProduct(mList);
                                //cartCarrierListener.loadCarrier(WebService.shipingBean);
                            } else {
                                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                Navigation.cancel.setText("Your cart is empty...");
                                Navigation.confirm.setVisibility(View.GONE);
                                Cart.nodata.setVisibility(View.VISIBLE);
                                Cart.withshipping.setVisibility(View.GONE);
                                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), mContext) + "0.00");
                                paypalAmount = "0.00";
                                WebService.addressid="";
                                WebService.shipingid="";
                                WebService.sip=0;
                                WebService.shipingBean.clear();
                                cartProductListener.loadProduct(mList);
                                if (WebService.shipingBean!=null) {
                                    cartCarrierListener.loadCarrier(WebService.shipingBean);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                //Log.e("ERROR", "error => " + error.toString());
                                pDialog.dismiss();
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = WebService.SetAuth();
                        return params;
                    }
                };
                loginreq.setRetryPolicy(new DefaultRetryPolicy(
                        10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
            } else {
                WebService.MakeToast(mContext, getString(R.string.check_internet));
            }
        }else {
            WebService.shipingBean.clear();
            Navigation.cancel.setText("Your cart is empty...");
            Navigation.confirm.setVisibility(View.GONE);
        }
    }

}
