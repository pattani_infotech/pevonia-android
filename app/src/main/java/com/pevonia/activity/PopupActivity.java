package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.ShipingAdapter;
import com.pevonia.bean.ShipingBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.ItemClickListener;
import com.pevonia.util.VerticalSpaceItemDecoration;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PopupActivity extends AppCompatActivity implements ItemClickListener {
Context mContext;
    RecyclerView popuplist;
    private ArrayList<ShipingBean> ship;
    private ShipingAdapter shipAdapter;
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mContext = PopupActivity.this;
        detector = new ConnectionDetector(mContext);
        manager = new UserSessionManager(mContext);
        map = manager.getUserDetail();
        SetUpView();

        if (WebService.shipingBean.size() != 0) {
            if (shipAdapter == null) {
                shipAdapter=new ShipingAdapter(WebService.shipingBean,mContext);
                shipAdapter.notifyDataSetChanged();
                popuplist.setAdapter(shipAdapter);
                shipAdapter.setClickListener(PopupActivity.this);
            }
        }
    }

    private void SetUpView() {
        popuplist = (RecyclerView)findViewById(R.id.recyclerViewpopup);
        popuplist.setLayoutManager(new LinearLayoutManager(mContext));
        popuplist.setItemAnimator(new DefaultItemAnimator());
        popuplist.addItemDecoration(new VerticalSpaceItemDecoration(18));
    }


    @Override
    public void onClick(View view, int position) {
        Log.e("position adapter",position+"");
        final ShipingBean shippingdata = WebService.shipingBean.get(position);
        Log.e("name",shippingdata.getId()+" "+shippingdata.getCarrier_name());

        if (shippingdata.getId()!=null||!shippingdata.getId().equals("")){
            if (detector.isConnectingToInternet()) {
                UpdateAddressInCart(WebService.addressid,shippingdata.getId(),position,shippingdata);
            } else {
                WebService.MakeToast(mContext, mContext.getString(R.string.check_internet));
            }
        }

    }

    private void UpdateAddressInCart(final String addressid, final String shippingid, final int position, final ShipingBean shippingdata) {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Update_Address + WebService.CartID+"&update=address";
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    WebService.shipingBean.clear();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        Log.e("Update_Address", fic_response + " ");
                        JSONObject obj = new JSONObject(fic_response);
                        JSONObject obj1 = new JSONObject(obj.getString("cart"));
                        if (obj1.has("associations")) {
                            JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                            JSONObject cartobj=new JSONObject(associationsObj.getString("cart_carrirers"));

                            if (cartobj.getString("cart_carrirer").trim().charAt(0) == '{'){
                                JSONObject carrirerobj = new JSONObject(cartobj.getString("cart_carrirer"));
                                ShipingBean bean = new ShipingBean();
                                bean.setId(carrirerobj.getString("id_carrier"));
                                if (carrirerobj.getString("logo").trim().charAt(0) == '{') {
                                    bean.setLogo("");
                                } else {
                                    bean.setLogo(carrirerobj.getString("logo"));
                                }
                                if (carrirerobj.getString("carrier_name").trim().charAt(0) == '{') {
                                    bean.setCarrier_name("");
                                } else {
                                    bean.setCarrier_name(carrirerobj.getString("carrier_name"));
                                }
                                if (carrirerobj.getString("delay").trim().charAt(0) == '{') {
                                    bean.setDelay("");
                                } else {
                                    bean.setDelay(carrirerobj.getString("delay"));
                                }
                                if (carrirerobj.getString("total_price_with_tax").trim().charAt(0) == '{') {
                                    bean.setTotal_price_with_tax("0");
                                } else {
                                    bean.setTotal_price_with_tax(carrirerobj.getString("total_price_with_tax"));
                                }
                                if (carrirerobj.getString("total_price_without_tax").trim().charAt(0) == '{') {
                                    bean.setTotal_price_without_tax("0");
                                } else {
                                    bean.setTotal_price_without_tax(carrirerobj.getString("total_price_without_tax"));
                                }
                                if (carrirerobj.getString("is_free").trim().charAt(0) == '{') {
                                    bean.setIs_free("");
                                } else {
                                    bean.setIs_free(carrirerobj.getString("is_free"));
                                }
                                WebService.shipingBean.add(bean);
                            }else {
                                JSONArray ary = cartobj.getJSONArray("cart_carrirer");
                                Log.e("Update_Addresssize",ary.length()+"");
                                for (int i = 0; i < ary.length(); i++) {
                                    JSONObject carrirerobj = ary.getJSONObject(i);
                                    ShipingBean bean = new ShipingBean();
                                    bean.setId(carrirerobj.getString("id_carrier"));
                                    if (carrirerobj.getString("logo").trim().charAt(0) == '{') {
                                        bean.setLogo("");
                                    } else {
                                        bean.setLogo(carrirerobj.getString("logo"));
                                    }
                                    if (carrirerobj.getString("carrier_name").trim().charAt(0) == '{') {
                                        bean.setCarrier_name("");
                                    } else {
                                        bean.setCarrier_name(carrirerobj.getString("carrier_name"));
                                    }
                                    if (carrirerobj.getString("delay").trim().charAt(0) == '{') {
                                        bean.setDelay("");
                                    } else {
                                        bean.setDelay(carrirerobj.getString("delay"));
                                    }
                                    if (carrirerobj.getString("total_price_with_tax").trim().charAt(0) == '{') {
                                        bean.setTotal_price_with_tax("0");
                                    } else {
                                        bean.setTotal_price_with_tax(carrirerobj.getString("total_price_with_tax"));
                                    }
                                    if (carrirerobj.getString("total_price_without_tax").trim().charAt(0) == '{') {
                                        bean.setTotal_price_without_tax("0");
                                    } else {
                                        bean.setTotal_price_without_tax(carrirerobj.getString("total_price_without_tax"));
                                    }
                                    if (carrirerobj.getString("is_free").trim().charAt(0) == '{') {
                                        bean.setIs_free("");
                                    } else {
                                        bean.setIs_free(carrirerobj.getString("is_free"));
                                    }
                                    WebService.shipingBean.add(bean);
                                }
                            }

                            Intent mIntent = new Intent();
                            mIntent.putExtra("shipId", shippingdata.getId());
                            mIntent.putExtra("tax", shippingdata.getTotal_price_with_tax());
                            setResult(RESULT_OK, mIntent);
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id_address_delivery",""+addressid);
                    params.put("id_address_invoice", ""+addressid);
                    params.put("id_carrier", ""+shippingid);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(mContext, getString(R.string.check_internet));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent = new Intent();
        mIntent.putExtra("shipId", "");
        mIntent.putExtra("tax", "0");
        setResult(RESULT_OK, mIntent);
        finish();
    }
}
