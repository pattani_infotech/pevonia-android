package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CatProductsAdapter;
import com.pevonia.bean.CatProductBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.pevonia.util.WebService.Category_Product_List;

public class CategoryProduct extends AppCompatActivity {
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    EditText search;
    Toolbar toolbar;
    public static TextView tprofile, nodata,catname,catdis;
    Button sort, filter;
    ImageButton back, list;
    List<CatProductBean> catproBean = new ArrayList<>();
    private CatProductsAdapter catProAdapter;
    public static GridView gridview;
    public static ListView listview;
    public static boolean listpressCP = false;
    private ProgressDialog pDialog;
    private RelativeLayout mRelativeLayout;
    private PopupWindow mPopupWindow;
    private Context mContext;
    String first = "0";
    boolean loadingMore = false;
    String productname = "productList";
    SwipeRefreshLayout swipe;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_product);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = getApplicationContext();
        connectionDetector = new ConnectionDetector(CategoryProduct.this);
        manager = new UserSessionManager(CategoryProduct.this);
        map = manager.getUserDetail();
        lcManager=new LCManager(CategoryProduct.this);
        lcmap=lcManager.getLC();
        SetUpViews();
        Log.e("listpressCP = ",listpressCP+"");
        if (getIntent().hasExtra("id")) {
            Log.e("id = ",getIntent().getStringExtra("id")+"");
            if (!getIntent().getStringExtra("id").equals("") || !getIntent().getStringExtra("id").equals("na")) {
                if (connectionDetector.isConnectingToInternet()) {
                    GetCategoryProduct(String.valueOf(position), productname);
                } else {
                    WebService.MakeToast(CategoryProduct.this, getString(R.string.check_internet));
                }
            }
        }
    }

    private void GetCategoryProduct(String first, final String productList) {
        pDialog = new ProgressDialog(CategoryProduct.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        if (first.equals("0")) {
            pDialog.show();
        } else {
            pDialog.dismiss();
        }
        String url = null;
        if (productList.equals("lowestfirst")) {
            productname = productList;
            url = WebService.Category_LowestFirst +getIntent().getStringExtra("id")+"&sort=price_DESC&limit="+ first + ",10" ;//+ WebService.output_format;
        } else if (productList.equals("highestfirst")) {
            productname = productList;
            url = WebService.Category_HighestFirst +getIntent().getStringExtra("id")+"&sort=price_ASC&limit="+ first + ",10";// + WebService.output_format;
        } else if (productList.equals("atoz")) {
            productname = productList;
            url = WebService.Category_AtoZ +getIntent().getStringExtra("id")+"&sort=name_DESC&limit="+ first + ",10";// + WebService.output_format;
        } else if (productList.equals("ztoa")) {
            productname = productList;
            url = WebService.Category_ZtoA +getIntent().getStringExtra("id")+"&sort=name_ASC&limit="+ first + ",10";// + WebService.output_format;
        } else if (productList.equals("instock")) {
            productname = productList;
            url = WebService.Category_INStockFirst + getIntent().getStringExtra("id")+"&sort=quantity_desc&limit="+first + ",10";// + WebService.output_format;
        } else {
            productname = productList;
            url = Category_Product_List + getIntent().getStringExtra("id")+"&limit="+first+",10";
        }

        loadingMore = true;
        //swipe.setEnabled(true);
        //Log.e("  url = ",url+"");
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                //Log.e("position", position + " ");
                try {
                    String fic_response = WebService.fixEncoding(response);
                    Log.e("Cat_Product res =", fic_response + " ");
                    JSONObject obj = new JSONObject(fic_response);
                    //Log.e("length", obj.length() + "");
                    if (obj.getString("code").equals("200")) {
                        JSONObject obj1=new JSONObject(obj.getString("data"));
                        JSONArray ary=obj1.getJSONArray("category_name");
                        for (int i = 0; i < ary.length(); i++) {
                            JSONObject obja1 = ary.getJSONObject(i);
                            if (obja1.getString("id").equals(lcmap.get(LCManager.languageID))){
                                catname.setText(obja1.getString("value"));
                            }
                        }
                        JSONArray disary=obj1.getJSONArray("description");
                        for (int i = 0; i < disary.length(); i++) {
                            JSONObject objd1 = disary.getJSONObject(i);
                            if (objd1.getString("id").equals(lcmap.get(LCManager.languageID))){
                                if (Build.VERSION.SDK_INT >= 24) {
                                    catdis.setText(Html.fromHtml(objd1.getString("value"),Html.FROM_HTML_MODE_LEGACY));
                                }else {
                                    catdis.setText(Html.fromHtml(objd1.getString("value")));
                                }
                            }
                        }
                        JSONArray pary = obj1.getJSONArray("products");
                        for (int i = 0; i < pary.length(); i++) {
                            JSONObject pobj1 = pary.getJSONObject(i);
                            CatProductBean bean = new CatProductBean();
                            /*Log.e("id",pobj1.getString("id"));
                            Log.e("price",pobj1.getString("price"));
                            Log.e("name",pobj1.getString("name"));
                            Log.e("available_for_order",pobj1.getString("available_for_order"));
                            Log.e("id_default_image",pobj1.getString("id_default_image"));*/

                            bean.setId(pobj1.getString("id"));
                            bean.setPrice(pobj1.getString("price"));
                            bean.setName(pobj1.getString("name"));
                            bean.setAvailable(pobj1.getString("available_for_order"));
                            bean.setId_default_image(pobj1.getString("id_default_image"));
                            if (i == (pary.length() - 1)) {
                                position = (position + i + 1);
                                //Log.e("position increase = ",position+"");
                            }
                            catproBean.add(0, bean);
                        }
                        if (catproBean.size()==0){
                            nodata.setVisibility(View.VISIBLE);
                            gridview.setVisibility(View.GONE);
                            listview.setVisibility(View.GONE);
                        }else {
                            //Log.e("catproBean size = ",catproBean.size()+"");
                            nodata.setVisibility(View.GONE);
                            catProAdapter = new CatProductsAdapter(catproBean, CategoryProduct.this);
                            gridview.setAdapter(catProAdapter);
                            listview.setAdapter(catProAdapter);
                            catProAdapter.notifyDataSetChanged();
                            loadingMore = false;
                        }
                        if (listpressCP){
                            //Log.e("123= plist",listpressCP+"");
                            listview.setVisibility(View.VISIBLE);
                            gridview.setVisibility(View.GONE);
                        }else {
                            //Log.e("listpressCP = plist",listpressCP+"");
                            listview.setVisibility(View.GONE);
                            gridview.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                        // swipe.setRefreshing(false);
                        loadingMore = false;
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        list = (ImageButton) toolbar.findViewById(R.id.list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        swipe = (SwipeRefreshLayout) findViewById(R.id.activity_category_product);

        nodata = (TextView) findViewById(R.id.nodata);
        catname = (TextView) findViewById(R.id.catname);
        catdis = (TextView) findViewById(R.id.catdis);
        search = (EditText) findViewById(R.id.search);
        sort = (Button) findViewById(R.id.sort);
        filter = (Button) findViewById(R.id.filter);
        gridview = (GridView) findViewById(R.id.gridview);
        listview = (ListView) findViewById(R.id.listview);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.mRelativeLayout);
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listpressCP) {
                    listpressCP = false;
                    //Log.e("false",""+listpress);
                    gridview.setVisibility(View.VISIBLE);
                    gridview.setAdapter(catProAdapter);
                    gridview.setTextFilterEnabled(true);
                    listview.setVisibility(View.GONE);
                } else {
                    listpressCP = true;
                    //Log.e("true",""+listpress);
                    gridview.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);
                    listview.setAdapter(catProAdapter);
                    listview.setTextFilterEnabled(true);
                }
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.equals("")) {
                    //Home.SearchFilter(s.toString());
                    catProAdapter.getFilter().filter(s.toString());
                    //pAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
                position = 0;
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 0;
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position), "productList");
                search.setText("");
                sort.setText("Sort By");
                filter.setVisibility(View.GONE);
            }
        });
        swipe.setColorSchemeResources(R.color.blue, R.color.yellow, R.color.accent);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipe.setRefreshing(false);
                        GetCategoryProduct(String.valueOf(position), productname);
                    }
                }, 3000);
            }
        });
    }
    private void initiatePopupWindow() {
        //mRelativeLayout.setClickable(false);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.custome_layout, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.update();
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);
        final TextView tv = (TextView) customView.findViewById(R.id.tv);
        final TextView tv1 = (TextView) customView.findViewById(R.id.tv1);
        final TextView tv2 = (TextView) customView.findViewById(R.id.tv2);
        final TextView tv3 = (TextView) customView.findViewById(R.id.tv3);
        final TextView tv4 = (TextView) customView.findViewById(R.id.tv4);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.dismiss();
            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position),"lowestfirst");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Lowest first");
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position),"highestfirst");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Highest first");
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position),"atoz");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By A to Z");
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position),"ztoa");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Z to A");
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catproBean.clear();
                GetCategoryProduct(String.valueOf(position),"instock");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By In-Stock");
            }
        });
    }
}
