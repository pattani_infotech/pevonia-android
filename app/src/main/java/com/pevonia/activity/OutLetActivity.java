package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OutLetActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
    /*EditText store;
    ExpandableListView listView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ExpandableListAdapter listAdapter;
    List<HeaderBean> headerBean= new ArrayList<>();
    List<ArrayList<String>> childBean= new ArrayList<>();*/
    WebView webView;
    int PIC_WIDTH;
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_let);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        SetUpViews();
        connectionDetector = new ConnectionDetector(OutLetActivity.this);
        manager = new UserSessionManager(OutLetActivity.this);
        map = manager.getUserDetail();
        if (connectionDetector.isConnectingToInternet()) {
            GetOutLet();
        } else {
            WebService.MakeToast(OutLetActivity.this, getString(R.string.check_internet));
        }
        //prepareListData();
      /* Headerdata();
        Childdata();
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        // setting list adapter
        listView.setAdapter(listAdapter);*/
    }

    private void GetOutLet() {
        if (connectionDetector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(OutLetActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.OurOutlet;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        //Log.e("OurOutlet", fic_response + "");
                        JSONObject obj = new JSONObject(fic_response);
                        JSONObject obj1=obj.getJSONObject("content");

                        JSONArray arr = obj1.getJSONArray("content");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj2 = arr.getJSONObject(0);
                            //Log.e("OurOutlet arr",obj2.getString("id")+"\n"+obj2.getString("value"));
                            String yourhtmlpage = "<html><body><b>"+obj2.getString("value")+"</b> </body></html>";
                            webView.loadUrl("javascript:document.getElementById(\"selectMe\").setAttribute(\"style\",\"visibility:hidden;\");");
                            webView.loadUrl("javascript:document.getElementById('selectMe').style.visibility='hidden';");
                            webView.loadDataWithBaseURL(WebService.webviewUrl, yourhtmlpage, "text/html", "UTF-8", null);


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(OutLetActivity.this, getString(R.string.check_internet));
        }
    }

   /* private void Childdata() {
        ArrayList<String> child = new ArrayList<String>();
        child.add("1");
        child.add("12");
        child.add("123");
        child.add("1234");
        child.add("12345");
        childBean.add(child);

        child.add("5");
        child.add("56");
        child.add("567");
        child.add("5678");
        child.add("56789");
        child.add("567890");
        childBean.add(child);

        child.add("a");
        child.add("ab");
        child.add("abc");
        child.add("abcd");
        child.add("abcde");
        childBean.add(child);
    }

    private void Headerdata() {
        HeaderBean bean=new HeaderBean("title 1");
        headerBean.add(bean);
        bean=new HeaderBean("title 2");
        headerBean.add(bean);
        bean=new HeaderBean("title 3");
        headerBean.add(bean);

    }*/

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        tprofile.setText("Our Outlets");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
       /* store = (EditText) findViewById(R.id.store);
        listView = (ExpandableListView) findViewById(listView);*/
        webView = (WebView) findViewById(R.id.webView);
        PIC_WIDTH= webView.getRight()-webView.getLeft();
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        WebSettings settings = webView.getSettings();
        settings.setMinimumFontSize(30);
        webView.setInitialScale(4);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
    }

    private void prepareListData() {
       /* listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("Top 250");
        listDataHeader.add("Now Showing");
        listDataHeader.add("Coming Soon..");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);*/
    }
}
