package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.DealerAdapter;
import com.pevonia.bean.DealerBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Registration extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
    EditText fname, lname, username, email, password, rpassword, mobile;
    RadioGroup rg;
    RadioButton customer, dealer;
    Spinner dlist;
    Button register;
    private ProgressDialog pDialog;
    List<DealerBean> dealerBean = new ArrayList<>();
    DealerAdapter adapter;
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        cd = new ConnectionDetector(Registration.this);
        manager = new UserSessionManager(Registration.this);
        map = manager.getUserDetail();
        SetUpViews();
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        tprofile.setText("Registration");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        fname = (EditText) findViewById(R.id.fname);
        lname = (EditText) findViewById(R.id.lname);
        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        rpassword = (EditText) findViewById(R.id.rpassword);
        mobile = (EditText) findViewById(R.id.mobile);

        rg = (RadioGroup) findViewById(R.id.rg);
        customer = (RadioButton) findViewById(R.id.customer);
        dealer = (RadioButton) findViewById(R.id.dealer);
        dlist = (Spinner) findViewById(R.id.dlist);
        register = (Button) findViewById(R.id.register);

        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) { final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false; }
        });
        rpassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) { final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (rpassword.getRight() - rpassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        rpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        rpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    rpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    rpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false; }
        });
        dlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (dlist.getSelectedItemPosition() != 0) {
                    WebService.dealer_id = dealerBean.get(position).getId();
                    //Toast.makeText(Registration.this,"position "+dlist.getSelectedItemPosition()+"\nid : "+dealerBean.get(i).getId()+"\nfname : "+dealerBean.get(i).getFname()+"\nlname : "+dealerBean.get(i).getLname(),Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(Registration.this,"No Select Dealer",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.customer:
                dlist.setVisibility(View.VISIBLE);
                if (cd.isConnectingToInternet()) {
                    GetDealerList();
                } else {
                    WebService.MakeToast(Registration.this, getString(R.string.check_internet));
                }

                break;
            case R.id.dealer:
                dlist.setVisibility(View.GONE);
                break;
            case R.id.register:
                RegistrationProcess();
                break;
        }
    }

    private void RegistrationProcess() {
        if (fname.getText().toString().equals("")) {
            fname.setError("Please type first name...");
        } else if (lname.getText().toString().equals("")) {
            lname.setError("Please type last lame...");
        } else if (username.getText().toString().equals("")) {
            username.setError("Please type username...");
        } else if (email.getText().toString().equals("")||!WebService.validateEmail(email.getText().toString())) {
            email.setError("Please type email...");
        } else if (password.getText().toString().length() <= 0) {
            password.setError("Please enter your password");
        } else if (!password.getText().toString().equals(rpassword.getText().toString())) {
            rpassword.setError("Password don't match please enter your currect password...");
        } else if (rg.getCheckedRadioButtonId() == -1) {
            Toast.makeText(Registration.this, "Please select account type", Toast.LENGTH_LONG).show();
        } else {
            if (customer.isChecked()) {
                if (dlist.getSelectedItemPosition() == 0) {
                    Toast.makeText(Registration.this, "Please select dealer", Toast.LENGTH_SHORT).show();
                } else {
                    /*Log.e("Registration", "Customer Registration");
                    Log.e("Id_default_group", "3");
                    Log.e("passwd", WebService.ConverToMD5(WebService.cookie_key + rpassword.getText().toString()));
                    Log.e("Lastname", lname.getText().toString());
                    Log.e("Firstname", fname.getText().toString());
                    Log.e("email", email.getText().toString());
                    Log.e("Id_dealer", WebService.dealer_id);
                    Log.e("username", username.getText().toString());
                    Log.e("active", "1");*/
                    if (cd.isConnectingToInternet()) {
                        GetRegistration();
                    }else {
                        WebService.MakeToast(Registration.this, getString(R.string.check_internet));
                    }
                }
            } else {
                if (cd.isConnectingToInternet()) {
                    GetRegistration();
                }else {
                    WebService.MakeToast(Registration.this, getString(R.string.check_internet));
                }
            }
        }
    }

    private void GetRegistration() {
        if (cd.isConnectingToInternet()) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Registration;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        //Log.e("Registration", fic_response + "");
                        if (fic_response.trim().charAt(0)=='{'||fic_response.trim().charAt(0)=='[') {
                            if (fic_response != null) {
                                JSONObject obj = new JSONObject(fic_response);
                                if (obj.has("code")) {
                                    if (obj.getString("code").equals("500")) {
                                        WebService.MakeToast(Registration.this, "User with same email exist");
                                    }
                                } else if (obj.has("customer")) {
                                    JSONObject obj1 = obj.getJSONObject("customer");
                                    WebService.MakeToast(Registration.this, "Registration successfully");
                                    manager.createUserLoginSession(obj1.getString("id"),obj1.getString("firstname"),obj1.getString("lastname"),obj1.getString("id_default_group"),obj1.getString("id_shop"),obj1.getString("id_shop_group"));
                                    startActivity(new Intent(Registration.this, Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    finish();
                                }
                            } else {
                                WebService.MakeToast(Registration.this, "Please try again...");
                            }
                        }else {
                            WebService.MakeToast(Registration.this, "Please try again...");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    if (customer.isChecked()){
                        params.put("id_default_group","3");
                        params.put("passwd",rpassword.getText().toString());
                        params.put("lastname",lname.getText().toString());
                        params.put("firstname",fname.getText().toString());
                        params.put("email",email.getText().toString());
                        params.put("username",username.getText().toString());
                        params.put("active","1");
                        params.put("id_dealer",WebService.dealer_id);
                    }else {
                        params.put("id_default_group","5");
                        params.put("passwd",rpassword.getText().toString());
                        params.put("lastname",lname.getText().toString());
                        params.put("firstname",fname.getText().toString());
                        params.put("email",email.getText().toString());
                        params.put("username",username.getText().toString());
                        params.put("active","1");
                    }
                    return params; //return the parameters
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }

            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(Registration.this, getString(R.string.check_internet));
        }
    }

    private void GetDealerList() {
        pDialog = new ProgressDialog(Registration.this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = WebService.dealerList;
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.setCancelable(false);
                pDialog.dismiss();
                dealerBean.clear();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    //Log.e("dealer List", fic_response + "");
                    DealerBean dealer = new DealerBean();
                    dealer.setId("0");
                    dealer.setFname("Select Dealer");
                    dealer.setLname("Name");
                    dealerBean.add(dealer);
                    JSONObject obj = new JSONObject(fic_response);
                    // Log.e("length", obj.length() + "");
                    JSONArray ary = obj.getJSONArray("customers");
                    for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj1 = ary.getJSONObject(i);
                        DealerBean bean = new DealerBean();
                        bean.setId(obj1.getString("id"));
                        bean.setFname(obj1.getString("firstname"));
                        bean.setLname(obj1.getString("lastname"));
                        dealerBean.add(bean);
                    }
                    adapter = new DealerAdapter(dealerBean, Registration.this);
                    dlist.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                       // Log.e("ERROR", "error => " + error.toString());
                        pDialog.setCancelable(false);
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }
}
