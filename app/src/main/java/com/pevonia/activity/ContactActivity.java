package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import java.util.HashMap;

public class ContactActivity extends AppCompatActivity {
Spinner subhead;
EditText email,orderid,message;
Button send;
    Toolbar toolbar;
    ImageButton back;
    public static TextView tprofile;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        detector = new ConnectionDetector(ContactActivity.this);
        manager = new UserSessionManager(ContactActivity.this);
        map = manager.getUserDetail();
        SetUpViews();
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        email= (EditText) findViewById(R.id.email);
        orderid= (EditText) findViewById(R.id.orderid);
        message= (EditText) findViewById(R.id.message);
        send= (Button) findViewById(R.id.send);
        subhead= (Spinner) findViewById(R.id.subhead);

    }

    public void OnClick(View v) {
        switch (v.getId()) {
            case R.id.send:
                sendProcess();

                break;
        }
    }

    private void sendProcess() {
        if (subhead.getSelectedItemPosition()==0){
            WebService.MakeToast(ContactActivity.this,getString(R.string.select_heading));
        }else if (email.getText().toString().equals("")){
            email.setError(getString(R.string.enter_email_contact));
        }else if (orderid.getText().toString().equals("")){
            orderid.setError(getString(R.string.enter_orderid_contact));
        } else if (message.getText().toString().equals("")) {
            message.setError(getString(R.string.enter_message_contact));
        }else {
            finish();
        }
    }
}
