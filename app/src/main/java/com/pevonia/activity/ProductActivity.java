package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.ProductsAdapter;
import com.pevonia.bean.ProGridBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.EndlessScrollListener;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductActivity extends AppCompatActivity {
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    EditText search;
    Toolbar toolbar;
    public static TextView tprofile;
    public static ImageView nodata;
    Button sort, filter;
    ImageButton back, list;

    public static GridView gridview;
    public static ListView listview;
    public static boolean listpress = false;
    private ProgressDialog pDialog;
    List<ProGridBean> proGridBean = new ArrayList<>();
    List<ProGridBean> dataBean = new ArrayList<>();
    private ProductsAdapter pAdapter;
    private RelativeLayout mRelativeLayout;
    private PopupWindow mPopupWindow;
    private Context mContext;
    String first = "0";
    boolean loadingMore = false;
    boolean isloading = false;
    public static String productname = "productList";
    SwipeRefreshLayout swipe;

    private int visibleThreshold = 5;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int startingPageIndex = 0;
    public static int position=0;
    private int current_index;
    private int lisposition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = getApplicationContext();
        connectionDetector = new ConnectionDetector(ProductActivity.this);
        manager = new UserSessionManager(ProductActivity.this);
        map = manager.getUserDetail();
        SetUpViews();
        //PrepareData();
        //pAdapter = new ProductsAdapter(proGridBean, ProductActivity.this);
        if (connectionDetector.isConnectingToInternet()) {
            //GetProduct("productList",first);
            GetProduct(String.valueOf(position), productname);
        } else {
            WebService.MakeToast(ProductActivity.this, getString(R.string.check_internet));
        }
       /* if (listpress){
            listview.setVisibility(View.VISIBLE);
            listview.setAdapter(pAdapter);
        }else {
            gridview.setVisibility(View.VISIBLE);
            gridview.setAdapter(pAdapter);
        }*/
        /*gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {     *//*int lastInScreen = (position + visibleItemCount);
                Log.e("lastInScreen ", lastInScreen + "");
                Log.e("firstVisibleItem ", firstVisibleItem + "");
                Log.e("visibleItemCount ", visibleItemCount + "");
                Log.e("totalItemCount ", totalItemCount + "");
                Log.e("grid123 ", lastInScreen + "");

                if ((position==(lastInScreen+totalItemCount)) && !(loadingMore)) {
                    first = String.valueOf(lastInScreen);
                    //Log.e("first", first);
                    //gridview.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    //gridview.setSelection(gridview.getAdapter().getCount()-1);
                    //Log.e("gridview.getAdapter()", (gridview.getAdapter().getCount()-1)+"");
                    //gridview.setItemChecked(gridview.getCheckedItemPosition() + 1, true);
                    GetProduct(String.valueOf(position), productname);
                }*//*
                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("firstVisibleItem ", firstVisibleItem + "");
                Log.e("visibleItemCount ", visibleItemCount + "");
                Log.e("totalItemCount ", totalItemCount + "");
                Log.e("list123 ", lastInScreen + "");
                Log.e("position 123 ", position + "");

                if ((position == lastInScreen) && !(loadingMore)) {
                    first = String.valueOf(lastInScreen);
                    Log.e("first", first);
                    GetProduct(String.valueOf(position), productname);
                }
            }
        });
        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {}
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                Log.e("firstVisibleItem ", firstVisibleItem + "");
                Log.e("visibleItemCount ", visibleItemCount + "");
                Log.e("totalItemCount ", totalItemCount + "");
                Log.e("list123 ", lastInScreen + "");
                Log.e("position 123 ", position + "");

                if ((position == lastInScreen) && !(loadingMore)) {
                    first = String.valueOf(lastInScreen);
                    Log.e("first", first);
                    //listview.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                    //listview.setNextFocusDownId(listview.getAdapter().getCount()-1);
                    GetProduct(String.valueOf(position), productname);
                    //listview.setSelection(View.FOCUS_UP);
                    listview.smoothScrollToPosition(position);
                }
            }
        });*/


        /*gridview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {}
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if (firstVisibleItem == 0) {
                    swipe.setEnabled(true);
                    Log.e("lastInScreen if",lastInScreen+"");
                    //GetProduct(first, productname);
                } else {
                    Log.e("lastInScreen else",lastInScreen+"");

                    swipe.setEnabled(false);
                }

            }
        });*/
       /* gridview.setTranscriptMode(AbsListView.FOCUS_DOWN);
        gridview.setItemChecked(gridview.getCheckedItemPosition() + 1, true);*/
        gridview.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
                // int lastInScreen = firstVisibleItem + visibleItemCount;
                // if ((position == lastInScreen) && !(loadingMore)) {
                current_index = gridview.getLastVisiblePosition();
                Log.e("current_index",current_index+"");
                if (pDialog!=null){
                    pDialog.dismiss();
                    GetProduct(String.valueOf(position),productname);
                }
                Log.e("sss",(position-1)+"");
                // gridview.setSelection(position);
                // }
                // or loadNextDataFromApi(totalItemsCount);
                return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });
        /*listview.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listview.setItemChecked(listview.getCheckedItemPosition() + 1, true);*/

        listview.setOnScrollListener(new AbsListView.OnScrollListener(){
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                lisposition=view.getLastVisiblePosition();
               /* int lastInScreen = firstVisibleItem + visibleItemCount;
                if((lastInScreen == totalItemCount) && !(loadingMore)){
                    if (pDialog!=null){
                        pDialog.dismiss();
                        GetProduct(String.valueOf(position),productname);
                    }
                }*/
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if ((lastInScreen == totalItemCount) && !isloading && (firstVisibleItem != 0)) {
                    isloading = true;
                    GetProduct(String.valueOf(position), productname);
                }
            }
        });
    }

    public  void GetProduct(final String first, final String productList) {

        pDialog = new ProgressDialog(ProductActivity.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
       // if (first.equals("0")) {
            pDialog.show();
        /*}else{
            pDialog.dismiss();
        }*/

        //String url = null;
        //Log.e("url navi", url);
        /*if (productList.equals("productList")){
            productname=productList;
             url = WebService.product+first+",10&"+WebService.output_format;
            proGridBean.clear();
        }else if (productList.equals("lowestfirst")){
            productname=productList;
            url = WebService.LowestFirst+first+",10&sort=[price_ASC]&"+WebService.output_format;
            //url = WebService.LowestFirst;
            proGridBean.clear();
        }else if (productList.equals("highestfirst")){
            url = WebService.HighestFirst+first+",10&sort=[price_DESC]&"+WebService.output_format;
            //url = WebService.HighestFirst;
            proGridBean.clear();
        }/*else if (productList.equals("atoz")){
            //url = WebService.AtoZ+first+",10&sort=[name_ASC]&"+WebService.output_format;
            url = WebService.AtoZ;
            proGridBean.clear();
        }else if (productList.equals("ztoa")){
            //url = WebService.ZtoA+first+",10&sort=[name_DESC]&"+WebService.output_format;
            url = WebService.ZtoA;
            proGridBean.clear();
        }else if (productList.equals("instock")){
            //url = WebService.INStockFirst+first+",10&sort=[quantity_desc]&"+WebService.output_format;
            url = WebService.INStockFirst;
            proGridBean.clear();
        }*/
        //Log.e("2106",productList);
        String url = null;
        if (productList.equals("lowestfirst")) {
            productname=productList;
            url = WebService.LowestFirst+first+",10&sort=[price_ASC]&"+WebService.output_format;
        }else if (productList.equals("highestfirst")){
            productname=productList;
            url = WebService.HighestFirst+first+",10&sort=[price_DESC]&"+WebService.output_format;
        }else if (productList.equals("atoz")){
            productname=productList;
            url = WebService.AtoZ+first+",10&sort=[name_ASC]&"+WebService.output_format;
        }else if (productList.equals("ztoa")){
            productname=productList;
            url = WebService.ZtoA+first+",10&sort=[name_DESC]&"+WebService.output_format;
        }else if (productList.equals("instock")){
            productname=productList;
            url = WebService.INStockFirst+first+",10&sort=[quantity_desc]&"+WebService.output_format;
        }else {
            productname=productList;
            url = WebService.product + first + ",10&" + WebService.output_format;
        }
        loadingMore = true;

        //swipe.setEnabled(true);
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                dataBean.clear();
                //Log.e("position", position + " ");
                try {
                    String fic_response = WebService.fixEncoding(response);
                    Log.e(productList +" response",fic_response + " ");
                    JSONObject obj = new JSONObject(fic_response);
                    //Log.e("length", obj.length() + "");
                    JSONArray ary = obj.getJSONArray("products");
                    for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj1 = ary.getJSONObject(i);
                        ProGridBean bean = new ProGridBean();
                        bean.setId(obj1.getString("id"));
                        bean.setPrice(obj1.getString("price"));
                        bean.setName(obj1.getString("name"));
                        bean.setAvailable(obj1.getString("available_for_order"));
                        bean.setId_default_image(obj1.getString("id_default_image"));
                        if(i==(ary.length()-1)) {
                           position=(position+i+1);
                            //position=Integer.toString(proGridBean.size());
                        }
                        int oo=proGridBean.size();
                        if (first.equals("0")) {
                            proGridBean.add(bean);
                        }else {
                            dataBean.add(bean);
                        }
                       // proGridBean.add(0,bean);
                    }
                    //Log.e("size", proGridBean.size()+"");
                    if (proGridBean.size()==0){
                        nodata.setVisibility(View.VISIBLE);
                        gridview.setVisibility(View.GONE);
                        listview.setVisibility(View.GONE);
                    }else {
                        nodata.setVisibility(View.GONE);
                        if (first.equals("0")) {
                            pAdapter = new ProductsAdapter(proGridBean, ProductActivity.this);
                            gridview.setAdapter(pAdapter);
                            listview.setAdapter(pAdapter);
                            //gridview.smoothScrollToPositionFromTop(current_index,0);
                            //listview.setSelection(lisposition);
                            //pAdapter.notifyDataSetChanged();
                            //loadingMore = false;
                        }else {
                            pAdapter.addListItemToAdapter(dataBean);
                            isloading = false;

                        }
                    }

                   // swipe.setRefreshing(false);
                    if (listpress){
                        listview.setVisibility(View.VISIBLE);
                        gridview.setVisibility(View.GONE);
                    }else {
                        listview.setVisibility(View.GONE);
                        gridview.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                       // swipe.setRefreshing(false);
                        loadingMore= false;
                        isloading=false;
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        list = (ImageButton) toolbar.findViewById(R.id.list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
       //swipe= (SwipeRefreshLayout) findViewById(R.id.activity_product);

        nodata = (ImageView) findViewById(R.id.nodata);
        search = (EditText) findViewById(R.id.search);
        sort = (Button) findViewById(R.id.sort);
        filter = (Button) findViewById(R.id.filter);
        gridview = (GridView) findViewById(R.id.gridview);
        listview = (ListView) findViewById(R.id.listview);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.mRelativeLayout);
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listpress) {
                    listpress = false;
                    //Log.e("false",""+listpress);
                    gridview.setVisibility(View.VISIBLE);
                    gridview.setAdapter(pAdapter);
                    gridview.setTextFilterEnabled(true);
                    listview.setVisibility(View.GONE);
                } else {
                    listpress = true;
                    //Log.e("true",""+listpress);
                    gridview.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);
                    listview.setAdapter(pAdapter);
                    listview.setTextFilterEnabled(true);
                }
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.equals("")) {
                    //Home.SearchFilter(s.toString());
                    pAdapter.getFilter().filter(s.toString());
                    //pAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
                position=0;
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position=0;
                proGridBean.clear();
                GetProduct(String.valueOf(position),"productList");
                search.setText("");
                sort.setText("Sort By");
                filter.setVisibility(View.GONE);
            }
        });
       /* swipe.setColorSchemeResources(R.color.blue,R.color.yellow,R.color.accent);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipe.setRefreshing(false);
                        GetProduct(String.valueOf(position), productname);
                    }
                }, 3000);
            }
        });*/
        /*EmbossMaskFilter debossFilter = new EmbossMaskFilter(new float[]{0f, -1f, 0.5f},0.8f,13,7.0f );
        nodata.getPaint().setMaskFilter(debossFilter);*/
    }

    private void initiatePopupWindow() {
        //mRelativeLayout.setClickable(false);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.custome_layout, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.update();
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);
        final TextView tv = (TextView) customView.findViewById(R.id.tv);
        final TextView tv1 = (TextView) customView.findViewById(R.id.tv1);
        final TextView tv2 = (TextView) customView.findViewById(R.id.tv2);
        final TextView tv3 = (TextView) customView.findViewById(R.id.tv3);
        final TextView tv4 = (TextView) customView.findViewById(R.id.tv4);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.dismiss();
            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proGridBean.clear();
                GetProduct(String.valueOf(position),"lowestfirst");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Lowest first");
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proGridBean.clear();
                GetProduct(String.valueOf(position),"highestfirst");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Highest first");
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proGridBean.clear();
                GetProduct(String.valueOf(position),"atoz");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By A to Z");
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proGridBean.clear();
                GetProduct(String.valueOf(position),"ztoa");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By Z to A");
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proGridBean.clear();
                GetProduct(String.valueOf(position),"instock");
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sort.setText("Sort By In-Stock");
            }
        });
    }

    private void PrepareData() {
        /*ProGridBean bean = new ProGridBean("clarigel exfoliating cleanser", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("clarigel exfoliating cleanser", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("clarigel exfoliating cleanser", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("clarigel exfoliating cleanser", R.drawable.product);
        proGridBean.add(bean);*/
    }

}
