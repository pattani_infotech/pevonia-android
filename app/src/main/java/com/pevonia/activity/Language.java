package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CurrencyAdapter;
import com.pevonia.adapter.LanguageAdapter;
import com.pevonia.bean.CurrencyBean;
import com.pevonia.bean.LanguageBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Language extends AppCompatActivity {
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String,String> map;

    LCManager lcManager;
    HashMap<String,String>lcmap;
    Button done;
    ImageButton back;
Spinner spinnerlanguage,spinnercurrency;
    List<LanguageBean> lgBean = new ArrayList<>();
    private LanguageAdapter lgAdapter;
    List<CurrencyBean> currencyBean = new ArrayList<>();
    private CurrencyAdapter currencyAdapter;
    private ProgressDialog pDialog,pDialog1;
String selectlanguageid,selectcurrencyid;
    private int indexl,indexc;
    private String ln_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector=new ConnectionDetector(Language.this);
        manager=new UserSessionManager(Language.this);
        map=manager.getUserDetail();
        lcManager=new LCManager(Language.this);
        lcmap=lcManager.getLC();
        SetUpView();
        //Log.e("currency id",lcmap.get(LCManager.currencyID));
        /*if (lcmap.get(LCManager.languageID)!=null){
            Intent intent = new Intent(Language.this, Navigation.class);
            //intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity(intent);
            finish();
        }*/
        if (getIntent().hasExtra("setting")){
            done.setText("done");
            back.setVisibility(View.VISIBLE);
        }
        if (detector.isConnectingToInternet()){
            GetLanguages();
            GetCurrency();
        }else {
            WebService.MakeToast(Language.this, getString(R.string.check_internet));
        }
    }

    private void GetCurrency() {
        pDialog1 = new ProgressDialog(this);
        pDialog1.setMessage("Loading...");
        pDialog1.setCancelable(false);
        pDialog1.show();
        String url = WebService.App_Currency;
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog1.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    //Log.e("App_Currency", fic_response + " ");
                    JSONObject obj = new JSONObject(fic_response);
                    //Log.e("length", obj.length() + "");
                    JSONArray ary = obj.getJSONArray("currencies");
                    for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj1 = ary.getJSONObject(i);
                        CurrencyBean bean = new CurrencyBean();
                        if (getIntent().hasExtra("setting")){
                            if (lcmap.get(LCManager.currencyID).equals(obj1.getString("id"))){
                                indexc=i;
                            }
                        }
                        bean.setId(obj1.getString("id"));
                        bean.setName(obj1.getString("name"));
                        //bean.setIso_code(obj1.getString("iso_code"));
                        //if (obj1.getString("active").equals("1")) {
                        //    if (obj1.getString("deleted").equals("0")) {
                                currencyBean.add(bean);
                        //  }
                        //}
                    }
                    currencyAdapter = new CurrencyAdapter(Language.this,currencyBean);
                    spinnercurrency.setAdapter(currencyAdapter);
                    if (getIntent().hasExtra("setting")) {
                        spinnercurrency.setSelection(indexc);
                    }
                   // spinnercurrency.setSelection(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog1.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void GetLanguages() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = WebService.App_Language;
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    Log.e("App_Language", fic_response + " ");
                    JSONObject obj = new JSONObject(fic_response);
                    JSONArray ary = obj.getJSONArray("languages");
                    for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj1 = ary.getJSONObject(i);
                        LanguageBean bean = new LanguageBean();
                        if (getIntent().hasExtra("setting")){
                            if (lcmap.get(LCManager.languageID).equals(obj1.getString("id"))){
                                indexl=i;
                            }
                        }
                        bean.setId(obj1.getString("id"));
                        bean.setName(obj1.getString("name"));
                        if(obj1.getString("language_code").equals("en-us") || obj1.getString("language_code").equals("en"))
                        {
                            Log.e("getString",obj1.getString("language_code"));
                            ln_code="en";
                            bean.setLanguage_code(ln_code);
                            setLocale(new Locale("en"));
                        }else if(obj1.getString("language_code").equals("th-th"))
                        {
                            Log.e("getString",obj1.getString("language_code"));
                            ln_code="th";
                            bean.setLanguage_code(ln_code);
                            setLocale(new Locale("th"));//
                        }else if(obj1.getString("language_code").equals("id-id"))
                        {
                            Log.e("getString",obj1.getString("language_code"));
                            ln_code="in";
                            bean.setLanguage_code(ln_code);
                            setLocale(new Locale("in"));//id-id
                        }else if(obj1.getString("language_code").equals("vi-vn"))
                        {
                            Log.e("getString",obj1.getString("language_code"));
                            ln_code="vi";
                            bean.setLanguage_code(ln_code);
                            setLocale(new Locale("vi"));
                        }else if(obj1.getString("language_code").equals("ms-my") || obj1.getString("language_code").equals("ms"))
                        {
                            Log.e("getString",obj1.getString("language_code"));
                            ln_code="ms";
                            bean.setLanguage_code(ln_code);
                            setLocale(new Locale("ms"));
                        }
                   //     Log.e("getString ln_code",ln_code);
                      //  bean.setLanguage_code(obj1.getString("language_code"));
                      //  bean.setLanguage_code(ln_code);
                            lgBean.add(bean);

                    }
                    lgAdapter = new LanguageAdapter(Language.this,lgBean);
                    spinnerlanguage.setAdapter(lgAdapter);
                    if (getIntent().hasExtra("setting")) {
                        spinnerlanguage.setSelection(indexl);
                    }
                    //spinnerlanguage.setSelection(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        //Log.e("ERROR", "error => " + error.toString());
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                //Log.e("params", params + "");
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        // Adding request to request queue
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void SetUpView() {
        spinnerlanguage= (Spinner) findViewById(R.id.spinnerlanguage);
        spinnercurrency= (Spinner) findViewById(R.id.spinnercurrency);
        done= (Button) findViewById(R.id.done);
        back= (ImageButton) findViewById(R.id.back);
        spinnerlanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectlanguageid=lgBean.get(i).getId();
                //selectlanguageid=lgBean.get(i).get;
                Log.e("onItemSelected",i+" "+lgBean.get(i).getLanguage_code());
                ln_code=lgBean.get(i).getLanguage_code();
              setLocale(new Locale(lgBean.get(i).getLanguage_code()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinnercurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectcurrencyid=currencyBean.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ln_code",ln_code+" ");
                lcManager.languageStore(selectlanguageid,selectcurrencyid,ln_code);
              //  setLocale(new Locale(ln_code));
                Intent intent = new Intent(Language.this, Navigation.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                startActivity(intent);
                finish();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void setLocale(Locale locale) {
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }
}
