package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Videos extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
   /* EditText store;
    GridView gridview;*/
   WebView webView;
    int PIC_WIDTH;
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        SetUpViews();
        connectionDetector = new ConnectionDetector(Videos.this);
        manager = new UserSessionManager(Videos.this);
        map = manager.getUserDetail();
        if (connectionDetector.isConnectingToInternet()) {
            GetVideos();
        } else {
            WebService.MakeToast(Videos.this, getString(R.string.check_internet));
        }

    }

    private void GetVideos() {
        if (connectionDetector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(Videos.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Videos;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        //Log.e("Videos", fic_response + "");
                        JSONObject obj = new JSONObject(fic_response);
                        JSONObject obj1=obj.getJSONObject("content");

                        JSONArray arr = obj1.getJSONArray("content");
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject obj2 = arr.getJSONObject(0);
                            //Log.e("Videos arr",obj2.getString("id")+"\n"+obj2.getString("value"));
                            String yourhtmlpage = "<html><body><b>"+obj2.getString("value")+"</b> </body></html>";
                            webView.loadDataWithBaseURL(WebService.webviewUrl, yourhtmlpage, "text/html", "UTF-8", null);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(Videos.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        tprofile.setText("Videos");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
       /* store = (EditText) findViewById(R.id.store);
        gridview= (GridView) findViewById(gridview);*/
        webView = (WebView) findViewById(R.id.webView);
        PIC_WIDTH= webView.getRight()-webView.getLeft();
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        WebSettings settings = webView.getSettings();
        settings.setMinimumFontSize(30);
        webView.setInitialScale(4);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
    }

}
