package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class StoreListDetail extends AppCompatActivity implements OnMapReadyCallback {
    Toolbar toolbar;
    TextView tprofile, disc, name, review, created, more, discnew, less, showreview;
    ImageButton back;
    ImageView proimg, ownerimg, managerimg, promotionimage;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    private ProgressDialog pDialog;
    RatingBar store_rating, review_rating;
    RelativeLayout morelayout;
    LinearLayout reviewlayout, ratinglayout, disclayout, imagelayout, promlayout, newaddlayout;
    GoogleMap gMap;
    MapFragment mapFragment;
    SliderLayout slideimg;
    Marker mCurrLocationMarker;
    String reviewArray;
int reviewCount=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_list_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        detector = new ConnectionDetector(StoreListDetail.this);
        manager = new UserSessionManager(StoreListDetail.this);
        map = manager.getUserDetail();
        lcManager=new LCManager(StoreListDetail.this);
        lcmap=lcManager.getLC();
        SetUpViews();
        Intent intent = getIntent();
        if (intent.hasExtra("id")) {
            /*if (intent.getStringExtra("imgurl").equals("") || intent.getStringExtra("imgurl").equals("na")) {
                Glide.with(StoreListDetail.this).load(R.drawable.not_available)
                        .thumbnail(0.5f)
                        .into(proimg);
            } else {
                Glide.with(StoreListDetail.this).load(intent.getStringExtra("imgurl").toString())
                        .thumbnail(0.5f)
                        .into(proimg);
            }*/
            try {
                JSONArray namearray = new JSONArray(intent.getStringExtra("strname"));
                for (int i=0;i<namearray.length();i++) {
                    JSONObject o = namearray.getJSONObject(i);
                    if (o.getString("id").equals(lcmap.get(LCManager.languageID))) {
                        if (o.getString("value").equals("")||o.getString("value").equals("null")){
                            tprofile.setText("No Store Name");
                        }else {
                            tprofile.setText(o.getString("value"));
                        }
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (detector.isConnectingToInternet()) {
                GetStoreDetails();
            } else {
                WebService.MakeToast(StoreListDetail.this, getString(R.string.check_internet));
            }
        }
        initilizeMap();

    }

    private void initilizeMap() {
        if (gMap == null) {
            mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.mapstr);
            mapFragment.getMapAsync(this);
            //gMap = mapFragment.getMap();
            /*gMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.mapstr)).getMap();
            // check if map is created successfully or not
            if (gMap == null) {
               *//* Toast.makeText(getApplicationContext(),
                        "Lo siento! incapaz de crear mapas", Toast.LENGTH_SHORT)
                        .show();*//*
            }*/
        }
    }


    private void GetStoreDetails() {
        pDialog = new ProgressDialog(StoreListDetail.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        String url = WebService.StoreListingDetail + getIntent().getStringExtra("id") + "/&" + WebService.output_format;
        StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    String fic_response = WebService.fixEncoding(response);
                    //Log.e("StoreListingDetail", fic_response + "");
                    JSONObject obj = new JSONObject(fic_response);
                    JSONObject obj1 = obj.getJSONObject("dealer_store");
                    JSONArray arystrname = obj1.getJSONArray("store_name");
                    JSONObject obj2 = arystrname.getJSONObject(0);

                    JSONArray arydiscription = obj1.getJSONArray("description");
                    //JSONObject discObj;// = arydiscription.getJSONObject(0);
                    for (int i=0;i<arydiscription.length();i++){
                        JSONObject  discObj=arydiscription.getJSONObject(i);
                        if (discObj.getString("id").equals(lcmap.get(LCManager.languageID))){
                          //  Log.e("123 id",discObj.getString("id"));
                          //  Log.e("123 value",discObj.getString("value"));
                            if (discObj.isNull("value") || discObj.getString("value").equals("") || discObj.getString("value").equals("null")) {
                                disclayout.setVisibility(View.GONE);
                            } else {
                                discnew.setText(discObj.getString("value"));
                                disc.setText(discObj.getString("value"));
                                //Log.e("maxline",discnew.getLineCount()+"");

                                if (discnew.getLineCount() > 2) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.GONE);
                                }
                            }
                        }/*else {
                          JSONObject  discObj1=arydiscription.getJSONObject(0);
                            if (discObj1.isNull("value") || discObj1.getString("value").equals("") || discObj1.getString("value").equals("null")) {
                                disclayout.setVisibility(View.GONE);
                            } else {
                                discnew.setText(discObj1.getString("value"));
                                disc.setText(discObj1.getString("value"));
                                //Log.e("maxline",discnew.getLineCount()+"");

                                if (discnew.getLineCount() > 2) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.GONE);
                                }
                            }
                        }*/
                    }

                    /*if (discObj.isNull("value") || discObj.getString("value").equals("") || discObj.getString("value").equals("null")) {
                        disclayout.setVisibility(View.GONE);
                    } else {
                        discnew.setText(discObj.getString("value"));
                        disc.setText(discObj.getString("value"));
                        //Log.e("maxline",discnew.getLineCount()+"");

                        if (discnew.getLineCount() > 2) {
                            more.setVisibility(View.VISIBLE);
                        } else {
                            more.setVisibility(View.GONE);
                        }
                    }*/
                    if (obj1.isNull("owner_image") || obj1.getString("owner_image").equals("") || obj1.getString("owner_image").equals("null")) {
                        ownerimg.setVisibility(View.GONE);
                    } else {
                        Glide.with(StoreListDetail.this).load(WebService.store_image_url + obj1.getString("owner_image"))
                                .thumbnail(0.5f)
                                .into(ownerimg);
                    }

                    if (obj1.isNull("manager_image") || obj1.getString("manager_image").equals("") || obj1.getString("manager_image").equals("null")) {
                        managerimg.setVisibility(View.GONE);
                    } else {
                        Glide.with(StoreListDetail.this).load(WebService.store_image_url + obj1.getString("manager_image"))
                                .thumbnail(0.5f)
                                .into(managerimg);
                    }
                    if (obj1.isNull("owner_image") || obj1.getString("owner_image").equals("") || obj1.getString("owner_image").equals("null") && obj1.isNull("manager_image") || obj1.getString("manager_image").equals("") || obj1.getString("manager_image").equals("null")) {
                        imagelayout.setVisibility(View.GONE);
                    }
                    if (obj1.isNull("store_rating")) {
                        ratinglayout.setVisibility(View.GONE);
                    } else {
                        //Log.e("rating", obj1.getString("store_rating"));
                        store_rating.setRating(Float.parseFloat(obj1.getString("store_rating")));
                    }
                    if (obj1.isNull("store_promotion_image") || obj1.getString("store_promotion_image").equals("") || obj1.getString("store_promotion_image").equals("null")) {
                        promlayout.setVisibility(View.GONE);
                    } else {
                        Glide.with(StoreListDetail.this).load(WebService.store_image_url + obj1.getString("store_promotion_image"))
                                .thumbnail(0.5f)
                                .into(promotionimage);
                    }

                    /*Log.e("id_country", obj1.getString("id_country"));
                    Log.e("id_dealer", obj1.getString("id_dealer"));
                    Log.e("postcode", obj1.getString("postcode"));
                    Log.e("latitude", obj1.getString("latitude")+" latitude");
                    Log.e("longitude", obj1.getString("longitude")+" longitude");
                    Log.e("associations", obj1.getString("associations"));*/
                    if (obj1.getString("latitude").equals("") || obj1.getString("latitude").equals("null") && obj1.getString("longitude").equals("") || obj1.getString("longitude").equals("null")) {
                        /*Log.e("latitude", "no if");
                        Log.e("longitude", "no if");*/
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pintwo);
                        //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_city);
                        LatLng location = new LatLng(3.1390,101.6869);

                        gMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                        gMap.animateCamera(CameraUpdateFactory.zoomTo(8));
                        //Log.e("location if part",""+location);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                        mCurrLocationMarker = gMap.addMarker(markerOptions);
                    } else {
                        /*Log.e("latitude", obj1.getString("latitude"));
                        Log.e("longitude", obj1.getString("longitude"));*/
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.pintwo);
                        LatLng location = new LatLng(Double.parseDouble(obj1.getString("latitude")), Double.parseDouble(obj1.getString("longitude")));

                        gMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                        gMap.animateCamera(CameraUpdateFactory.zoomTo(8));
                        //Log.e("location else part",""+location);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                        mCurrLocationMarker = gMap.addMarker(markerOptions);
                    }                                                                                                                    /*if (obj1.has("store_review")) {
                        JSONArray store_review = obj1.getJSONArray("store_review");
                        JSONObject strvalue = store_review.getJSONObject(0);
                        if (strvalue.has("value")) {
                            if (strvalue.isNull("value")) {
                                reviewlayout.setVisibility(View.GONE);
                            } else {
                                JSONObject obj11 = strvalue.getJSONObject("value");
                                name.setText(obj11.getString("name"));
                                review.setText(obj11.getString("review"));
                                review_rating.setRating(Float.parseFloat(obj11.getString("rating")));
                                created.setText(obj11.getString("created_at"));
                                //Log.e("strvalue", obj11.getString("name"));
                            }
                        } else {
                            reviewlayout.setVisibility(View.GONE);
                        }
                    } else {
                        reviewlayout.setVisibility(View.GONE);
                    }
                    Log.e("associations",obj1.getString("associations"));
                    if (obj1.getString("latitude").equals("")||obj1.getString("latitude")==null && obj1.getString("longitude").equals("") || obj1.getString("longitude")==null){
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_map);
                        LatLng location = new LatLng(22.3039,70.8022);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                        gMap.addMarker(markerOptions);
                        gMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                        gMap.animateCamera(CameraUpdateFactory.zoomTo(8));
                        Log.e("location if part",""+location);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                         mCurrLocationMarker = gMap.addMarker(markerOptions);
                    }else {
                        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_map);
                        LatLng location = new LatLng(Double.parseDouble(obj1.getString("latitude")),Double.parseDouble(obj1.getString("longitude")));
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                        gMap.addMarker(markerOptions);
                        gMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                        gMap.animateCamera(CameraUpdateFactory.zoomTo(8));
                        Log.e("location else part",""+location);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(location);
                        markerOptions.icon(icon);
                        mCurrLocationMarker = gMap.addMarker(markerOptions);
                    }*/

                    if (obj1.has("associations")) {
                        try {
                            JSONObject objimg = new JSONObject(obj1.getString("associations"));
                            if (objimg.isNull("store_image")) {
                                slideimg.setVisibility(View.GONE);
                                proimg.setVisibility(View.VISIBLE);
                                Glide.with(StoreListDetail.this).load(R.drawable.pevoniastore)
                                        .thumbnail(0.5f)
                                        .into(proimg);
                            } else {
                                JSONArray arr = objimg.getJSONArray("store_image");
                                //Log.e("store_image",objimg.getJSONArray("store_image")+"");
                                for (int i = 0; i < arr.length(); i++) {
                                    JSONObject obj21 = arr.getJSONObject(i);
                                    //Log.e("image", obj21.getString("image"));
                                    HashMap<String, String> file_maps = new HashMap<String, String>();
                                    file_maps.put(String.valueOf(i), WebService.store_image_url + obj21.getString("image"));
                                    for (String name : file_maps.keySet()) {
                                        DefaultSliderView defaultSliderView = new DefaultSliderView(StoreListDetail.this);
                                        defaultSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
                                        slideimg.addSlider(defaultSliderView);
                                    }
                                    slideimg.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                    slideimg.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                    slideimg.setCustomAnimation(new DescriptionAnimation());
                                    slideimg.setDuration(2500);
                                }
                            }
                            /*if (objimg.has("store_review")) {
                                    JSONArray store_review = objimg.getJSONArray("store_review");
                                    JSONObject strvalue = store_review.getJSONObject(0);
                                    name.setText(strvalue.getString("name"));
                                    review.setText(strvalue.getString("review"));
                                    review_rating.setRating(Float.parseFloat(strvalue.getString("rating")));
                                    created.setText(strvalue.getString("created_at"));                                                *//*if (strvalue.has("value")) {
                                    if (strvalue.isNull("value")) {
                                        reviewlayout.setVisibility(View.GONE);
                                    } else {
                                        JSONObject obj11 = strvalue.getJSONObject("value");
                                        name.setText(obj11.getString("name"));
                                        review.setText(obj11.getString("review"));
                                        review_rating.setRating(Float.parseFloat(obj11.getString("rating")));
                                        created.setText(obj11.getString("created_at"));
                                        //Log.e("strvalue", obj11.getString("name"));
                                    }
                                } else {
                                    reviewlayout.setVisibility(View.GONE);
                                }*//*
                            } else {
                                reviewlayout.setVisibility(View.GONE);
                            }*/

                            if (objimg.has("store_review")) {
                                JSONArray store_review = objimg.getJSONArray("store_review");
                                reviewArray = objimg.getString("store_review");
                                //Log.e("reviewArray",reviewArray);
                                for (int i = 0; i < store_review.length(); i++) {
                                    JSONObject strvalue = store_review.getJSONObject(i);
                                    //newaddlayout
                                    RelativeLayout layout = (RelativeLayout) View.inflate(StoreListDetail.this, R.layout.new_layout, null);
                                    name = ((TextView) layout.findViewById(R.id.name));
                                    review = ((TextView) layout.findViewById(R.id.review));
                                    review_rating = ((RatingBar) layout.findViewById(R.id.review_rating));
                                    created = ((TextView) layout.findViewById(R.id.created));
                                    name.setText(strvalue.getString("name"));
                                    if (strvalue.getString("review").equals("") || strvalue.getString("review") == null || strvalue.getString("review").equalsIgnoreCase("na")) {
                                        //review.setText("No Review added...");
                                        review.setText("");
                                    } else {
                                        review.setText(strvalue.getString("review"));
                                    }
                                    review_rating.setRating(Float.parseFloat(strvalue.getString("rating")));
                                    created.setText(strvalue.getString("created_at"));
                                    newaddlayout.addView(layout, i);
                                }
                                //Log.e("count", "" + newaddlayout.getChildCount());
                                if (newaddlayout.getChildCount() > reviewCount) {
                                    showreview.setVisibility(View.VISIBLE);
                                } else {
                                    showreview.setVisibility(View.GONE);
                                }
                                for (int j = reviewCount; j < newaddlayout.getChildCount(); j++) {
                                    newaddlayout.getChildAt(j).setVisibility(View.GONE);
                                }

                            } else {
                                reviewlayout.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        slideimg.setVisibility(View.GONE);
                        proimg.setVisibility(View.VISIBLE);
                        Glide.with(StoreListDetail.this).load(R.drawable.not_available)
                                .thumbnail(0.5f)
                                .into(proimg);
                        reviewlayout.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        pDialog.dismiss();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = WebService.SetAuth();
                return params;
            }
        };
        loginreq.setRetryPolicy(new DefaultRetryPolicy(
                10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        disc = (TextView) findViewById(R.id.disc);
       /* name = (TextView) findViewById(R.id.name);
        review = (TextView) findViewById(R.id.review);
        created = (TextView) findViewById(R.id.created);*/
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        proimg = (ImageView) findViewById(R.id.proimg);
        ownerimg = (ImageView) findViewById(R.id.ownerimg);
        managerimg = (ImageView) findViewById(R.id.managerimg);
        promotionimage = (ImageView) findViewById(R.id.promotionimage);
        store_rating = (RatingBar) findViewById(R.id.store_rating);
        //review_rating = (RatingBar) findViewById(R.id.review_rating);
        reviewlayout = (LinearLayout) findViewById(R.id.reviewlayout);
        ratinglayout = (LinearLayout) findViewById(R.id.ratinglayout);
        disclayout = (LinearLayout) findViewById(R.id.disclayout);
        imagelayout = (LinearLayout) findViewById(R.id.imagelayout);
        promlayout = (LinearLayout) findViewById(R.id.promlayout);
        newaddlayout = (LinearLayout) findViewById(R.id.newaddlayout);
        morelayout = (RelativeLayout) findViewById(R.id.morelayout);
        more = (TextView) findViewById(R.id.more);
        less = (TextView) findViewById(R.id.less);
        showreview = (TextView) findViewById(R.id.showreview);
        discnew = (TextView) findViewById(R.id.discnew);
        slideimg = (SliderLayout) findViewById(R.id.slideimg);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disc.setVisibility(View.VISIBLE);
                less.setVisibility(View.VISIBLE);
                morelayout.setVisibility(View.GONE);
            }
        });
        less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disc.setVisibility(View.GONE);
                less.setVisibility(View.GONE);
                morelayout.setVisibility(View.VISIBLE);
            }
        });

        showreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reviewArray != null) {
                    Intent intent = new Intent(StoreListDetail.this, ReviewList.class);
                    intent.putExtra("data", reviewArray);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Log.e("onMapReady", "onMapReady");
        gMap = googleMap;
        /*BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_map_new);
        LatLng location = new LatLng(22.3039, 70.8022);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(location);
        markerOptions.icon(icon);
        gMap.addMarker(markerOptions);
        gMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        gMap.animateCamera(CameraUpdateFactory.zoomTo(8));
        // gMap.setMyLocationEnabled(true);
        gMap.getUiSettings().setMyLocationButtonEnabled(true);
        //mCurrLocationMarker = gMap.addMarker(markerOptions);*/
    }
}
