package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CommissionAdapter;
import com.pevonia.bean.CommissionBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyCommission extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
    TextView tprofile, nodata;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog, pcDialog;
    RecyclerView commission_history;
    List<CommissionBean> commissionBean = new ArrayList<>();
    private CommissionAdapter commissionAdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_commission);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        SetUpViews();
        detector = new ConnectionDetector(MyCommission.this);
        manager = new UserSessionManager(MyCommission.this);
        map = manager.getUserDetail();
        if (detector.isConnectingToInternet()) {
            GetCummissionRate();
        } else {
            WebService.MakeToast(MyCommission.this, getString(R.string.check_internet));
        }
        if (detector.isConnectingToInternet()) {

            GetCummission();
        } else {
            WebService.MakeToast(MyCommission.this, getString(R.string.check_internet));
        }
    }

    private void GetCummissionRate() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(MyCommission.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Dealer_Commission + "[" + map.get(UserSessionManager.KEY_USERID) + "]";
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        //Log.e("Dealer_Commission", response);
                        if (!object.isNull("dealer_commissionss")) {
                            JSONArray commissionarray = object.getJSONArray("dealer_commissionss");
                            for (int i = 0; i < commissionarray.length(); i++) {
                                JSONObject object1 = commissionarray.getJSONObject(i);
                                if (!object1.isNull("commission")) {
                                    WebService.dealer_Commission_rate = object1.getString("commission");
                                    //Log.e("dealer_Commission_rate", WebService.dealer_Commission_rate);
                                } else {
                                    WebService.dealer_Commission_rate = "";
                                }
                            }
                        } else {
                            WebService.MakeToast(MyCommission.this, "No commission to display");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyCommission.this, getString(R.string.check_internet));
        }
    }

    private void GetCummission() {
        if (detector.isConnectingToInternet()) {
            pcDialog = new ProgressDialog(MyCommission.this);
            pcDialog.setMessage("Loading...");
            pcDialog.setCancelable(false);
            pcDialog.show();
            String url = WebService.Dealer_order + map.get(UserSessionManager.KEY_USERID) + WebService.order_Commission;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pcDialog.dismiss();
                    try {
                        JSONObject object = new JSONObject(response);
                        //Log.e("Dealer_order_Commission", response);
                        if (!object.isNull("order")) {
                            JSONArray array = object.getJSONArray("order");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                CommissionBean bean = new CommissionBean();
                                bean.setId(object1.getString("id"));
                                bean.setCurrecy_id(object1.getString("id_currency"));
                                bean.setCustomerName(object1.getString("customer_name"));
                                bean.setInvoice_date(object1.getString("date_add"));
                                bean.setPayment(object1.getString("payment"));
                                bean.setStatus(object1.getString("current_state"));
                                bean.setTotal_paid(object1.getString("total_paid"));
                                bean.setOrder_reference(object1.getString("reference"));
                                commissionBean.add(bean);
                               /* Log.e("id",object1.getString("id"));
                                Log.e("id_currency",object1.getString("id_currency"));
                                Log.e("customer_name",object1.getString("customer_name"));
                                Log.e("current_state",object1.getString("current_state"));
                                Log.e("date_add",object1.getString("date_add"));
                                Log.e("payment",object1.getString("payment"));
                                Log.e("total_paid",object1.getString("total_paid"));
                                Log.e("reference",object1.getString("reference"));*/
                            }
                            if (commissionBean.size() == 0) {
                                nodata.setVisibility(View.VISIBLE);
                            } else {
                                commissionAdp = new CommissionAdapter(commissionBean, MyCommission.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyCommission.this);
                                commission_history.setLayoutManager(mLayoutManager);
                                commission_history.setItemAnimator(new DefaultItemAnimator());
                                commission_history.setAdapter(commissionAdp);
                            }
                        } else {
                            WebService.MakeToast(MyCommission.this, "No Orders to display");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pcDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyCommission.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        nodata = (TextView) findViewById(R.id.nodata);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        commission_history = (RecyclerView) findViewById(R.id.commission_history);
    }
}
