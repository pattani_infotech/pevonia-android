package com.pevonia.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;

import java.util.HashMap;
import java.util.Locale;

public class Splash extends AppCompatActivity {
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String,String>lcmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector = new ConnectionDetector(Splash.this);
        manager = new UserSessionManager(Splash.this);
        map = manager.getUserDetail();
        lcManager=new LCManager(Splash.this);
        lcmap=lcManager.getLC();
        if(lcmap.get(LCManager.ln_code)!=null)
        {
            setLocale(new Locale(lcmap.get(LCManager.ln_code).toString()));
        }



        runTask();
       /* Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(2500);

                        Intent intent = new Intent(Splash.this, Login.class);
                        startActivity(intent);
                        finish();

                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        };
        thread.start();*/
    }

    private void runTask() {
        if (!detector.isConnectingToInternet()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("No internet");
            builder.setMessage("Internet is required. please retry.");

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });

            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    runTask();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            Toast.makeText(this, "Network unavailable!", Toast.LENGTH_LONG).show();


        } else {
            Thread thread = new Thread() {
                public void run() {
                    try {
                        sleep(1500);
                        //if (map.get(UserSessionManager.KEY_USERID)!=null){
                        //Intent intent=new Intent( Splash.this,Navigation.class );
                        //intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        //startActivity( intent );
                        //finish();
                        /*}else{
                            Intent intent = new Intent(Splash.this, Login.class);
                            startActivity(intent);
                              finish();
                        }*/
                        if (lcmap.get(LCManager.languageID) != null) {
                            Intent intent = new Intent(Splash.this, Navigation.class);
                            //intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(Splash.this, Language.class);
                            //intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity(intent);
                            finish();
                        }
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            };
            thread.start();
        }
    }

    private void setLocale(Locale locale) {
        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }
}
