package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.MyOrderAdapter;
import com.pevonia.bean.MyOrderBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyOrder extends AppCompatActivity {

    HashMap<String,String> map;
    ConnectionDetector detector;
    UserSessionManager sessionManager;
    ImageButton back;
    ImageView nodata;
    Button shopcontinue;
    FrameLayout framelayout;
    private RecyclerView order_history;
    private ProgressDialog pDialog;
    List<MyOrderBean> myOrderBeen;
    private MyOrderAdapter myorderadapte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sessionManager=new UserSessionManager(MyOrder.this);
        map=sessionManager.getUserDetail();
        detector=new ConnectionDetector(MyOrder.this);
        SetUpViews();
        //Log.e("OrderHis",map.get(UserSessionManager.KEY_USERID)+" ");
        if (detector.isConnectingToInternet()) {
            GetOrderHistory();
        } else {
            WebService.MakeToast(MyOrder.this, getString(R.string.check_internet));
        }

    }

    private void SetUpViews() {
        nodata = (ImageView)findViewById(R.id.nodata);
        order_history=(RecyclerView)findViewById(R.id.order_history);
        myOrderBeen=new ArrayList<>();
        back= (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        shopcontinue= (Button) findViewById(R.id.shopcontinue);
        framelayout= (FrameLayout) findViewById(R.id.framelayout);
        shopcontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyOrder.this,ProductActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });
    }

    private void GetOrderHistory() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(MyOrder.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.OrderHisory+"["+map.get(UserSessionManager.KEY_USERID).toString()+"]";
            //Log.e("url navi", url);

            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    //Log.e("OrderHisory",response);
                    if(response.trim().charAt(0) == '[') {
                        //Log.e("Response is : " , "JSONArray");
                        try {
                            JSONArray jsonArray =new JSONArray(response);
                            if(jsonArray.length()<=0) {
                                framelayout.setVisibility(View.VISIBLE);
                                order_history.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if(response.trim().charAt(0) == '{') {
                        try {
                            //Log.e("OrderHisory", response);
                            JSONObject object = new JSONObject(response);
                            if (!object.isNull("orders")) {
                                //Log.e("OrderHisory", response);
                                JSONArray orderarray = object.getJSONArray("orders");
                                for (int i = 0; i < orderarray.length(); i++) {
                                    MyOrderBean myOrderBean = new MyOrderBean();
                                    JSONObject object1 = orderarray.getJSONObject(i);
                                    myOrderBean.setOrder_id(object1.getString("id"));
                                    myOrderBean.setInvoice_date(object1.getString("invoice_date"));
                                    myOrderBean.setOrder_reference(object1.getString("reference"));
                                    myOrderBean.setPayment(object1.getString("payment"));
                                    myOrderBean.setTotal_paid(object1.getString("total_paid"));
                                    myOrderBean.setCurrecy_id(object1.getString("id_currency"));
                                    myOrderBeen.add(myOrderBean);
                                    //Log.e("myOrderBeen",myOrderBeen.size()+" ");
                                }
                                if (myOrderBeen.size() == 0) {
                                    framelayout.setVisibility(View.VISIBLE);
                                } else {
                                    myorderadapte = new MyOrderAdapter(myOrderBeen, MyOrder.this);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyOrder.this);
                                    order_history.setLayoutManager(mLayoutManager);
                                    order_history.setItemAnimator(new DefaultItemAnimator());
                                    order_history.setAdapter(myorderadapte);
                                }
                            } else {
                                WebService.MakeToast(MyOrder.this, "No orders to display");
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(MyOrder.this, getString(R.string.check_internet));
        }
    }
}
