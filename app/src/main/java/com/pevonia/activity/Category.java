package com.pevonia.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.adapter.CategoryAdapter;
import com.pevonia.bean.CategoryBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Category extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
     public static TextView tprofile,nodata;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    EditText search;
    public static RecyclerView categorylist;
    List<CategoryBean> categoryBean = new ArrayList<>();
    CategoryAdapter cAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        detector = new ConnectionDetector(Category.this);
        manager = new UserSessionManager(Category.this);
        map = manager.getUserDetail();
        SetUpViews();
        if (detector.isConnectingToInternet()){
            GetCategoryList();
        }else {
            WebService.MakeToast(Category.this, getString(R.string.check_internet));
        }
    }

    private void GetCategoryList() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(Category.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Category_List;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        Log.e("Category_List",response);
                        JSONObject object=new JSONObject(response);
                        if(!object.isNull("categories")) {
                            JSONArray custarry=object.getJSONArray("categories");
                            for(int i=0;i<custarry.length();i++) {
                                JSONObject object1=custarry.getJSONObject(i);
                                CategoryBean bean=new CategoryBean();
                                //Log.e("id",object1.getString("id"));
                                //Log.e("nb_products_recursive",object1.getString("nb_products_recursive"));
                                //Log.e("position",object1.getString("position"));
                                //Log.e("name",object1.getString("name"));
                                //Log.e("link_rewrite",object1.getString("link_rewrite"));

                                bean.setId(object1.getString("id"));
                                bean.setNb_products_recursive(object1.getString("nb_products_recursive"));
                                bean.setPosition(object1.getString("position"));
                                bean.setName(object1.getString("name"));
                                bean.setLink_rewrite(object1.getString("link_rewrite"));
                                categoryBean.add(bean);
                                //Log.e("myOrderBeen",myOrderBeen.size()+" ");
                            }
                            if (categoryBean.size()==0){
                                nodata.setVisibility(View.VISIBLE);
                            }else {
                                cAdapter = new CategoryAdapter(categoryBean, Category.this);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Category.this);
                                categorylist.setLayoutManager(mLayoutManager);
                                categorylist.setAdapter(cAdapter);
                            }
                        }else{
                            WebService.MakeToast(Category.this, "No category found");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(Category.this, getString(R.string.check_internet));
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        nodata = (TextView) findViewById(R.id.nodata);
        back = (ImageButton) toolbar.findViewById(R.id.back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        search= (EditText) findViewById(R.id.search);
        categorylist= (RecyclerView) findViewById(R.id.categorylist);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.equals("")) {
                    cAdapter.getFilter().filter(s.toString());
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }
}
