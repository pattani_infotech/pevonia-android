package com.pevonia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity {
EditText currentpassword,newpassword,confirmpass;
    Button submit;
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        detector = new ConnectionDetector(ChangePassword.this);
        manager = new UserSessionManager(ChangePassword.this);
        map = manager.getUserDetail();
        SetUpViews();
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        currentpassword= (EditText) findViewById(R.id.currentpassword);
        newpassword= (EditText) findViewById(R.id.newpassword);
        confirmpass= (EditText) findViewById(R.id.confirmpass);
        submit= (Button) findViewById(R.id.submit);
        currentpassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (currentpassword.getRight() - currentpassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        currentpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        currentpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    currentpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    currentpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;
            }
        });
        confirmpass.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (confirmpass.getRight() - confirmpass.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        confirmpass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        confirmpass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    confirmpass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    confirmpass.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;
            }
        });
        newpassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (newpassword.getRight() - newpassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        newpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        newpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_close_eye, 0);
                        return true;
                    }
                    newpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    newpassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_open_eye, 0);
                }
                return false;
            }
        });
        confirmpass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ChangePasswordProcess();
                }
                return false;
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangePasswordProcess();
            }
        });
    }

    private void ChangePasswordProcess() {
        if (currentpassword.getText().toString().length()<4){
            currentpassword.setError("Please type current password");
        }else if (newpassword.getText().toString().length()<4){
            newpassword.setError("Please type password...");
        }else if (!newpassword.getText().toString().equals(confirmpass.getText().toString())){
            confirmpass.setError("Password doesn't match please try again...");
        }else {
            if (detector.isConnectingToInternet()) {
                GetChangePassword();
            } else {
                WebService.MakeToast(ChangePassword.this, getString(R.string.check_internet));
            }
        }
    }

    private void GetChangePassword() {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.Change_Password;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        //String fic_response = WebService.fixEncoding(response);
                         //Log.e("Change_Password", response + "");
                        JSONObject object=new JSONObject(response);
                        if (response!=null){
                            if (object.has("code")){
                                if (object.getString("code").equals("200")){
                                    WebService.MakeToast(ChangePassword.this, "Password change successfully...");
                                    startActivity(new Intent(ChangePassword.this,Navigation.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                }else if (object.getString("code").equals("404")){
                                    WebService.MakeToast(ChangePassword.this, "Current password incorrect...");
                                }else {
                                    WebService.MakeToast(ChangePassword.this, "Please try again...");
                                }
                            }else {
                                WebService.MakeToast(ChangePassword.this, "Please try again...");
                            }
                        }else {
                            WebService.MakeToast(ChangePassword.this, "Please try again...");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                    params.put("id",map.get(UserSessionManager.KEY_USERID));
                    params.put("current_password",currentpassword.getText().toString());
                    params.put("new_password",confirmpass.getText().toString());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }

            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(ChangePassword.this, getString(R.string.check_internet));
        }
    }
}
