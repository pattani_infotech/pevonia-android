package com.pevonia.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ReviewList extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LinearLayout addlayout;
    TextView name,review,created;
    RatingBar review_rating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        overridePendingTransition(R.anim.buttom_up, R.anim.buttom_down);
        //overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector = new ConnectionDetector(ReviewList.this);
        manager = new UserSessionManager(ReviewList.this);
        map = manager.getUserDetail();
        SetUpViews();
        if (getIntent().hasExtra("data")) {
                GetReviewDetails(getIntent().getStringExtra("data"));
        }else {
            WebService.MakeToast(ReviewList.this, "No review found...");
        }
    }

    private void GetReviewDetails(String data) {
        //Log.e("data",data);
        try {
            JSONArray store_review = new JSONArray(data);
            for (int i=0;i<store_review.length();i++){
                JSONObject strvalue = store_review.getJSONObject(i);
                RelativeLayout layout = (RelativeLayout) View.inflate(ReviewList.this, R.layout.new_layout, null);
                name=((TextView) layout.findViewById(R.id.name));
                review=((TextView) layout.findViewById(R.id.review));
                review_rating=((RatingBar) layout.findViewById(R.id.review_rating));
                created=((TextView) layout.findViewById(R.id.created));
                name.setText(strvalue.getString("name"));
                if (strvalue.getString("review").equals("")||strvalue.getString("review")==null||strvalue.getString("review").equalsIgnoreCase("na")){
                    //review.setText("No Review added...");
                    review.setText("");
                }else {
                    review.setText(strvalue.getString("review"));
                }
                review_rating.setRating(Float.parseFloat(strvalue.getString("rating")));

                created.setText(strvalue.getString("created_at"));
                addlayout.addView(layout,i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) toolbar.findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tprofile.setAllCaps(false);
        tprofile.setText("Store Reviews");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();

            }
        });
        addlayout= (LinearLayout) findViewById(R.id.addlayout);
        name= (TextView) findViewById(R.id.name);
        review= (TextView) findViewById(R.id.review);
        created= (TextView) findViewById(R.id.created);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.buttom_up, R.anim.buttom_down);
        //overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
