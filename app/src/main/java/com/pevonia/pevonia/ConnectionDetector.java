package com.pevonia.pevonia;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Naman on 1/31/2017.
 */

// checking Internet start or not


public class ConnectionDetector {

    private Context _context;
    public ConnectionDetector(Context context){
        this._context = context;
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null)
        {
            NetworkInfo nInfo=connectivity.getActiveNetworkInfo();

            if (nInfo != null && nInfo.getState()== NetworkInfo.State.CONNECTED) {
                //do your thing
                return  true;
            }

        }
        return false;
    }


}
