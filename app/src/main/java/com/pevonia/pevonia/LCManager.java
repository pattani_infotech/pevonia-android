package com.pevonia.pevonia;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by vimalkumar on 3/7/2017.
 */

public class LCManager {

    SharedPreferences Lcpref;
    SharedPreferences.Editor Lceditor;

    private  static  final String IS_USER_LOGIN="IsUserLogInLC";
    public static  final String REG_AS="reg_as";
    //Context
    Context _contex;
    //Shared Prefernce Mode
    int PRIVATE_MODE=0;
    private  static final String PREFER_NAME="PevoniaLCSessionManager";
    //PHP Field Name
    public static final String languageID="lid";
    public static final String currencyID="cid";
    public static final String ln_code="ln_code";

    // Create Constructore
    public LCManager(Context context) {
        this._contex=context;
        // STORE DATA IN PREFER_NAME,PRIVATE_MODE
        Lcpref=_contex.getSharedPreferences(PREFER_NAME,PRIVATE_MODE);
        Lceditor=Lcpref.edit();
    }

    // set Language
    public void languageStore( String lid,String cid,String code){
        Lceditor.putBoolean(IS_USER_LOGIN,true);
        Lceditor.putString(languageID,lid);
        Lceditor.putString(currencyID,cid);
        Lceditor.putString(ln_code,code);
        Lceditor.commit();
    }
    public HashMap<String,String>getLC() {
        HashMap<String,String> user=new HashMap<String, String>();
        user.put(languageID,Lcpref.getString(languageID,null));
        user.put(currencyID,Lcpref.getString(currencyID,null));
        user.put(ln_code,Lcpref.getString(ln_code,null));
        return user;
    }
    public boolean isUserLogin()
    {
        return Lcpref.getBoolean(IS_USER_LOGIN,false);
    }



}
