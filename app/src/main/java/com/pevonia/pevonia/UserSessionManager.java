package com.pevonia.pevonia;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.pevonia.activity.Navigation;

import java.util.HashMap;

/**
 * Created by vimalkumar on 3/7/2017.
 */

public class UserSessionManager {

    SharedPreferences pref;


    //Editor reference for Shared preferences
    SharedPreferences.Editor editor;


    private  static  final String IS_USER_LOGIN="IsUserLogIn";
    public static  final String REG_AS="reg_as";

    //Context
    Context _contex;

    //Shared Prefernce Mode
    int PRIVATE_MODE=0;


    private  static final String PREFER_NAME="PevoniaSessionManager";

    //PHP Field Name

    public static final String KEY_USERID="uid";
    public static final String KEY_GroupID="groupid";
    public static final String KEY_USERNAME="username";
    public static final String KEY_fname="fname";
    public static final String KEY_lname="lname";
    public static final String KEY_shope_id="id_shop";
    public static final String KEY_shope_group="id_shop_group";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_TYPE="type";
    public static final String KEY_BRANCHID="branch_id";
    public static final String KEY_COMPANYID="company_id";
    public static final String KEY_CREDIT="credit";
    public static final String KEY_POINTS="points";
    public static final String KEY_IMG="avatar";
    public static final String KEY_MOBILE="mobile";
    public static final String KEY_CITY="city";
    public static final String KEY_DOB="dob";
    public static final String KEY_BRANCH="branch_name";
    public static final String KEY_DAY="dayappnt";
    public static final String KEY_Curreny="currency";

    // Create Constructore

    public UserSessionManager(Context context) {
        this._contex=context;
        // STORE DATA IN PREFER_NAME,PRIVATE_MODE
        pref=_contex.getSharedPreferences(PREFER_NAME,PRIVATE_MODE);
        editor=pref.edit();
    }

    public void createUserLoginSession( String id,String fname,String lname,String groupid,String shopid,String shopgroup) {

        editor.putBoolean(IS_USER_LOGIN,true);
        // Store All Data in Preference
        //Log.e("crearte",id+" "+groupid);
        editor.putString(KEY_USERID,id);
        editor.putString(KEY_GroupID,groupid);
        editor.putString(KEY_fname,fname);
        editor.putString(KEY_lname,lname);
        editor.putString(KEY_shope_id,shopid);
        editor.putString(KEY_shope_group,shopgroup);
        /*editor.putString(KEY_FIRSTNAME,f_name);
        editor.putString(KEY_LASTNAME,l_name);
        editor.putString(KEY_EMAIL,email);
        editor.putString(KEY_PASSWORD,pass);
        editor.putString(KEY_TYPE, type);
        editor.putString(KEY_BRANCHID,branch_id);
        editor.putString(KEY_COMPANYID,comp_id);
        editor.putString(KEY_CREDIT,credi);
        editor.putString(KEY_POINTS,pont);
        editor.putString(KEY_IMG,img);
        editor.putString(KEY_MOBILE,mobile);
        editor.putString(KEY_CITY,city);
        editor.putString(KEY_DOB,dob);
        editor.putString(KEY_BRANCH,branch);
        editor.putString(KEY_DAY,day);
        editor.putString(KEY_Curreny,currency);*/

        editor.commit();
    }

    // Data Check and Store
    public HashMap<String,String>getUserDetail()
    {
        HashMap<String,String> user=new HashMap<String, String>();

        user.put(KEY_USERID,pref.getString(KEY_USERID,null));
        user.put(KEY_GroupID,pref.getString(KEY_GroupID,null));
        user.put(KEY_fname,pref.getString(KEY_fname,null));
        user.put(KEY_lname,pref.getString(KEY_lname,null));
        user.put(KEY_shope_id,pref.getString(KEY_shope_id,null));
        user.put(KEY_shope_group,pref.getString(KEY_shope_group,null));
        /*user.put(KEY_USERNAME,pref.getString(KEY_USERNAME,null));
        user.put(KEY_FIRSTNAME,pref.getString(KEY_FIRSTNAME,null));
        user.put(KEY_LASTNAME,pref.getString(KEY_LASTNAME,null));
        user.put(KEY_EMAIL,pref.getString(KEY_EMAIL,null));
        user.put(KEY_PASSWORD,pref.getString(KEY_PASSWORD,null));
        user.put(KEY_TYPE,pref.getString(KEY_TYPE,null));
        user.put(KEY_BRANCHID,pref.getString(KEY_BRANCHID,null));
        user.put(KEY_COMPANYID,pref.getString(KEY_COMPANYID,null));
        user.put(KEY_CREDIT,pref.getString(KEY_CREDIT,null));
        user.put(KEY_POINTS,pref.getString(KEY_POINTS,null));
        user.put(KEY_IMG,pref.getString(KEY_IMG,null));
        user.put(KEY_MOBILE,pref.getString(KEY_MOBILE,null));
        user.put(KEY_CITY,pref.getString(KEY_CITY,null));
        user.put(KEY_DOB,pref.getString(KEY_DOB,null));
        user.put(KEY_BRANCH,pref.getString(KEY_BRANCH,null));
        user.put(KEY_DAY,pref.getString(KEY_DAY,null));
        user.put(KEY_Curreny,pref.getString(KEY_Curreny,null));*/
        return user;
    }

    // Log Out
    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent intent=new Intent(_contex, Navigation.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _contex.startActivity(intent);

    }

    // check whether user login or not
    /*public void languageStore( String lid,String cid){
        Lceditor.putBoolean(IS_USER_LOGIN,true);
        Lceditor.putString(languageID,lid);
        Lceditor.putString(currencyID,cid);
        Lceditor.commit();
    }
    public HashMap<String,String>getLC() {
        HashMap<String,String> user=new HashMap<String, String>();
        user.put(languageID,Lcpref.getString(languageID,null));
        user.put(currencyID,Lcpref.getString(currencyID,null));
        return user;
    }*/
    public boolean isUserLogin()
    {
        return pref.getBoolean(IS_USER_LOGIN,false);
    }

}
