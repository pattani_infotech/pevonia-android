package com.pevonia.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.activity.PevoniaApp;
import com.pevonia.adapter.StoreListAdapter;
import com.pevonia.bean.StoreListBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Store extends Fragment {
    private View v;
    Context mContext;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    TextView nodata;
    private RecyclerView store_listing;
    private ProgressDialog pDialog;
    List<StoreListBean> storeListBean = new ArrayList<>();
    public static StoreListAdapter sAdapter;
    private int position=0;
    boolean loadingMore = false;
    SwipeRefreshLayout swipe;
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.store, container, false);
        detector = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        SetupView();
        if (detector.isConnectingToInternet()) {
            GetStoreList(String.valueOf(position));
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }
        swipe.setColorSchemeResources(R.color.blue,R.color.yellow,R.color.accent);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                ( new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipe.setRefreshing(false);
                        GetStoreList(String.valueOf(position));
                    }
                }, 500);
            }
        });

        return v;
    }

    private void GetStoreList(String first) {
        if (detector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            if (first.equals("0")) {
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
            String url = WebService.StoreListing+ first + ",10&"+WebService.output_format;
            loadingMore = true;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        JSONObject object=new JSONObject(response);
                        //Log.e("StoreListing",response);
                        if(!object.isNull("dealer_stores")) {
                            JSONArray orderarray=object.getJSONArray("dealer_stores");
                            for(int i=0;i<orderarray.length();i++) {
                                JSONObject object1=orderarray.getJSONObject(i);
                                StoreListBean bean=new StoreListBean();
                                bean.setId(object1.getString("id"));
                                bean.setStore_name(object1.getString("store_name"));
                                bean.setCity(object1.getString("city"));
                                if(object1.has("associations"))
                                {
                                    bean.setStore_image(object1.getString("associations"));
                                }else{
                                    bean.setStore_image("NA");
                                }
                                bean.setStore_rating(object1.getString("store_rating"));

/*                                Log.e("id",object1.getString("id"));
                                Log.e("store_name",object1.getString("store_name"));
                                Log.e("city",object1.getString("city"));
                                Log.e("store_image",object1.getString("store_image"));
                                Log.e("postcode",object1.getString("postcode"));
                                Log.e("latitude",object1.getString("latitude"));
                                Log.e("longitude",object1.getString("longitude"));
                                Log.e("associations",object1.getString("associations"));*/
                                if(i==(orderarray.length()-1)) {
                                    position=(position+i+1);
                                    //position=Integer.toString(proGridBean.size());
                                }
                                storeListBean.add(0,bean);
                            }
                            if (storeListBean.size()==0){
                                nodata.setVisibility(View.VISIBLE);
                            }else {
                                sAdapter = new StoreListAdapter(storeListBean, mContext);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                store_listing.setLayoutManager(mLayoutManager);
                                store_listing.setItemAnimator(new DefaultItemAnimator());
                                store_listing.setAdapter(sAdapter);
                                loadingMore = false;
                            }

                        }else{
                            WebService.MakeToast(getActivity(), "No orders to display");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                            loadingMore = false;
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }
    }

    private void SetupView() {
        swipe = (SwipeRefreshLayout) v.findViewById(R.id.swipestore);
        nodata = (TextView)v.findViewById(R.id.nodata);
        store_listing=(RecyclerView)v.findViewById(R.id.store_listing);
    }
    public static void SearchFilter(String s) {
        //Log.e("Store",s+"");
        sAdapter.getFilter().filter(s.toString());
    }
}