package com.pevonia.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pevonia.R;
import com.pevonia.activity.ChangePassword;
import com.pevonia.activity.Login;
import com.pevonia.activity.PevoniaApp;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UserAccount extends Fragment {
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    EditText fname, lname, email, dob, username;
    Button editprofile, login;
    RelativeLayout loginlayout;
    LinearLayout profilelayout;
    TextView changepass;
    private View v;

    static final int DATE_DIALOG_ID = 0;
    private int mYear, mMonth, mDay;
    Calendar c = Calendar.getInstance();
    DatePickerDialog datePickerDialog;

    String currentdate;
    String datef = null;
    Date date1 = null;
    Date date2=null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.account, container, false);
        cd = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        SetupView();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        if (cd.isConnectingToInternet()) {
            GetCustomerDetails();
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }
        return v;
    }

    private void GetCustomerDetails() {
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            loginlayout.setVisibility(View.GONE);
            profilelayout.setVisibility(View.VISIBLE);
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.show();
            String url = WebService.customerDetail + map.get(UserSessionManager.KEY_USERID) + "&" + WebService.output_format;
            //Log.e("customerID", map.get(UserSessionManager.KEY_USERID));
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.setCancelable(false);
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        //Log.e("customerDetail", fic_response + "");
                        JSONObject obj = new JSONObject(fic_response);
                        JSONObject obj1 = obj.getJSONObject("customer");
                        //Log.e("firstname", obj1.getString("firstname"));
                        fname.setText(obj1.getString("firstname"));
                        lname.setText(obj1.getString("lastname"));
                        email.setText(obj1.getString("email"));
                        username.setText(obj1.getString("username"));
                        dob.setText(obj1.getString("birthday"));
                        WebService.firstname=obj1.getString("firstname");
                        WebService.lastname=obj1.getString("lastname");
                   /* // Log.e("length", obj.length() + "");
                    JSONArray ary = obj.getJSONArray("products");
                    for (int i = 0; i < ary.length(); i++) {
                        JSONObject obj1 = ary.getJSONObject(i);
                        PerentBean bean = new PerentBean();
                        bean.setId(obj1.getString("id"));
                        bean.setPrice(obj1.getString("price"));
                        bean.setName(obj1.getString("name"));
                        bean.setId_default_image(obj1.getString("id_default_image"));

                    }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.setCancelable(false);
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            loginlayout.setVisibility(View.VISIBLE);

           profilelayout.setVisibility(View.GONE);
            WebService.MakeToast(getActivity(),"Please login to continue");
            Intent intent=new Intent(getContext(),Login.class);
            startActivity(intent);
            getActivity().finish();
            /*Intent intent=new Intent(getActivity(),Login.class);
            getActivity().startActivity(intent);
            getActivity().finish();*/
        }
    }

    private void SetupView() {
        fname = (EditText) v.findViewById(R.id.fname);
        lname = (EditText) v.findViewById(R.id.lname);
        email = (EditText) v.findViewById(R.id.email);
        dob = (EditText) v.findViewById(R.id.dob);
        username = (EditText) v.findViewById(R.id.username);
        editprofile = (Button) v.findViewById(R.id.editprofile);
        login = (Button) v.findViewById(R.id.login);
        changepass = (TextView) v.findViewById(R.id.changepass);
        profilelayout = (LinearLayout) v.findViewById(R.id.profilelayout);
        loginlayout = (RelativeLayout) v.findViewById(R.id.loginlayout);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getContext(), Login.class));
            }
        });
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetDatePicker();
            }
        });
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileProcess();
            }
        });
        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),ChangePassword.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
    }

    private void EditProfileProcess() {

        SimpleDateFormat mSDF = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            datef= mSDF.format(formatter.parse(dob.getText().toString()));
            date1 = mSDF.parse(datef);
            currentdate=mSDF.format(formatter.parse(WebService.CurrentDate()));
            date2=mSDF.parse(currentdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (fname.getText().toString().equals("")||!WebService.isValidName(fname.getText().toString())) {
            fname.setError("Please type valid first name...");
        } else if (lname.getText().toString().equals("")||!WebService.isValidName(lname.getText().toString())) {
            lname.setError("Please type valid last name...");
        } else if (username.getText().toString().equals("")) {
            username.setError("Please type username...");
        } else if (dob.getText().toString().equals("")) {
            dob.setError("Please select date...");
        } else if ((date1.compareTo(date2) > 0)) {
            WebService.MakeToast(getActivity(),"Please select currect date...!");
        } else {
             /*if(!WebService.isValidName(fname.getText().toString()))
             {
                 fname.setError("Please Type valid First Name...");
             }*/
           /* Log.e("id", map.get(UserSessionManager.KEY_USERID));
            Log.e("firstname", fname.getText().toString());
            Log.e("lastname", lname.getText().toString());
            Log.e("email", email.getText().toString());
            Log.e("username", username.getText().toString());
            Log.e("birthday", dob.getText().toString());*/
            if (cd.isConnectingToInternet()) {
                GetEditProfile();
            } else {
                WebService.MakeToast(getActivity(), getString(R.string.check_internet));
            }
        }
    }

    private void GetEditProfile() {
        if (cd.isConnectingToInternet()) {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
            String url = WebService.EditProfile;
            StringRequest loginreq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        //String fic_response = WebService.fixEncoding(response);
                       // Log.e("EditProfile", response + "");
                        JSONObject object=new JSONObject(response);
                        if (response!=null){
                            if (object.has("code")){
                                if (object.getString("code").equals("200")){
                                    WebService.MakeToast(getActivity(), "User updated successfully...");
                                    manager.createUserLoginSession(map.get(UserSessionManager.KEY_USERID),fname.getText().toString(),lname.getText().toString(),map.get(UserSessionManager.KEY_GroupID),
                                            map.get(UserSessionManager.KEY_shope_id),map.get(UserSessionManager.KEY_shope_group));
                                }else {
                                    WebService.MakeToast(getActivity(), "Please try again...");
                                }
                            }else {
                                WebService.MakeToast(getActivity(), "Please try again...");
                            }
                        }else {
                            WebService.MakeToast(getActivity(), "Please try again...");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                        params.put("id",map.get(UserSessionManager.KEY_USERID));
                        //params.put("passwd",rpassword.getText().toString());
                        params.put("lastname",lname.getText().toString());
                        params.put("firstname",fname.getText().toString());
                        params.put("email",email.getText().toString());
                        params.put("username",username.getText().toString());
                        params.put("birthday",dob.getText().toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }

            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }
    }

    private void SetDatePicker() {
        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mnth = Integer.toString(monthOfYear + 1);
                String d_day = Integer.toString(dayOfMonth);
                if (mnth.length() == 1) {
                    mnth = "0" + mnth;
                }
                if (d_day.length() == 1) {
                    d_day = "0" + d_day;
                }
                dob.setText(year + "-" + mnth + "-" + d_day);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

}