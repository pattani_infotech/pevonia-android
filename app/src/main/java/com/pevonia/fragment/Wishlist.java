package com.pevonia.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pevonia.R;
import com.pevonia.activity.Login;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.WebService;

import java.util.HashMap;

public class Wishlist extends Fragment {
    private View v;
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v= inflater.inflate(R.layout.wishlist, container, false);
        cd = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        SetupView();
        if (map.get(UserSessionManager.KEY_USERID)!=null){

        }else {
            WebService.MakeToast(getActivity(),"Please login to continue");
            Intent intent=new Intent(getContext(),Login.class);
            startActivity(intent);
            getActivity().finish();
        }
        return v;
    }
    private void SetupView() {}
}