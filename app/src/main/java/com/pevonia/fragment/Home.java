package com.pevonia.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.pevonia.R;
import com.pevonia.activity.PevoniaApp;
import com.pevonia.adapter.ProGridAdaptern;
import com.pevonia.bean.PerentBean;
import com.pevonia.bean.ProGridBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.EndlessScrollListener;
import com.pevonia.util.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Home extends Fragment {
    SliderLayout slideimg;
    Context mContext;
    public static GridView gridview;
    public static ImageView nodata;
    public static ListView list;
    ConnectionDetector connectionDetector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private ProgressDialog pDialog;
    List<ProGridBean> proGridBean = new ArrayList<>();
    //public static ProGridAdapter pgAdapter;
    public static ProGridAdaptern pgAdapter;
    List<PerentBean> pBean = new ArrayList<>();
String first="0";
    private int position=0;
    boolean loadingMore = false;
    //SwipeRefreshLayout swipe;
    NestedScrollView nestedScroll;
    private View v;
    private int current_index;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }



    @SuppressLint("NewApi")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.home, container, false);


        slideimg = (SliderLayout) v.findViewById(R.id.slideimg);
        gridview = (GridView) v.findViewById(R.id.gridview);
        list = (ListView) v.findViewById(R.id.list);
        nodata = (ImageView) v.findViewById(R.id.nodata);
        //nestedScroll= (NestedScrollView) v.findViewById(R.id.nestedScroll);
        //swipe = (SwipeRefreshLayout) v.findViewById(R.id.swipe);
        connectionDetector = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        Imageslidelogo();
       /* PrepareData();
        pgAdapter = new ProGridAdapter(proGridBean, getActivity());
        gridview.setAdapter(pgAdapter);
        list.setAdapter(pgAdapter);*/
        if (connectionDetector.isConnectingToInternet()) {
            GetProduct(String.valueOf(position));
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }


        gridview.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView
               // int lastInScreen = firstVisibleItem + visibleItemCount;
               // if ((position == lastInScreen) && !(loadingMore)) {
                 current_index = gridview.getLastVisiblePosition();
                Log.e("current_index",current_index+"");
                   // GetProduct(String.valueOf(position));
                    Log.e("sss",(position-1)+"");
                    // gridview.setSelection(position);
               // }
                // or loadNextDataFromApi(totalItemsCount);
                return true; // ONLY if more data is actually being loaded; false otherwise.
            }
        });


        return v;
    }

    public void GetProduct(final String first) {
        if (connectionDetector.isConnectingToInternet()) {
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            //if (first.equals("0")) {
                pDialog.show();
            /*}else{
                pDialog.dismiss();
            }*/
            //String url = WebService.product;
            //String url = "http://www.pevonia.com.my/demo/api/products?display=full&limit="+first+",10&output_format=JSON";
            String url = WebService.product+first+",10&"+WebService.output_format;
            //Log.e("url navi", url);
            loadingMore = true;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        //Log.e("product", fic_response + "");
                        JSONObject obj = new JSONObject(fic_response);
                        // Log.e("length", obj.length() + "");
                        JSONArray ary = obj.getJSONArray("products");
                        for (int i = 0; i < ary.length(); i++) {
                            JSONObject obj1 = ary.getJSONObject(i);
                            PerentBean bean = new PerentBean();
                            bean.setId(obj1.getString("id"));
                            bean.setPrice(obj1.getString("price"));
                            bean.setAvailable(obj1.getString("available_for_order"));
                            bean.setName(obj1.getString("name"));
                            bean.setId_default_image(obj1.getString("id_default_image"));
                            if(i==(ary.length()-1)) {
                                position=(position+i+1);
                            }
                            //pBean.add(0,bean);
                            int oo=pBean.size();
                            pBean.add(oo,bean);

                        }
                        if (pBean.size() == 0) {
                            nodata.setVisibility(View.VISIBLE);
                            gridview.setVisibility(View.GONE);
                            list.setVisibility(View.GONE);
                        } else {
                            nodata.setVisibility(View.GONE);
                            pgAdapter = new ProGridAdaptern(pBean, mContext);
                            gridview.setAdapter(pgAdapter);
                            list.setAdapter(pgAdapter);

                            Log.e("positionj",current_index+" ");
                           // gridview.smoothScrollToPosition(current_index);
                            gridview.smoothScrollToPositionFromTop(current_index,0);
                            //pgAdapter.notifyDataSetChanged();
                            loadingMore = false;
                            //setGridViewHeightBasedOnChildren(gridview,3);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("Exception", e.toString());
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            Log.e("ERROR", "error => " + error.toString());
                            pDialog.setCancelable(false);
                            pDialog.dismiss();
                            loadingMore= false;
                        }
                    }

            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    //Log.e("params", params + "");
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            // Adding request to request queue
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        } else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }
    }
    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
        gridView.requestLayout();
    }
    private void Imageslidelogo() {
        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("1", R.drawable.slideimage);
        file_maps.put("2", R.drawable.slidetwo);
        file_maps.put("3", R.drawable.slidethree);

        for (String name : file_maps.keySet()) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            defaultSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
            slideimg.addSlider(defaultSliderView);
        }
        slideimg.setPresetTransformer(SliderLayout.Transformer.Accordion);
        slideimg.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slideimg.setCustomAnimation(new DescriptionAnimation());
        slideimg.setDuration(2500);
    }

    private void PrepareData() {
        /*ProGridBean bean = new ProGridBean("kdllskd", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkdfjdjknc", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkjdfjeijc", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkjdfjeijc", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkjdfjeijc", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkjdfjeijc", R.drawable.product);
        proGridBean.add(bean);
        bean = new ProGridBean("lkjdfjeijc", R.drawable.product);
        proGridBean.add(bean);*/
    }

    public static void SearchFilter(String s) {
        //Log.e("Store",s+"");
        pgAdapter.getFilter().filter(s.toString());
    }
}