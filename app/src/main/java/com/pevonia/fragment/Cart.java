package com.pevonia.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pevonia.R;
import com.pevonia.activity.Login;
import com.pevonia.activity.Navigation;
import com.pevonia.adapter.CartProAdapter;
import com.pevonia.bean.CartProductBean;
import com.pevonia.pevonia.ConnectionDetector;
import com.pevonia.pevonia.LCManager;
import com.pevonia.pevonia.UserSessionManager;
import com.pevonia.util.CartProductListener;
import com.pevonia.util.WebService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Cart extends Fragment implements CartProductListener {
    private View v;
    Context mContext;
    public static TextView ordertotal1, otnew1, ordertotal,tv1;
    public static RelativeLayout withshipping;
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;
    LCManager lcManager;
    HashMap<String, String> lcmap;
    public static List<CartProductBean> cpBean = new ArrayList<>();
    public static CartProAdapter cpAdapter;
    static ListView list;
    ScrollView parentScroll;
    public static ImageView nodata;
    double tt = 0;
    private ProgressDialog pDialog;
    private ArrayList<CartProductBean> mList;
    CartProAdapter mAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.cart, container, false);
        cd = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        lcManager = new LCManager(getActivity());
        lcmap = lcManager.getLC();
        SetupView();
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            if (WebService.CartID.equals("")) {
                nodata.setVisibility(View.VISIBLE);
                list.setVisibility(View.GONE);
                withshipping.setVisibility(View.GONE);
                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
            }
        } else {
            WebService.MakeToast(getActivity(), "Please login to continue");
            Intent intent = new Intent(getActivity(), Login.class);
            startActivity(intent);
            getActivity().finish();
        }
        if (!WebService.addressid.equals("")) {
            Navigation.confirm.setText("Confirm Order");
        }else {
            Navigation.confirm.setText("Select Address");
        }
        if (!WebService.shipingid.equals("")){
            ordertotal.setVisibility(View.VISIBLE);
            tv1.setVisibility(View.VISIBLE);
        }else {
            ordertotal.setVisibility(View.GONE);
            tv1.setVisibility(View.GONE);
        }

        return v;
    }


    private void dataset() {
       /* if (cpBean.size() == 0) {
            nodata.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            withshipping.setVisibility(View.GONE);
            Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
        } else {
            list.setVisibility(View.VISIBLE);
            withshipping.setVisibility(View.VISIBLE);
            nodata.setVisibility(View.GONE);
        }
        cpAdapter = new CartProAdapter(cpBean, getActivity());
        list.setAdapter(cpAdapter);
        cpAdapter.notifyDataSetChanged();
        //Navigation.counttext.setVisibility(View.VISIBLE);
        Log.e("size", "" + cpAdapter.getCount());
        Navigation.counttext.setText("" + cpAdapter.getCount());
        if (cpAdapter.getCount() != 0) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Navigation.counttext.setBackground(getActivity().getResources().getDrawable(R.drawable.round, null));
            }else {
                Navigation.counttext.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.round));
            }
            //Navigation.counttext.setBackground(getResources().getDrawable(R.drawable.round, null));
        } else {
            Navigation.counttext.setBackground(null);
        }*/
    }

    private void SetupView() {
        list = (ListView) v.findViewById(R.id.list);
        nodata = (ImageView) v.findViewById(R.id.nodata);
        tv1 = (TextView) v.findViewById(R.id.tv1);
        ordertotal = (TextView) v.findViewById(R.id.ordertotal);
        ordertotal1 = (TextView) v.findViewById(R.id.ot);
        otnew1 = (TextView) v.findViewById(R.id.otnew);
        withshipping = (RelativeLayout) v.findViewById(R.id.withshipping);
        //parentScroll=v.findViewById(R.id.)
    }

    public static void RemoveData(String rm) {
        if (rm.equals("confirm")) {
            //GetPayment(WebService.paypalAmount);
            cpAdapter.Remove();
            Cart.nodata.setVisibility(View.VISIBLE);
            Cart.withshipping.setVisibility(View.GONE);
        } else {
            cpAdapter.Remove();
            Cart.nodata.setVisibility(View.VISIBLE);
            Cart.withshipping.setVisibility(View.GONE);
            //newdataset();
        }
    }

    private void getCartProduct() {
       /* if (cd.isConnectingToInternet()) {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

            String url =WebService.Cart_List+WebService.CartID+"?"+WebService.output_format;
            StringRequest loginreq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pDialog.dismiss();
                    cpBean.clear();
                    try {
                        String fic_response = WebService.fixEncoding(response);
                        Log.e("Cart_List ",fic_response + " ");
                        JSONObject obj = new JSONObject(fic_response);
                        JSONObject obj1 = new JSONObject(obj.getString("cart"));
                        if (obj1.has("associations")) {
                            JSONObject associationsObj = new JSONObject(obj1.getString("associations"));
                            Log.e("cart_rows", "" + associationsObj.getString("cart_rows"));
                            JSONArray ary = associationsObj.getJSONArray("cart_rows");
                            for (int i = 0; i < ary.length(); i++) {
                                JSONObject cproduct = ary.getJSONObject(i);
                                CartProductBean bean = new CartProductBean();
                                Log.e("id_product", "" + cproduct.getString("id_product"));
                                Log.e("product_name", "" + cproduct.getString("product_name"));
                                Log.e("quantity", "" + cproduct.getString("quantity"));
                                bean.setId(cproduct.getString("id_product"));
                                bean.setPrice(cproduct.getString("product_price"));
                                bean.setName(cproduct.getString("product_name"));
                                bean.setQty(cproduct.getString("quantity"));
                                bean.setId_default_image(cproduct.getString("product_image"));
                                cpBean.add(bean);
                            }
                            if (cpBean.size() == 0) {
                                nodata.setVisibility(View.VISIBLE);
                                list.setVisibility(View.GONE);
                                withshipping.setVisibility(View.GONE);
                                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                                Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                                Navigation.cancel.setText("Your cart is empty...");
                                Navigation.confirm.setVisibility(View.GONE);
                                Cart.nodata.setVisibility(View.VISIBLE);
                                Cart.withshipping.setVisibility(View.GONE);
                                Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                                paypalAmount = "0.00";
                            } else {
                                list.setVisibility(View.VISIBLE);
                                withshipping.setVisibility(View.VISIBLE);
                                nodata.setVisibility(View.GONE);
                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Navigation.counttext.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round));
                                } else {
                                    Navigation.counttext.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.round));
                                }
                                //Navigation.counttext.setBackground(context.getResources().getDrawable(R.drawable.round,null));
                                Navigation.counttext.setText("" + cpBean.size());
                                Navigation.cancel.setText("cancel Order");
                                Navigation.confirm.setVisibility(View.VISIBLE);
                            }
                        }else {
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Navigation.counttext.setBackgroundColor(getActivity().getResources().getColor(R.color.trans));
                            } else {
                                Navigation.counttext.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.trans));
                            }
                            nodata.setVisibility(View.VISIBLE);
                            list.setVisibility(View.GONE);
                            withshipping.setVisibility(View.GONE);
                            Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                            Navigation.cancel.setText("Your cart is empty...");
                            Navigation.confirm.setVisibility(View.GONE);
                            Cart.nodata.setVisibility(View.VISIBLE);
                            Cart.withshipping.setVisibility(View.GONE);
                            Cart.otnew1.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
                            paypalAmount = "0.00";
                        }
                        cpAdapter = new CartProAdapter(cpBean, getActivity());
                        list.setAdapter(cpAdapter);
                        cpAdapter.notifyDataSetChanged();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // TODO Auto-generated method stub
                            //Log.e("ERROR", "error => " + error.toString());
                            pDialog.dismiss();
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = WebService.SetAuth();
                    return params;
                }
            };
            loginreq.setRetryPolicy(new DefaultRetryPolicy(
                    10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            PevoniaApp.getInstance().addToRequestQueue(loginreq, WebService.tag_string_req);
        }else {
            WebService.MakeToast(getActivity(), getString(R.string.check_internet));
        }*/
    }

    @Override
    public void loadProduct(ArrayList<CartProductBean> cartProductBeans) {
        this.mList = cartProductBeans;
        Log.e("size", mList.size() + "");
        if (mList.size() == 0) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Navigation.counttext.setBackgroundColor(getActivity().getResources().getColor(R.color.trans));
            } else {
                Navigation.counttext.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.trans));
                }
            Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getActivity()) + "0.00");
            Navigation.cancel.setText("Your cart is empty...");
            Navigation.confirm.setVisibility(View.GONE);
        } else {
            if (mAdapter == null) {
                //mAdapter = new AgendaAdapter(mContext, mList);
                mAdapter = new CartProAdapter(mList, getActivity());
            }
        }
        mAdapter.notifyDataSetChanged();
        list.setAdapter(mAdapter);
    }
}